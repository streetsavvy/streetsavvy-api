# StreetSavvy-API

## Synopsis

The StreetSavvy API package contains the necessary scripts and packages to deploy an API which uses ngninx and mongodb to serve risk data pertaining to a city's fire and police calls for service. The current iteration uses San Francisco data.

## About the Code

The package uses a Python Flask application to serve JSON datasets containing the risk data. Python scripts are used to plug into the San Francisco Open Data API to download the city's calls for service data and cache geolocations for the data. Code examples to follow.

## Motivation

This project exists to provide risk data to safety apps used by consumers as they traverse through urban environments. By using this data, applications can provide routing instructions for safer routes.

## Installation

Installation instructions forthcoming.

## API Reference

API reference will be provided via Sphinx.

## Tests

Testing will be provided using PyTest.

## Contributing

As this project is in its initial stages, we don't expect much outside contribution, but helpful comments and suggestions are accepted via GitLab Issues. We will develop further structure in the near future.

