"""
    This script updates police service call documents in the database
    with latitude and longitudes for their location if available in
    the geocode cache
"""
# Standard Libs
from argparse import ArgumentParser
from configparser import ConfigParser

# 3rd Party Libs
from pymongo import MongoClient

# Internal Libs
from tools.ssgeo.ssgeo import SSGeo

def main():
    """Main entry point function."""
    parser = ArgumentParser(description="Update police documents with " +
                            "locations")
    parser.add_argument("-c", dest='cfg_db', default='db.ini',
                        help='ini file for configurations. Default is db.ini')
    args = parser.parse_args()

    cfg = ConfigParser()
    cfg.read(args.cfg_db)
    db_streetsavvy_name = cfg['Databases']['streetsavvy']
    coll_pol_name = cfg['Collections']['police_serv_calls']

    db_streetsavvy = MongoClient()[db_streetsavvy_name]
    db_streetsavvy.authenticate(cfg['Accounts']['user_scraper'],
                                cfg['Accounts']['pw_scraper'])
    coll_pol = db_streetsavvy[coll_pol_name]

    # Get police service calls documents without lat/long fields
    cursor = coll_pol.find({'$or': [{'latitude': {'$exists': False}},
                                    {'longitude': {'$exists': False}},
                                    {'loc': {'$exists': False}}]},
                           modifiers={"$snapshot": True},
                           batch_size=1000)

    # Update documents that have a lat/lon cached
    geo = SSGeo(args.cfg_db)
    bulk = coll_pol.initialize_unordered_bulk_op()
    counter = 0
    for doc in cursor:
        # Check if current doc has a lat/lon cached in the database
        if 'city' in doc:
            lat, lon = geo.getlatlon_cached(doc['address'], doc['state'],
                                            doc['city'])
        else:
            lat, lon = geo.getlatlon_cached(doc['address'], doc['state'])

        # If it was found in the cache
        if lat is not None and lon is not None:
            # Update the doc to include this lat and long and a geoJSON
            # version of it
            geo_json = {'coordinates': [lon, lat], 'type': 'Point'}
            bulk.find({'crime_id': doc['crime_id']}).update({
                '$set': {'latitude': lat, 'longitude': lon, 'loc': geo_json}})
            counter += 1

        # Execute bulk operation if we're at 1000 operations
        if counter == 1000:
            bulk.execute()
            bulk = coll_pol.initialize_unordered_bulk_op()
            counter = 0

    # If we have any leftover bulk operations to execute
    if counter > 0:
        bulk.execute()

if __name__ == "__main__":
    main()
