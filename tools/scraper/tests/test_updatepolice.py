"""
    Tests for updatepolice.py
"""
# pylint: disable=redefined-outer-name,invalid-name

# Standard Libs
import os
from configparser import ConfigParser
from datetime import datetime
from json import loads
from subprocess import call
from timeit import Timer

# 3rd Party Libs
import pytest
from pymongo import MongoClient

PATH = os.path.dirname(os.path.realpath(__file__))
UPDATEPOLICE_PATH = os.path.join(PATH, '../updatepolice.py')

DB_INI = os.path.join(PATH, 'test_db.ini')

POLICE_JSON = os.path.join(PATH, 'police.json')
GEO_JSON = os.path.join(PATH, 'geo.json')

@pytest.fixture()
def coll_police():
    """
        Return a Mongo connection to the police service call data that
        has been cleared
    """
    cfg = ConfigParser()
    cfg.read(DB_INI)
    db_streetsavvy_name = cfg['Databases']['streetsavvy']
    coll_police_name = cfg['Collections']['police_serv_calls']

    # Make sure database/collection names starts with test so we don't
    # accidentally mess with production servers
    assert db_streetsavvy_name.startswith('test')
    assert coll_police_name.startswith('test')

    db_streetsavvy = MongoClient()[db_streetsavvy_name]
    db_streetsavvy.authenticate(cfg['Accounts']['user_scraper'],
                                cfg['Accounts']['pw_scraper'])
    coll_police = db_streetsavvy[coll_police_name]
    coll_police.drop() # Delete data for testing
    coll_police.create_index('crime_id', unique=True)

    # Make sure data was actually deleted
    assert coll_police.count() == 0

    return coll_police

@pytest.fixture()
def coll_geo_cache():
    """
        Return a Mongo connection to the geo_cache collection
        that has been cleared
    """
    cfg = ConfigParser()
    cfg.read(DB_INI)
    db_streetsavvy_name = cfg['Databases']['streetsavvy']
    coll_geo_cache_name = cfg['Collections']['geocode_cache']

    # Make sure database/collection names starts with test so we don't
    # accidentally mess with production servers
    assert db_streetsavvy_name.startswith('test')
    assert coll_geo_cache_name.startswith('test')

    db_streetsavvy = MongoClient()[db_streetsavvy_name]
    db_streetsavvy.authenticate(cfg['Accounts']['user_scraper'],
                                cfg['Accounts']['pw_scraper'])
    coll_geo_cache = db_streetsavvy[coll_geo_cache_name]
    coll_geo_cache.drop() # Delete data for testing
    assert coll_geo_cache.count() == 0

    return coll_geo_cache

@pytest.mark.mongo
def run_updatepolice():
    """
        Run updatepolice.py with the test_db.ini configuration file by
        spawning a subprocess. This is to simulate running
        updatepolice.py on its own as it'll be done in a production
        environment.
    """
    call('python ' + UPDATEPOLICE_PATH + ' -c ' + DB_INI, shell=True)

@pytest.mark.mongo
def test_updatepolice_with_one_police_location_in_cache(
        coll_police, coll_geo_cache):
    """
        Test updatepolice with a police service call that has its
        location in the geocode cache. The associated document should
        be updated with latitude and longitude fields.
    """
    # Setup data
    coll_geo_cache.insert_one(
        {
            "address" : "1500 Block Of Vallejo St",
            "city" : "San Francisco",
            "state" : "CA",
            "latitude" : 37.77711868286133,
            "longitude" : -122.41963958740234
        })
    coll_police.insert_one(
        {
            "city" : "San Francisco",
            "call_date" : datetime(2016, 8, 27, 0, 0, 0, 0),
            "state" : "CA",
            "original_crimetype_name" : "Homeless Complaint",
            "call_time" : "12:04",
            "agency_id" : "1",
            "call_dttm" : datetime(2016, 8, 27, 12, 4, 0, 0),
            "crime_id" : "162401538",
            "offense_date" : datetime(2016, 8, 27, 0, 0, 0, 0),
            "disposition" : "HAN",
            "address" : "1500 Block Of Vallejo St",
            "report_date" : datetime(2016, 8, 27, 0, 0, 0, 0),
            "address_type" : "Premise Address"
        })
    run_updatepolice()
    doc = coll_police.find_one()
    assert doc['latitude'] == 37.77711868286133
    assert doc['longitude'] == -122.41963958740234

    # Check geoJSON version of the coordinates
    loc = doc['loc']
    assert loc['coordinates'][0] == -122.41963958740234
    assert loc['coordinates'][1] == 37.77711868286133
    assert loc['type'] == 'Point'

@pytest.mark.mongo
def test_updatepolice_with_one_police_location_in_cache_and_one_not_in_cache(
        coll_police, coll_geo_cache):
    """
        Test updatepolice with a police service call that has its
        location in the geocode cache and a police service call that
        does not have its location in the cache. The police service call
        document with its location in the geocode cache should be
        updated with lat/long fields while the other one should not.
    """
    # Setup data
    coll_geo_cache.insert_one(
        {
            "address" : "1500 Block Of Vallejo St",
            "city" : "San Francisco",
            "state" : "CA",
            "latitude" : 37.77711868286133,
            "longitude" : -122.41963958740234
        })
    coll_police.insert_one(
        {
            "city" : "San Francisco",
            "call_date" : datetime(2016, 8, 27, 0, 0, 0, 0),
            "state" : "CA",
            "original_crimetype_name" : "Homeless Complaint",
            "call_time" : "12:04",
            "agency_id" : "1",
            "call_dttm" : datetime(2016, 8, 27, 12, 4, 0, 0),
            "crime_id" : "162401538",
            "offense_date" : datetime(2016, 8, 27, 0, 0, 0, 0),
            "disposition" : "HAN",
            "address" : "1500 Block Of Vallejo St",
            "report_date" : datetime(2016, 8, 27, 0, 0, 0, 0),
            "address_type" : "Premise Address"
        })
    coll_police.insert_one(
        {
            "city" : "San Francisco",
            "call_date" : datetime(2016, 3, 31, 0, 0, 0),
            "state" : "CA",
            "original_crimetype_name" : "Intoxicated Person",
            "call_time" : "19:57",
            "agency_id" : "1",
            "call_dttm" : datetime(2016, 3, 31, 19, 57, 0),
            "crime_id" : "160913309",
            "offense_date" : datetime(2016, 3, 31, 0, 0, 0),
            "disposition" : "HAN",
            "address" : "300 Block Of Ofarrell St",
            "report_date" : datetime(2016, 3, 31, 0, 0, 0),
            "address_type" : "Premise Address"
        })
    run_updatepolice()

    doc = coll_police.find_one({'crime_id': '162401538'})
    assert doc['latitude'] == 37.77711868286133
    assert doc['longitude'] == -122.41963958740234

    # Check geoJSON version of the coordinates
    loc = doc['loc']
    assert loc['coordinates'][0] == -122.41963958740234
    assert loc['coordinates'][1] == 37.77711868286133
    assert loc['type'] == 'Point'

    doc = coll_police.find_one({'crime_id': '160913309'})
    assert 'latitude' not in doc
    assert 'longitude' not in doc
    assert 'loc' not in doc

@pytest.mark.mongo
@pytest.mark.usefixtures('coll_geo_cache')
def test_updatepolice_with_one_police_location_not_in_cache(coll_police):
    """
        Test updatepolice with a police service call that does not have
        its location in the cache. It should not be updated.
    """
    # Setup data
    coll_police.insert_one(
        {
            "city" : "San Francisco",
            "call_date" : datetime(2016, 3, 31, 0, 0, 0),
            "state" : "CA",
            "original_crimetype_name" : "Intoxicated Person",
            "call_time" : "19:57",
            "agency_id" : "1",
            "call_dttm" : datetime(2016, 3, 31, 19, 57, 0),
            "crime_id" : "160913309",
            "offense_date" : datetime(2016, 3, 31, 0, 0, 0),
            "disposition" : "HAN",
            "address" : "300 Block Of Ofarrell St",
            "report_date" : datetime(2016, 3, 31, 0, 0, 0),
            "address_type" : "Premise Address"
        })
    run_updatepolice()

    doc = coll_police.find_one({'crime_id': '160913309'})
    assert 'latitude' not in doc
    assert 'longitude' not in doc
    assert 'loc' not in doc

@pytest.mark.mongo
def test_updatepolice_with_two_police_locations_that_have_same_cache(
        coll_police, coll_geo_cache):
    """
        Test updatepolice with two police service calls that have
        locations in the geocode cache. Both should be updated with
        the same lat/long fields
    """
    # Setup data
    coll_geo_cache.insert_one(
        {
            "address" : "1500 Block Of Vallejo St",
            "city" : "San Francisco",
            "state" : "CA",
            "latitude" : 37.77711868286133,
            "longitude" : -122.41963958740234
        })
    coll_police.insert_one(
        {
            "city" : "San Francisco",
            "call_date" : datetime(2016, 8, 27, 0, 0, 0, 0),
            "state" : "CA",
            "original_crimetype_name" : "Homeless Complaint",
            "call_time" : "12:04",
            "agency_id" : "1",
            "call_dttm" : datetime(2016, 8, 27, 12, 4, 0, 0),
            "crime_id" : "162401538",
            "offense_date" : datetime(2016, 8, 27, 0, 0, 0, 0),
            "disposition" : "HAN",
            "address" : "1500 Block Of Vallejo St",
            "report_date" : datetime(2016, 8, 27, 0, 0, 0, 0),
            "address_type" : "Premise Address"
        })
    coll_police.insert_one(
        {
            "city" : "San Francisco",
            "call_date" : datetime(2016, 8, 25, 0, 0, 0),
            "state" : "CA",
            "original_crimetype_name" : "Fraud",
            "call_time" : "15:20",
            "agency_id" : "1",
            "call_dttm" : datetime(2016, 8, 25, 15, 20, 0),
            "crime_id" : "162382367",
            "offense_date" : datetime(2016, 8, 25, 0, 0, 0),
            "disposition" : "GOA",
            "address" : "1500 Block Of Vallejo St",
            "report_date" : datetime(2016, 8, 25, 0, 0, 0),
            "address_type" : "Premise Address"
        })
    run_updatepolice()

    doc1 = coll_police.find_one({'crime_id': '162401538'})
    doc2 = coll_police.find_one({'crime_id': '162382367'})
    assert doc1['latitude'] == doc2['latitude']
    assert doc1['longitude'] == doc2['longitude']
    assert doc1['loc'] == doc2['loc']

@pytest.mark.mongo
@pytest.mark.usefixtures('coll_geo_cache')
def test_updatepolice_with_no_police_docs_makes_no_changes(
        coll_police):
    """
        Test update police when there are no police documents. Make sure
        nothing changes in the police collection
    """
    run_updatepolice()

    assert coll_police.count() == 0

@pytest.mark.stress
@pytest.mark.mongo
def test_updatepolice_with_large_data_updates_expected_records(
        coll_police, coll_geo_cache):
    """
        Test update police when a large dataset similar to what will be
        in the database during production will properly update police
        documents that have their location in the geocode cache
    """
    # Import data from files
    data = []
    with open(POLICE_JSON, 'r') as f:
        for line in f:
            json_parsed = loads(line)
            json_parsed.pop('_id', None)
            json_parsed['call_date'] = datetime.strptime(
                json_parsed['call_date']['$date'], '%Y-%m-%dT%H:%M:%S.%fZ')
            json_parsed['call_dttm'] = datetime.strptime(
                json_parsed['call_dttm']['$date'], '%Y-%m-%dT%H:%M:%S.%fZ')
            json_parsed['offense_date'] = datetime.strptime(
                json_parsed['offense_date']['$date'], '%Y-%m-%dT%H:%M:%S.%fZ')
            json_parsed['report_date'] = datetime.strptime(
                json_parsed['report_date']['$date'], '%Y-%m-%dT%H:%M:%S.%fZ')
            data.append(json_parsed)
    coll_police.insert_many(data)
    data = []
    with open(GEO_JSON, 'r') as file:
        for line in file:
            json_parsed = loads(line)
            json_parsed.pop('_id', None)
            data.append(json_parsed)
    coll_geo_cache.insert_many(data)

    # Record how many docs have a lat/long before running updatepolice
    cursor = coll_police.find({'$or': [{'latitude': {'$exists': False}},
                                       {'longitude': {'$exists': False}}]})
    docs_without_loc = cursor.count()
    cursor = coll_police.find({'$or': [{'latitude': {'$exists': True}},
                                       {'longitude': {'$exists': True}}]})
    docs_with_loc = cursor.count()

    # Makes sure updatepolice takes a reasonable amount of time.
    # 600 s = 10 minutes
    timer = Timer('run_updatepolice()', globals=globals())
    assert timer.timeit(1) < 600

    # Make sure we have the expected number of docs without a location
    # and that some police records have been updated
    cursor = coll_police.find({'$or': [{'latitude': {'$exists': False}},
                                       {'longitude': {'$exists': False}}]})
    assert cursor.count() == 6069
    assert cursor.count() != docs_without_loc

    # Make sure we have the expected number of docs with a location
    # and that some police records have been updated
    cursor = coll_police.find({'$or': [{'latitude': {'$exists': True}},
                                       {'longitude': {'$exists': True}}]})
    assert cursor.count() == 3931
    assert cursor.count() != docs_with_loc

@pytest.mark.mongo
def test_updatepolice_with_police_records_that_have_lat_lon_fields_but_no_geo_json_adds_geo_json_fields(
        coll_police, coll_geo_cache):
    # Setup data
    coll_geo_cache.insert_one(
        {
            "address" : "1500 Block Of Vallejo St",
            "city" : "San Francisco",
            "state" : "CA",
            "latitude" : 37.77711868286133,
            "longitude" : -122.41963958740234
        })
    coll_police.insert_one(
        {
            "city" : "San Francisco",
            "call_date" : datetime(2016, 8, 27, 0, 0, 0, 0),
            "state" : "CA",
            "original_crimetype_name" : "Homeless Complaint",
            "call_time" : "12:04",
            "agency_id" : "1",
            "call_dttm" : datetime(2016, 8, 27, 12, 4, 0, 0),
            "crime_id" : "162401538",
            "offense_date" : datetime(2016, 8, 27, 0, 0, 0, 0),
            "disposition" : "HAN",
            "address" : "1500 Block Of Vallejo St",
            "report_date" : datetime(2016, 8, 27, 0, 0, 0, 0),
            "address_type" : "Premise Address",
            "latitude" : 37.77711868286133,
            "longitude" : -122.41963958740234
        })
    coll_police.insert_one(
        {
            "city" : "San Francisco",
            "call_date" : datetime(2016, 8, 25, 0, 0, 0),
            "state" : "CA",
            "original_crimetype_name" : "Fraud",
            "call_time" : "15:20",
            "agency_id" : "1",
            "call_dttm" : datetime(2016, 8, 25, 15, 20, 0),
            "crime_id" : "162382367",
            "offense_date" : datetime(2016, 8, 25, 0, 0, 0),
            "disposition" : "GOA",
            "address" : "1500 Block Of Vallejo St",
            "report_date" : datetime(2016, 8, 25, 0, 0, 0),
            "address_type" : "Premise Address",
            "latitude" : 37.77711868286133,
            "longitude" : -122.41963958740234
        })
    run_updatepolice()

    doc = coll_police.find_one({'crime_id': '162401538'})
    loc = doc['loc']
    assert loc['coordinates'][0] == -122.41963958740234
    assert loc['coordinates'][1] == 37.77711868286133
    assert loc['type'] == 'Point'

    doc = coll_police.find_one({'crime_id': '162382367'})
    loc = doc['loc']
    assert loc['coordinates'][0] == -122.41963958740234
    assert loc['coordinates'][1] == 37.77711868286133
    assert loc['type'] == 'Point'
