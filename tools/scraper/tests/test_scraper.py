"""
    Tests for scraper.py
"""
# pylint: disable=redefined-outer-name,invalid-name

# Standard Libs
import os
from configparser import ConfigParser
from datetime import datetime

# 3rd Party Libs
import pytest
from pymongo import MongoClient

# Internal Libs
from tools.scraper.scrape import getloctogeocode, geocodelocations

DB_INI = os.path.join(os.path.dirname(os.path.realpath(__file__)),
                      'test_db.ini')

POLICE_JSON = os.path.join(os.path.dirname(os.path.realpath(__file__)),
                           'police.json')

@pytest.fixture()
def cfg():
    """Return initialized ConfigParser"""
    config = ConfigParser()
    config.read(DB_INI)
    return config

@pytest.fixture()
def coll_police(cfg):
    """
        Return a Mongo connection to the police service call data that
        has been cleared
    """
    db_streetsavvy_name = cfg['Databases']['streetsavvy']
    coll_police_name = cfg['Collections']['police_serv_calls']

    # Make sure database/collection names starts with test so we don't
    # accidentally mess with production servers
    assert db_streetsavvy_name.startswith('test')
    assert coll_police_name.startswith('test')

    db_streetsavvy = MongoClient()[db_streetsavvy_name]
    db_streetsavvy.authenticate(cfg['Accounts']['user_scraper'],
                                cfg['Accounts']['pw_scraper'])
    coll_police = db_streetsavvy[coll_police_name]
    coll_police.drop() # Delete data for testing
    coll_police.create_index('crime_id', unique=True)

    # Make sure data was actually deleted
    assert coll_police.count() == 0

    return coll_police

@pytest.fixture()
def coll_geo_cache(cfg):
    """
        Return a Mongo connection to the geo_cache collection
        that has been cleared
    """
    db_streetsavvy_name = cfg['Databases']['streetsavvy']
    coll_geo_cache_name = cfg['Collections']['geocode_cache']

    # Make sure database/collection names starts with test so we don't
    # accidentally mess with production servers
    assert db_streetsavvy_name.startswith('test')
    assert coll_geo_cache_name.startswith('test')

    db_streetsavvy = MongoClient()[db_streetsavvy_name]
    db_streetsavvy.authenticate(cfg['Accounts']['user_scraper'],
                                cfg['Accounts']['pw_scraper'])
    coll_geo_cache = db_streetsavvy[coll_geo_cache_name]
    coll_geo_cache.drop()
    assert coll_geo_cache.count() == 0

    return coll_geo_cache

@pytest.fixture()
def coll_geo_cache_with_data(coll_geo_cache):
    """
        Return a Mongo connection to the geo_cache collection
        that has data
    """
    coll_geo_cache.insert_one(
        {
            "address" : "13th St and gateview Ave",
            "state" : "CA",
            "longitude" : -122.3720527,
            "latitude" : 37.8283565
        }
    )
    assert coll_geo_cache.count() == 1
    return coll_geo_cache

@pytest.fixture()
def coll_geo_api_rates(cfg):
    """
        Return a Mongo connection to the geo_api_rates collection
        that has been cleared
    """
    db_streetsavvy_name = cfg['Databases']['streetsavvy']
    coll_geo_api_rates_name = cfg['Collections']['geo_api_rates']

    # Make sure database/collection names starts with test so we don't
    # accidentally mess with production servers
    assert db_streetsavvy_name.startswith('test')
    assert coll_geo_api_rates_name.startswith('test')

    db_streetsavvy = MongoClient()[db_streetsavvy_name]
    db_streetsavvy.authenticate(cfg['Accounts']['user_scraper'],
                                cfg['Accounts']['pw_scraper'])
    coll_geo_api_rates = db_streetsavvy[coll_geo_api_rates_name]
    coll_geo_api_rates.drop()
    assert coll_geo_api_rates.count() == 0

    return coll_geo_api_rates

@pytest.fixture()
def coll_geo_api_rates_with_data(cfg, coll_geo_api_rates):
    """
        Return a Mongo connection to the geo_api_rates collection
        that has been initialized
    """
    # Initialize geo_api_rates with mock data
    coll_geo_api_rates.insert_one(
        {
            'callsperday': 2500,
            'date': datetime(2016, 11, 8, 0, 44, 7, 258),
            'geocoder': 'GoogleV3',
            'callstoday': 2409,
            'apikey': cfg['APIKeys']['GoogleV3']
        }
    )
    coll_geo_api_rates.insert_one(
        {
            'callsperday': 341,
            'date': datetime(2016, 11, 8, 0, 44, 7, 253),
            'geocoder': 'Bing',
            'callstoday': 300,
            'apikey': cfg['APIKeys']['Bing']
        }
    )

    assert coll_geo_api_rates.count() == 2

    return coll_geo_api_rates

@pytest.mark.mongo
def test_getloctogeocode(coll_police):
    """
        Test getloctogeocode returns a command cursor to unique
        locations from police service calls that have not been updated
        with a latitude or longitude field yet.
    """
    coll_police.insert_one(
        {
            "crime_id" : "160960018",
            "original_crimetype_name" : "Suspicious Person",
            "call_time" : "00:06",
            "disposition" : "ADV",
            "address" : "300 Block Of Ofarrell St",
            "address_type" : "Premise Address",
            "call_dttm" : datetime(2016, 4, 5, 0, 6, 0),
            "offense_date" : datetime(2016, 4, 5, 0, 0, 0),
            "report_date" : datetime(2016, 4, 5, 0, 0, 0),
            "call_date" : datetime(2016, 4, 5, 0, 0, 0),
            "state" : "CA",
            "city" : "San Francisco",
            "agency_id" : "1"
        })
    coll_police.insert_one(
        {
            "crime_id" : "160913309",
            "original_crimetype_name" : "Intoxicated Person",
            "call_time" : "19:57",
            "disposition" : "HAN",
            "address" : "300 Block Of Ofarrell St",
            "address_type" : "Premise Address",
            "call_dttm" : datetime(2016, 3, 31, 19, 57, 0),
            "offense_date" : datetime(2016, 3, 31, 0, 0, 0),
            "report_date" : datetime(2016, 3, 31, 0, 0, 0),
            "call_date" : datetime(2016, 3, 31, 0, 0, 0),
            "state" : "CA",
            "city" : "San Francisco",
            "agency_id" : "1"
        })
    coll_police.insert_one(
        {
            "city" : "San Francisco",
            "call_date" : datetime(2016, 3, 31, 0, 0, 0),
            "state" : "CA",
            "original_crimetype_name" : "Tent",
            "call_time" : "19:54",
            "agency_id" : "1",
            "call_dttm" : datetime(2016, 3, 31, 19, 54, 0),
            "crime_id" : "160913298",
            "offense_date" : datetime(2016, 3, 31, 0, 0, 0),
            "disposition" : "ADV",
            "address" : "100 Block Of South Van Ness Av",
            "report_date" : datetime(2016, 3, 31, 0, 0, 0),
            "address_type" : "Premise Address",
            "longitude" : -122.4187386,
            "latitude" : 37.7730649
        })
    coll_police.insert_one(
        {
            "city" : "San Francisco",
            "call_date" : datetime(2016, 4, 11, 0, 0, 0),
            "state" : "CA",
            "original_crimetype_name" : "Homeless Complaint",
            "call_time" : "07:48",
            "agency_id" : "1",
            "call_dttm" : datetime(2016, 4, 11, 7, 48, 0),
            "crime_id" : "161020675",
            "offense_date" : datetime(2016, 4, 11, 0, 0, 0),
            "disposition" : "HAN",
            "address" : "0 Block Of 101nb C Chavez Of",
            "report_date" : datetime(2016, 4, 11, 0, 0, 0),
            "address_type" : "Premise Address"
        })
    coll_police.insert_one(
        {
            "call_date" : datetime(2016, 4, 12, 0, 0, 0),
            "state" : "CA",
            "original_crimetype_name" : "Passing Call",
            "call_time" : "19:30",
            "agency_id" : "1",
            "call_dttm" : datetime(2016, 4, 12, 19, 30, 0),
            "crime_id" : "161033563",
            "offense_date" : datetime(2016, 4, 12, 0, 0, 0),
            "disposition" : "HAN",
            "address" : "Haight St Corridor",
            "report_date" : datetime(2016, 4, 12, 0, 0, 0),
            "address_type" : "Geo-Override"
        })
    cursor = getloctogeocode(coll_police)
    locations = list(cursor)
    assert len(locations) == 3
    assert {'_id': {'address': 'Haight St Corridor',
                    'address_type': 'Geo-Override',
                    'state': 'CA'}} in locations
    assert {'_id': {'address': '0 Block Of 101nb C Chavez Of',
                    'address_type': 'Premise Address',
                    "city" : "San Francisco",
                    'state': 'CA'}} in locations
    assert {'_id': {'address': 'Haight St Corridor',
                    'address_type': 'Geo-Override',
                    'state': 'CA'}} in locations

@pytest.mark.mongo
def test_getloctogeocode_with_all_records_have_lat_lon(coll_police):
    """
        Test getloctogeocode where all records have lat/long fields.
        Should return a command cursor with no results.
    """
    coll_police.insert_one(
        {
            "city" : "San Francisco",
            "call_date" : datetime(2016, 3, 31, 0, 0, 0),
            "state" : "CA",
            "original_crimetype_name" : "Tent",
            "call_time" : "19:54",
            "agency_id" : "1",
            "call_dttm" : datetime(2016, 3, 31, 19, 54, 0),
            "crime_id" : "160913298",
            "offense_date" : datetime(2016, 3, 31, 0, 0, 0),
            "disposition" : "ADV",
            "address" : "100 Block Of South Van Ness Av",
            "report_date" : datetime(2016, 3, 31, 0, 0, 0),
            "address_type" : "Premise Address",
            "longitude" : -122.4187386,
            "latitude" : 37.7730649
        })
    coll_police.insert_one(
        {
            "call_date" : datetime(2016, 4, 1, 0, 0, 0),
            "state" : "CA",
            "original_crimetype_name" : "Muni Inspection",
            "call_time" : "07:35",
            "agency_id" : "1",
            "call_dttm" : datetime(2016, 4, 1, 7, 35, 0),
            "crime_id" : "160920634",
            "offense_date" : datetime(2016, 4, 1, 0, 0, 0),
            "disposition" : "HAN",
            "address" : "13th St/gateview Ave",
            "report_date" : datetime(2016, 4, 1, 0, 0, 0),
            "address_type" : "Geo-Override",
            "longitude" : -122.3720527,
            "latitude" : 37.8283565
        })
    cursor = getloctogeocode(coll_police)
    locations = list(cursor)
    assert len(locations) == 0

@pytest.mark.mongo
@pytest.mark.geocode
@pytest.mark.usefixtures('coll_geo_api_rates_with_data')
def test_geocodelocations(coll_police, coll_geo_cache_with_data):
    """
        Test geocodelocations when one location needs to be geocoded
        and one does not because it is cached. Make sure that the
        location that was geocoded has been cached.
    """
    coll_police.insert_one(
        {
            "crime_id" : "160913309",
            "original_crimetype_name" : "Intoxicated Person",
            "call_time" : "19:57",
            "disposition" : "HAN",
            "address" : "300 Block Of Ofarrell St",
            "address_type" : "Premise Address",
            "call_dttm" : datetime(2016, 3, 31, 19, 57, 0),
            "offense_date" : datetime(2016, 3, 31, 0, 0, 0),
            "report_date" : datetime(2016, 3, 31, 0, 0, 0),
            "call_date" : datetime(2016, 3, 31, 0, 0, 0),
            "state" : "CA",
            "city" : "San Francisco",
            "agency_id" : "1"
        })
    coll_police.insert_one(
        {
            "call_date" : datetime(2016, 4, 1, 0, 0, 0),
            "state" : "CA",
            "original_crimetype_name" : "Muni Inspection",
            "call_time" : "07:35",
            "agency_id" : "1",
            "call_dttm" : datetime(2016, 4, 1, 7, 35, 0),
            "crime_id" : "160920634",
            "offense_date" : datetime(2016, 4, 1, 0, 0, 0),
            "disposition" : "HAN",
            "address" : "13th St/gateview Ave",
            "report_date" : datetime(2016, 4, 1, 0, 0, 0),
            "address_type" : "Geo-Override",
        })
    locations = getloctogeocode(coll_police)

    geocodelocations(DB_INI, locations)

    # Assert that the one location was geocoded and cached
    assert coll_geo_cache_with_data.count() == 2
    cursor = coll_geo_cache_with_data.find(projection={
        '_id': False, 'latitude': False, 'longitude': False})
    cache = list(cursor)
    assert {'address': '300 Block Of Ofarrell St',
            'city': 'San Francisco',
            'state': 'CA'} in cache

@pytest.mark.mongo
@pytest.mark.usefixtures('coll_geo_api_rates_with_data')
def test_geocodelocations_when_all_locations_cached(coll_police, coll_geo_cache_with_data):
    """
        Test geocodelocations() all locations have already been cached.
        Assert that no locations are added to the cache.
    """
    coll_police.insert_one(
        {
            "call_date" : datetime(2016, 4, 1, 0, 0, 0),
            "state" : "CA",
            "original_crimetype_name" : "Muni Inspection",
            "call_time" : "07:35",
            "agency_id" : "1",
            "call_dttm" : datetime(2016, 4, 1, 7, 35, 0),
            "crime_id" : "160920634",
            "offense_date" : datetime(2016, 4, 1, 0, 0, 0),
            "disposition" : "HAN",
            "address" : "13th St/gateview Ave",
            "report_date" : datetime(2016, 4, 1, 0, 0, 0),
            "address_type" : "Geo-Override",
        })
    geo_count = coll_geo_cache_with_data.count()
    locations = getloctogeocode(coll_police)

    geocodelocations(DB_INI, locations)

    assert coll_geo_cache_with_data.count() == geo_count
