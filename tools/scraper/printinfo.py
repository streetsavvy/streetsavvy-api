"""
    Script to print info about the database during nightly scraper runs
    for logging purposes
"""
# Standard Libraries
from argparse import ArgumentParser
from configparser import ConfigParser
from pprint import PrettyPrinter
from datetime import datetime

# 3rd Party Libraries
from pymongo import MongoClient

def main():
    """Main entry point function."""
    parser = ArgumentParser(description="Update police documents with " +
                            "locations")
    parser.add_argument("-c", dest='cfg_db', default='db.ini',
                        help='ini file for configurations. Default is db.ini')
    args = parser.parse_args()

    cfg = ConfigParser()
    cfg.read(args.cfg_db)
    db_streetsavvy_name = cfg['Databases']['streetsavvy']

    db_streetsavvy = MongoClient()[db_streetsavvy_name]
    db_streetsavvy.authenticate(cfg['Accounts']['user_scraper'],
                                cfg['Accounts']['pw_scraper'])

    coll_police = db_streetsavvy[cfg['Collections']['police_serv_calls']]
    print("Police Count: " + str(coll_police.count()))
    coll_fire = db_streetsavvy[cfg['Collections']['fire_serv_calls']]
    print("Fire Count: " + str(coll_fire.count()))
    coll_geocode_cache = db_streetsavvy[cfg['Collections']['geocode_cache']]
    print("Geocode Cache Count: " + str(coll_geocode_cache.count()))
    coll_geo_exc = db_streetsavvy[cfg['Collections']['geo_exc']]
    print("Geocode Exceptions Count: " + str(coll_geo_exc.count()))

    # Get number of unique locations in police service call data
    pipeline = [
        {'$group': {'_id': {'state': '$state', 'city': '$city',
                            'address': '$address'}}},
        {'$group': {'_id': {'state': '$state', 'city': '$city',
                            'address': '$address'}, 'count': {'$sum': 1}}}]
    cursor = coll_police.aggregate(pipeline)
    print("Number of unique locations in police service call data: " +
          str(cursor.next()['count']))

    # Get how many police docs have a lat/long before running updatepolice
    cursor = coll_police.find({'$or': [{'latitude': {'$exists': False}},
                                       {'longitude': {'$exists': False}}]})
    print("Police docs without lat/longs: " + str(cursor.count()))
    cursor = coll_police.find({'$or': [{'latitude': {'$exists': True}},
                                       {'longitude': {'$exists': True}}]})
    print("Police docs with lat/longs: " + str(cursor.count()))

    # Get geo_api_rate documents that were generated today
    starttime = datetime.today().replace(hour=0, minute=0,
                                         second=0, microsecond=0)
    endtime = datetime.today().replace(hour=23, minute=59,
                                       second=59, microsecond=999999)
    coll_geo_api_rates = db_streetsavvy[cfg['Collections']['geo_api_rates']]
    cursor = coll_geo_api_rates.find(
        {'date': {'$gte': starttime, '$lt': endtime}}).limit(2)
    print("Geo API Rates Info from today: ")
    pprinter = PrettyPrinter()
    for doc in cursor:
        pprinter.pprint(doc)

if __name__ == "__main__":
    main()
