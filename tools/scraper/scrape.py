"""
    This script accepts an ini file for database information, imports
    data using SSImportData, and geocodes locations until either the
    limits are reached or all locations have been geocoded. When
    locations are geocoded, they are cached into the database.
"""
# Standard Libs
import sys
from argparse import ArgumentParser
from configparser import ConfigParser
from datetime import datetime
from enum import Enum, unique

# 3rd Party Libs
from pymongo import MongoClient, DESCENDING
from geopy.exc import GeocoderTimedOut, GeocoderQuotaExceeded

# Internal Libs
from tools.importdata.ssimportdata import SSImportData
from tools.ssgeo.ssgeo import SSGeo

# Return status codes for safegetlatlon function
@unique
class Status(Enum):
    """
        Simple enum for displaying statuses.

        success  (0): Deocoded successfully.
        timeout (-1): GeocoderTimedOut was raised, even after multiple
                      attempts.
        quota   (-2): Quota was reached on a geocoder.
        value   (-3): ValueError was raised, indicating either that the
                      limit was reached on all geocoders, or no
                      geocoders are in the geo_api_rate collection.
    """
    success = 0
    timeout = -1
    quota = -2
    value = -3


def safegetlatlon(cfg_db, address, state, city):
    """
        Wrapper around `ssgeo.getlatlon` to catch various errors and
        reattempt `getlatlon` as necessary.

        Returns a status code as specified in the `Status` enum.
    """
    geo = SSGeo(cfg_db)

    try:
        (_, _, _) = geo.getlatlon(address, state, city)
        return Status.success
    except ValueError:
        return Status.value
    except GeocoderTimedOut:
        print("GeocoderTimedOut raised. ")
        # Attempt to getlatlon 2 more times
        for i in range(2, 4):
            print("Retrying. Attempt: " + str(i))
            try:
                (_, _, _) = geo.getlatlon(address, state, city)
                return Status.success
            except ValueError:
                return Status.value
            except GeocoderTimedOut:
                continue
            except GeocoderQuotaExceeded:
                return Status.quota
    except GeocoderQuotaExceeded:
        return Status.quota

def getloctogeocode(coll_pol):
    """
        Helper function that returns a command cursor to unique
        locations from police service calls that have not been updated
        with a latitude and longitude field yet.

        :param coll_pol authentictated MongoDB collection for police
                        service calls
        :type coll_pol pymongo.Collection

        :return: comamand cursor to specified data
        :return type: pymongo.command_cursor.CommandCursor
    """
    pipeline = [
        {"$match": {"latitude": None, "longitude": None}},
        {"$group": {"_id": {
            "state": "$state", "city": "$city",
            "address": "$address", "address_type": "$address_type"}}}]
    return coll_pol.aggregate(pipeline)

def geocodelocations(cfg_db, locations):
    """
        Helper function to geocode until we hit the limit or all
        locations have been cached

        :param cfg_db path to ini file for configurations
        :type cfg_db str
        :param locations locations to geocode
        :type locations pymongo.command_cursor.CommandCursor
    """
    # Store the results in memory because in larger datasets, the
    # cursor will sometimes timeout and expire.
    locs = list(locations)
    for doc in locs:
        data = doc['_id']

        state = data['state']
        if 'city' in data:
            city = data['city']
        else:
            city = ""
        address = data['address']

        status = safegetlatlon(cfg_db, address, state, city)
        if status == status.value:
            print("SSGeo returned ValueError indicating the geocoders have " +
                  "reached their limits or there are no geocoders in the " +
                  "geo_api_rate collection. Exiting...")
            sys.exit(status.value)
        elif status == status.timeout:
            print("Geocoders timed out even after several attempts. Exiting...")
            sys.exit(status.timeout)
        elif status == status.quota:
            print("A geocoder has reached its quota. Exiting...")
            sys.exit(status.quota)

def main():
    """Main entry point function."""
    parser = ArgumentParser(
        description="Import latest service call data " +
        "and geocode until the daily limit is hit or all locations are " +
        "geocoded.")
    parser.add_argument("-c", dest='cfg_db', default='db.ini',
                        help='ini file for configurations. Default is db.ini')
    args = parser.parse_args()

    cfg = ConfigParser()
    cfg.read(args.cfg_db)
    db_streetsavvy_name = cfg['Databases']['streetsavvy']
    coll_pol_name = cfg['Collections']['police_serv_calls']

    db_streetsavvy = MongoClient()[db_streetsavvy_name]
    db_streetsavvy.authenticate(cfg['Accounts']['user_scraper'],
                                cfg['Accounts']['pw_scraper'])
    coll_pol = db_streetsavvy[coll_pol_name]

    # Get latest date imported into the database
    lastdate = coll_pol.find().sort(
        'call_dttm', DESCENDING).limit(1).next()['call_dttm']

    # Import data from the latest date to the current date
    ssimport = SSImportData(args.cfg_db)
    ssimport.importdata(lastdate, datetime.today())

    locations = getloctogeocode(coll_pol)
    geocodelocations(args.cfg_db, locations)

if __name__ == "__main__":
    main()
