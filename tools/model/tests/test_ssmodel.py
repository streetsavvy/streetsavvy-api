import os
import math
from configparser import ConfigParser
from datetime import datetime, timedelta
import pytest
from pymongo import MongoClient, GEOSPHERE
from geopy.distance import vincenty, great_circle
from tools.model.ssmodel import SSModel, MAX_DIST

DB_INI = os.path.join(os.path.dirname(os.path.realpath(__file__)),
                      'test_db.ini')

@pytest.fixture()
def cfg():
    """Return initialized ConfigParser"""
    c = ConfigParser()
    c.read(DB_INI)
    return c

@pytest.fixture()
def model():
    return SSModel(DB_INI)

@pytest.fixture()
def coll_timed_edges(cfg):
    """
        Return a Mongo connection to the timed_edges collection
        that has been cleared
    """
    db_streetsavvy_name = cfg['Databases']['routing']
    coll_timed_edges_name = cfg['Collections']['timed_edges']

    # Make sure database/collection names starts with test so we don't
    # accidentally mess with production servers
    assert db_streetsavvy_name.startswith('test')
    assert coll_timed_edges_name.startswith('test')

    db_streetsavvy = MongoClient()[db_streetsavvy_name]
    # TODO Authentication
    coll_timed_edges = db_streetsavvy[coll_timed_edges_name]
    coll_timed_edges.drop()
    coll_timed_edges.create_index([('loc', GEOSPHERE)])
    assert coll_timed_edges.count() == 0

    return coll_timed_edges

@pytest.fixture()
def coll_police(cfg):
    """
        Return a Mongo connection to the police collection
        that has been cleared
    """
    db_streetsavvy_name = cfg['Databases']['streetsavvy']
    coll_police_name = cfg['Collections']['police_serv_calls']

    # Make sure database/collection names starts with test so we don't
    # accidentally mess with production servers
    assert db_streetsavvy_name.startswith('test')
    assert coll_police_name.startswith('test')

    db_streetsavvy = MongoClient()[db_streetsavvy_name]
    # TODO Authentication
    coll_police = db_streetsavvy[coll_police_name]
    coll_police.drop()
    coll_police.create_index('crime_id', unique=True)
    coll_police.create_index([('loc', GEOSPHERE)])
    assert coll_police.count() == 0

    return coll_police

@pytest.mark.mongo
def test_get_intensity_returns_higher_intensity_near_call_with_higher_weight(
        model, coll_police):
    coll_police.insert_one(
        {
            "city" : "San Francisco",
            "call_date" : datetime(2016, 10, 11, 0, 0, 0),
            "state" : "CA",
            "original_crimetype_name" : "Traffic Stop",
            "call_time" : "18:24",
            "agency_id" : "1",
            "call_dttm" : datetime(2016, 10, 11, 18, 24, 0),
            "crime_id" : "162853299",
            "offense_date" : datetime(2016, 10, 11, 0, 0, 0),
            "disposition" : "HAN",
            "address" : "16th St/potrero Av",
            "report_date" : datetime(2016, 10, 11, 0, 0, 0),
            "address_type" : "Intersection",
            "longitude" : -122.407567,
            "latitude" : 37.7657933,
            "crimetype" : "traffic",
            "weight" : 20,
            "loc" : {
                    "type" : "Point",
                    "coordinates" : [
                            -122.407567,
                            37.7657933
                    ]
            }
        }
    )
    time = datetime(2016, 10, 12, 0, 0, 0)
    val = model.get_intensity((37.7657933, -122.407567), time) # Closer location to point
    val2 = model.get_intensity((37.76558, -122.41056), time) # Further location to point
    val3 = model.get_intensity((37.76429, -122.41039), time) # Even further location to point
    assert val > val2
    assert val2 > val3

@pytest.mark.mongo
def test_get_intensity_returns_lower_intensity_when_later_time_is_given(
        model, coll_police):
    call_dttm = datetime.now() - timedelta(hours=1)
    call_date = datetime(call_dttm.year, call_dttm.month,
                         call_dttm.day)
    coll_police.insert_one(
        {
            "city" : "San Francisco",
            "call_date" : datetime(2016, 10, 11, 0, 0, 0),
            "state" : "CA",
            "original_crimetype_name" : "Traffic Stop",
            "call_time" : "18:24",
            "agency_id" : "1",
            "call_dttm" : datetime(2016, 10, 11, 18, 24, 0),
            "crime_id" : "162853299",
            "offense_date" : datetime(2016, 10, 11, 0, 0, 0),
            "disposition" : "HAN",
            "address" : "16th St/potrero Av",
            "report_date" : datetime(2016, 10, 11, 0, 0, 0),
            "address_type" : "Intersection",
            "longitude" : -122.407567,
            "latitude" : 37.7657933,
            "crimetype" : "traffic",
            "weight" : 20,
            "loc" : {
                    "type" : "Point",
                    "coordinates" : [
                            -122.407567,
                            37.7657933
                    ]
            }
        }
    )
    loc = (37.7643158, -122.40742545)
    val = model.get_intensity(loc, datetime(2016, 10, 11, 19, 0, 0)) # Slightly later time than when call occurred
    val2 = model.get_intensity(loc, datetime(2016, 10, 12, 0, 0, 0)) # Later time
    val3 = model.get_intensity(loc, datetime(2016, 10, 15, 0, 0, 0)) # Even later time
    assert val > val2
    assert val2 > val3

@pytest.mark.mongo
def test_get_intensity_returns_higher_intensity_when_another_call_is_added(
        model, coll_police):
    time = datetime(2016, 10, 12, 0, 0, 0)
    loc = (37.7657933, -122.407567)

    coll_police.insert_one(
        {
            "city" : "San Francisco",
            "call_date" : datetime(2016, 10, 11, 0, 0, 0),
            "state" : "CA",
            "original_crimetype_name" : "Traffic Stop",
            "call_time" : "18:24",
            "agency_id" : "1",
            "call_dttm" : datetime(2016, 10, 11, 18, 24, 0),
            "crime_id" : "162853299",
            "offense_date" : datetime(2016, 10, 11, 0, 0, 0),
            "disposition" : "HAN",
            "address" : "16th St/potrero Av",
            "report_date" : datetime(2016, 10, 11, 0, 0, 0),
            "address_type" : "Intersection",
            "longitude" : -122.407567,
            "latitude" : 37.7657933,
            "crimetype" : "traffic",
            "weight" : 20,
            "loc" : {
                    "type" : "Point",
                    "coordinates" : [
                            -122.407567,
                            37.7657933
                    ]
            }
        }
    )
    val = model.get_intensity(loc, time)

    coll_police.insert_one(
        {
            "city" : "San Francisco",
            "call_date" : datetime(2016, 10, 11, 0, 0, 0),
            "state" : "CA",
            "original_crimetype_name" : "Traffic Stop",
            "call_time" : "18:24",
            "agency_id" : "1",
            "call_dttm" : datetime(2016, 10, 11, 18, 24, 0),
            "crime_id" : "162853300",
            "offense_date" : datetime(2016, 10, 11, 0, 0, 0),
            "disposition" : "HAN",
            "address" : "16th St/potrero Av",
            "report_date" : datetime(2016, 10, 11, 0, 0, 0),
            "address_type" : "Intersection",
            "longitude" : -122.407567,
            "latitude" : 37.7657933,
            "crimetype" : "traffic",
            "weight" : 20,
            "loc" : {
                    "type" : "Point",
                    "coordinates" : [
                            -122.407567,
                            37.7657933
                    ]
            }
        }
    )
    val2 = model.get_intensity(loc, time)

    assert val2 > val

@pytest.mark.parametrize('loc1, loc2', [
    ((37.76580, -122.40757), (37.76558, -122.41056)),
    ((37.7701, -122.4129), (37.7598, -122.4258)),
    ((37.7871, -122.4271), (37.7541, -122.4763)),
    ((37.6934, -122.4889), (37.8040, -122.4134))
])
def test_get_distance_returns_proper_distance_with_coordinates_in_san_francisco(
        loc1, loc2):
    dist = SSModel.get_distance(loc1, loc2)
    vinc_dist = vincenty(loc1, loc2).meters # more accurate distance

    # Check that the distances are close enough
    assert math.isclose(dist, vinc_dist, rel_tol=0.005)
