"""
    StreetSavvy model
"""
import os
import math
import logging
from datetime import timedelta
from configparser import ConfigParser
from geopy.distance import great_circle
from pymongo import MongoClient
import numpy as np
from scipy import spatial

# TEMP See TODO at the get_intensity function
TIME_DECAY_FACTOR = 0.000002
# DIST_DECAY_FACTOR = 0.0015
DIST_DECAY_FACTOR = 0.004

# All service calls within this MAX_DIST distance (in meters) will be
# considered when getting the intensity at a certain location
MAX_DIST = 1000

# All service calls within this MAX_DIST distance will be
# considered when getting the intensity at a certain location
# The units used for this are the raw distance between two
# lat, lon coordinates in degrees
MAX_LAT_LON_DIST = 0.00925418824

EARTH_RADIUS = 6371000 # in meters

# Service calls within this time of the specified model_time
# will be considered (in seconds)
# CALLS_TIME = 86400
CALLS_TIME = 2592000

class SSModel:
    def __init__(self, cfg_db):
        if not os.path.isfile(cfg_db):
            raise ValueError("cfg_db file '" + cfg_db + "' does not exist")
        self.cfg_db = cfg_db
        # self.coll_police = self.get_coll_police()

    # TODO time_decay and dist_decay should be something that is found
    # when service_call data is accessed. Update the dataset so that
    # the decay factors are there rather than having them constant here
    def get_intensity(self, loc, time,
            calls=None,
            time_decay_factor=TIME_DECAY_FACTOR,
            dist_decay_factor=DIST_DECAY_FACTOR):
        """
            loc should be a tuple in the form of (lat, lon)
            time should be a datetime
        """
        # TEMP All these notes about the intensity equation

        # Get the points around the location (Probably timed_edges)
        # Sum up the
        # Equation:
        # severity_weight * (Time Decay) * (Distance Decay)

        # severity_weight is the weight for that particular crime category

        # Time Decay
        # (time - incident_time)^(-time_decay_factor)
        # time is the time variable the intensity is calculated at, usually the current time
        # incident_time is the crime happened
        # time_decay_factor is a constant

        # Distance Decay
        # || y - x_i ||^(-dist_decay_factor)
        # y is the geographic location variable that the intensity is calculated at
        # x_i is the is the geographic location where the crime happened
        # dist_decay_factor is a constant

        if calls is None:
            calls = self.get_calls(loc[0], loc[1], time)
        time_decay_sum = 0
        dist_decay_sum = 0
        sum = 0

        for call in calls:
            time_diff = (time - call['call_dttm']).total_seconds()
            call_time_decay = math.exp(-(time_diff*time_decay_factor))

            call_loc = (call['loc']['coordinates'][1], call['loc']['coordinates'][0])
            dist_diff = SSModel.get_distance(loc, call_loc)
            call_dist_decay = math.exp(-(dist_diff*dist_decay_factor))

            sum += call['weight']*call_time_decay*call_dist_decay
        return sum

    def get_intensity_fast(self, loc, time,
            time_decay_factor=TIME_DECAY_FACTOR,
            dist_decay_factor=DIST_DECAY_FACTOR):
        """Must have tree and call_list members by running setup_fast"""
        if self.call_list is None:
            return 0

        # Get calls in proper distance
        calls_mask = self.tree.query_ball_point(loc, MAX_LAT_LON_DIST)

        time_decay_sum = 0
        dist_decay_sum = 0
        sum = 0

        for i in calls_mask:
            call = self.call_list[i]
            time_diff = (time - call['call_dttm']).total_seconds()
            call_time_decay = math.exp(-(time_diff*time_decay_factor))

            call_loc = (call['loc']['coordinates'][1], call['loc']['coordinates'][0])
            dist_diff = SSModel.get_distance(loc, call_loc)
            call_dist_decay = math.exp(-(dist_diff*dist_decay_factor))

            sum += call['weight']*call_time_decay*call_dist_decay
        return sum

    def get_coll_timed_edges(self):
        """
            Return a Mongo connection to the `timed_edges` collection.
        """
        cfg = ConfigParser()
        cfg.read(self.cfg_db)
        db_streetsavvy_name = cfg['Databases']['routing']
        coll_timed_edges_name = cfg['Collections']['timed_edges']
        db_streetsavvy = MongoClient()[db_streetsavvy_name]
        # TODO Authentication
        return db_streetsavvy[coll_timed_edges_name]

    def get_coll_police(self):
        """
            Return a Mongo connection to the police service calls
            collection.
        """
        cfg = ConfigParser()
        cfg.read(self.cfg_db)
        db_streetsavvy_name = cfg['Databases']['streetsavvy']
        coll_police_name = cfg['Collections']['police_serv_calls']
        db_streetsavvy = MongoClient()[db_streetsavvy_name]
        # TODO Authentication
        return db_streetsavvy[coll_police_name]

    def get_calls(self, lat, lon, time):
        """
            Return a cursor to calls that should be considered when
            calculating intensity. This means calls within a certain
            distance of the given location and calls that occur before or at
            the specified time
        """
        police_collection = self.get_coll_police()
        min_time = time - timedelta(seconds=CALLS_TIME)
        query = {
            'report_date': {'$lte': time, '$gte': min_time},
            'weight': {'$exists': True},
            'loc': {
                '$near': {
                    '$geometry': {'type': 'Point', 'coordinates': [lon, lat]},
                    '$maxDistance': MAX_DIST
                }
            }
        }
        proj = {
            '_id': False,
            'call_dttm': True,
            'loc': True,
            'weight': True
        }
        return police_collection.find(query, proj)

    def get_calls_list(self, time):
        police_collection = self.get_coll_police()
        min_time = time - timedelta(seconds=CALLS_TIME)
        query = {
            'call_dttm': {'$lte': time, '$gte': min_time},
            'weight': {'$gt': 0},
            'loc': {'$exists': True}
        }
        proj = {
            '_id': False,
            'call_dttm': True,
            'loc': True,
            'weight': True
        }
        cursor = police_collection.find(query, proj)
        return list(cursor)

    @staticmethod
    def get_distance(loc1, loc2):
        """Equirectangular approximation of geographical distance"""
        lat1 = math.radians(loc1[0])
        lon1 = math.radians(loc1[1])
        lat2 = math.radians(loc2[0])
        lon2 = math.radians(loc2[1])

        x = (lon2 - lon1) * math.cos(0.5*(lat2+lat1))
        y = lat2 - lat1
        dist = math.sqrt(x*x + y*y)*EARTH_RADIUS
        return dist

    def setup_fast(self, time):
        call_list = self.get_calls_list(time)
        if len(call_list) == 0:
            logging.debug("Call list is empty. Setting call_list and tree to None")
            self.call_list = None
            self.tree = None
            return
        self.call_list = call_list
        logging.debug("Creating raw data array from call_list")
        data = []
        for call in call_list:
            call_loc = (call['loc']['coordinates'][1], call['loc']['coordinates'][0])
            data.append(call_loc)
        logging.debug("Creating numpy version of array from raw data array")
        data_arr = np.array(data)
        logging.debug("Creating KDTree")
        self.tree = spatial.cKDTree(data_arr)
