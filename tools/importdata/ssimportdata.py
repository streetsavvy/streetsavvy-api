"""
    Module to import data from an external API into the internal
    MongoDB instance.
"""
# Standard Libs
from configparser import ConfigParser
from datetime import datetime, timedelta
from urllib import request
import json
import os

# 3rd Party Libs
from pymongo import MongoClient
from pymongo.errors import InvalidOperation

class SSImportData:
    """
        Class to import data from various APIs and sources for
        StreetSavvy. This includes police and fire service call data
        from SF OpenData. Users should use the `importdata` function
        to import all data between the specified dates into the
        database.
    """

    def __init__(self, cfg_db='db.ini'):
        if not os.path.isfile(cfg_db):
            raise ValueError("cfg_db file '" + cfg_db + "' does not exist")
        self.cfg_db = cfg_db

    def importdata(self, startdate, enddate, days=15):
        """
            Import the police servica call data and fire service call
            data into the database from SFOpen data between the
            specified `startdate` and `enddate`. `startdate` and
            `enddate` should be Python `datetime` objects. An optional
            `days` parameter can be used to specify how many days of
            data should be grabbed at once from the data source.
        """
        date = startdate
        todate = startdate
        while date < enddate:
            fromdate = date
            todate += timedelta(days)

            print("Grabbing data between " + str(fromdate) +
                  " and " + str(todate))
            self.importpolicedata(fromdate, todate)
            self.importfiredata(fromdate, todate)

            date += timedelta(days)

    def importpolicedata(self, startdate, enddate):
        """
            Import police service call data between the specified
            dates. `startdate` and `enddate` should be Python `datetime`
            objects.
        """
        police_json = self.getpolicejson(startdate, enddate)
        self.importpoliceintodb(police_json)

    def importfiredata(self, startdate, enddate):
        """
            Import fire service call data between the specified dates.
            `startdate` and `enddate` should be Python `datetime`
            objects.
        """
        fire_json = self.getfirejson(startdate, enddate)
        self.importfireintodb(fire_json)

    def getpolicejson(self, startdate, enddate):
        """
            Get json of SF Open Data Police Calls for Service between
            the specified dates. `startdate` and `enddate` should be
            Python `datetime` objects.
        """
        cfg = ConfigParser()
        cfg.read(self.cfg_db)
        url = ("https://data.sfgov.org/resource/fjjd-jecq.json?" +
               "$where=call_dttm%20between%20'" +
               startdate.strftime('%Y-%m-%dT%H:%M:%S') + "'%20and%20'" +
               enddate.strftime('%Y-%m-%dT%H:%M:%S') +
               "'&$limit=50000&$$app_token=" + cfg['AppTokens']['Socrata'])
        response = request.urlopen(url)
        data = json.loads(response.read().decode('utf-8'))
        return data

    def getfirejson(self, startdate, enddate):
        """
            Get json of Fire Department Calls for Service between the
            specified dates. `startdate` and `enddate` should be Python
            `datetime` objects.
        """
        cfg = ConfigParser()
        cfg.read(self.cfg_db)
        url = ("https://data.sfgov.org/resource/enhu-st7v.json?" +
               "$where=received_dttm%20between%20'" +
               startdate.strftime('%Y-%m-%dT%H:%M:%S') + "'%20and%20'" +
               enddate.strftime('%Y-%m-%dT%H:%M:%S') +
               "'&$limit=50000&$$app_token=" + cfg['AppTokens']['Socrata'])
        response = request.urlopen(url)
        data = json.loads(response.read().decode('utf-8'))
        return data

    def importpoliceintodb(self, police_json):
        """
            Import the specified police JSON dataset into the database.
            Make sure we don't add duplicate records by doing an
            upsert based on `crime_id` for SF OpenData. Convert string
            dates to `datetime` objects.
        """
        cfg = ConfigParser()
        cfg.read(self.cfg_db)
        db_streetsavvy_name = cfg['Databases']['streetsavvy']
        coll_police_name = cfg['Collections']['police_serv_calls']

        db_streetsavvy = MongoClient()[db_streetsavvy_name]
        db_streetsavvy.authenticate(cfg['Accounts']['user_scraper'],
                                    cfg['Accounts']['pw_scraper'])
        coll_police = db_streetsavvy[coll_police_name]

        # Convert date strings to Python datetime objects so that
        # MongoDB imports them as datetime objects
        bulk = coll_police.initialize_unordered_bulk_op()
        for item in police_json:
            if not SSImportData.isvalidpolice(item):
                continue

            item['call_date'] = datetime.strptime(item['call_date'],
                                                  '%Y-%m-%dT%H:%M:%S.%f')
            item['call_dttm'] = datetime.strptime(item['call_dttm'],
                                                  '%Y-%m-%dT%H:%M:%S.%f')
            item['offense_date'] = datetime.strptime(item['offense_date'],
                                                     '%Y-%m-%dT%H:%M:%S.%f')
            item['report_date'] = datetime.strptime(item['report_date'],
                                                    '%Y-%m-%dT%H:%M:%S.%f')
            bulk.find({'crime_id': item['crime_id']}).upsert(
                ).update({'$set': item})
        SSImportData.safe_bulk_exec(bulk)

    def importfireintodb(self, fire_json):
        """
            Import the specified fire JSON dataset into the database.
            Make sure we don't add duplicate records by doing an
            upsert based on `rowid` for SF OpenData. Convert string
            dates to `datetime` objects.
        """
        cfg = ConfigParser()
        cfg.read(self.cfg_db)
        db_streetsavvy_name = cfg['Databases']['streetsavvy']
        coll_fire_name = cfg['Collections']['fire_serv_calls']

        db_streetsavvy = MongoClient()[db_streetsavvy_name]
        db_streetsavvy.authenticate(cfg['Accounts']['user_scraper'],
                                    cfg['Accounts']['pw_scraper'])
        coll_fire = db_streetsavvy[coll_fire_name]

        # Convert date strings to Python datetime objects so that
        # MongoDB imports them as datetime objects
        bulk = coll_fire.initialize_unordered_bulk_op()
        for item in fire_json:
            if not SSImportData.isvalidfire(item):
                continue

            item['call_date'] = datetime.strptime(
                item['call_date'], '%Y-%m-%dT%H:%M:%S.%f')
            item['watch_date'] = datetime.strptime(
                item['watch_date'], '%Y-%m-%dT%H:%M:%S.%f')
            item['received_dttm'] = datetime.strptime(
                item['received_dttm'], '%Y-%m-%dT%H:%M:%S.%f')
            item['entry_dttm'] = datetime.strptime(
                item['entry_dttm'], '%Y-%m-%dT%H:%M:%S.%f')
            item['dispatch_dttm'] = datetime.strptime(
                item['dispatch_dttm'], '%Y-%m-%dT%H:%M:%S.%f')

            if 'response_dttm' in item:
                item['response_dttm'] = datetime.strptime(
                    item['response_dttm'], '%Y-%m-%dT%H:%M:%S.%f')
            if 'on_scene_dttm' in item:
                item['on_scene_dttm'] = datetime.strptime(
                    item['on_scene_dttm'], '%Y-%m-%dT%H:%M:%S.%f')
            if 'transport_dttm' in item:
                item['transport_dttm'] = datetime.strptime(
                    item['transport_dttm'], '%Y-%m-%dT%H:%M:%S.%f')
            if 'hospital_dttm' in item:
                item['hospital_dttm'] = datetime.strptime(
                    item['hospital_dttm'], '%Y-%m-%dT%H:%M:%S.%f')
            if 'available_dttm' in item:
                item['available_dttm'] = datetime.strptime(
                    item['available_dttm'], '%Y-%m-%dT%H:%M:%S.%f')
            bulk.find({'rowid': item['rowid']}).upsert(
                ).update({'$set': item})
        SSImportData.safe_bulk_exec(bulk)

    @staticmethod
    def isvalidpolice(police_record):
        # Check if the following keys are inside the record
        keys = ('call_date', 'call_dttm', 'offense_date', 'report_date',
                'crime_id')
        if all (k in police_record for k in keys):
            return True
        else:
            print("Error: Found invalid police record: {0}".format(
                police_record))
            return False

    @staticmethod
    def isvalidfire(fire_record):
        # Check if the following keys are inside the record
        keys = ('call_date', 'watch_date', 'received_dttm', 'entry_dttm',
                'dispatch_dttm', 'rowid')
        if all (k in fire_record for k in keys):
            return True
        else:
            print("Error: Found invalid fire record: {0}".format(
                fire_record))
            return False

    @staticmethod
    def safe_bulk_exec(bulk):
        """
            Safely execute bulk.execute so that when there are no operations
            to execute, no exceptions are thrown
        """
        try:
            bulk.execute()
        except InvalidOperation as exc:
            if exc.args[0] != 'No operations to execute':
                raise exc
