"""Tests for ssimportdata"""
# pylint: disable=redefined-outer-name

# Standard Libs
import os
from configparser import ConfigParser
from datetime import datetime, timedelta
from timeit import Timer

# 3rd Party Libs
import pytest
from pymongo import MongoClient, DESCENDING

# Internal Libs
from tools.importdata.ssimportdata import SSImportData

DB_INI = os.path.join(os.path.dirname(os.path.realpath(__file__)),
                      'test_db.ini')

@pytest.fixture()
def coll_police():
    """
        Return a Mongo connection to the police service call data
    """
    cfg = ConfigParser()
    cfg.read(DB_INI)
    db_streetsavvy_name = cfg['Databases']['streetsavvy']
    coll_police_name = cfg['Collections']['police_serv_calls']

    # Make sure database/collection names starts with test so we don't
    # accidentally mess with production servers
    assert db_streetsavvy_name.startswith('test')
    assert coll_police_name.startswith('test')

    db_streetsavvy = MongoClient()[db_streetsavvy_name]
    db_streetsavvy.authenticate(cfg['Accounts']['user_scraper'],
                                cfg['Accounts']['pw_scraper'])
    coll_police = db_streetsavvy[coll_police_name]
    coll_police.drop() # Delete data for testing
    coll_police.create_index('crime_id', unique=True)

    # Make sure data was actually deleted
    assert coll_police.count() == 0

    return coll_police

@pytest.fixture()
def coll_fire():
    """
        Return a Mongo connection to the fire service call data
    """
    cfg = ConfigParser()
    cfg.read(DB_INI)
    db_streetsavvy_name = cfg['Databases']['streetsavvy']
    coll_fire_name = cfg['Collections']['fire_serv_calls']

    # Make sure database/collection names starts with test so we don't
    # accidentally mess with production servers
    assert db_streetsavvy_name.startswith('test')
    assert coll_fire_name.startswith('test')

    db_streetsavvy = MongoClient()[db_streetsavvy_name]
    db_streetsavvy.authenticate(cfg['Accounts']['user_scraper'],
                                cfg['Accounts']['pw_scraper'])
    coll_fire = db_streetsavvy[coll_fire_name]
    coll_fire.drop() # Delete data for testing
    coll_fire.create_index('rowid', unique=True)

    assert coll_fire.count() == 0 # Make sure data was actually deleted

    return coll_fire

def mock_getpolicejson(obj, startdate, enddate):
    return [
        {"original_crimetype_name" : "\"7,2,41//dw\"",
         "crimetype" : "n/a", "weight" : 0 }
    ]

def mock_getfirejson(obj, startdate, enddate):
    return [{
        'rowid': '1'
    }]

@pytest.mark.sfopendata
@pytest.mark.mongo
def test_importdata(coll_police, coll_fire):
    """
        Test if data can be imported without error and that data is
        in the proper format
    """
    startdoccount_police = coll_police.count()
    startdoccount_fire = coll_fire.count()

    ssimport = SSImportData(DB_INI)
    startdate = datetime(2016, 10, 1)
    enddate = datetime(2016, 10, 2)
    ssimport.importdata(startdate, enddate)

    enddoccount_police = coll_police.count()
    enddoccount_fire = coll_fire.count()

    # Make sure data was imported
    assert startdoccount_police != enddoccount_police
    assert startdoccount_fire != enddoccount_fire

    # Assert other expectiations of the data
    # Note: These assertions are dependent on SF OpenData not adding old
    # service call data with dates between 2016-10-01 to 2016-10-02.
    assert enddoccount_police == 36751
    assert enddoccount_fire == 13124

    result = coll_police.find_one(sort=[('crime_id', DESCENDING)],
                                  projection=['crime_id'])
    last_crimeid = result['crime_id']
    assert last_crimeid == '162900009'

    # Check fields to make sure police data is in the correct format
    doc = coll_police.find_one({})
    assert isinstance(doc['crime_id'], str)
    assert isinstance(doc['disposition'], str)
    assert isinstance(doc['offense_date'], datetime)
    assert isinstance(doc['call_date'], datetime)

    # Check fields to make sure fire data is in the correct format
    doc = coll_fire.find_one({})
    assert isinstance(doc['rowid'], str)
    assert isinstance(doc['call_number'], str)
    assert isinstance(doc['call_date'], datetime)
    assert isinstance(doc['received_dttm'], datetime)

@pytest.mark.mongo
def test_importdata_doesnt_import_incomplete_police_records(
        monkeypatch, coll_police, coll_fire):
    ssimport = SSImportData(DB_INI)
    startdate = datetime(2016, 10, 1)
    enddate = datetime(2016, 10, 2)

    monkeypatch.setattr(
        'tools.importdata.ssimportdata.SSImportData.getpolicejson',
        mock_getpolicejson)
    monkeypatch.setattr(
        'tools.importdata.ssimportdata.SSImportData.getfirejson',
        mock_getfirejson)
    ssimport.importdata(startdate, enddate)

    assert coll_police.count() == 0
    assert coll_fire.count() == 0

@pytest.mark.stress
@pytest.mark.sfopendata
@pytest.mark.mongo
def test_large_importdata(coll_police, coll_fire):
    """
        Test if a large dataset can be imported without error and that
        the imported dataset has data across the specified date range
    """
    startdoccount_police = coll_police.count()
    startdoccount_fire = coll_fire.count()

    # pylint: disable=unused-variable
    ssimport = SSImportData(DB_INI)
    # pylint: enable=unused-variable
    startdate = datetime(2016, 11, 1)
    enddate = startdate + timedelta(days=15)

    # Makes sure the import process takes a reasonable amount of time.
    # 300 s = 5 minutes
    timer = Timer('ssimport.importdata(startdate, enddate)', globals=locals())
    assert timer.timeit(1) < 300

    enddoccount_police = coll_police.count()
    enddoccount_fire = coll_fire.count()

    # Make sure data was imported
    assert startdoccount_police != enddoccount_police
    assert startdoccount_fire != enddoccount_fire

    # Assert other expectiations of the data
    # Note: These assertions are dependent on SF OpenData not adding old
    # service call data with dates between 2016-11-01 to 2016-11-16.
    # TODO Check these numbers. As of 4/3/2017, they don't work
    # assert enddoccount_police == 71582
    # assert enddoccount_fire == 26165

    result = coll_police.find_one(sort=[('crime_id', DESCENDING)],
                                  projection=['crime_id'])
    last_crimeid = result['crime_id']
    assert last_crimeid == '163210005'

    # Loop over the date range where we imported data
    date = datetime(2016, 11, 1)
    enddate = date + timedelta(days=15)
    while date < enddate:
        assert coll_police.count({'call_date': date}) != 0
        assert coll_fire.count({'call_date': date}) != 0
        date += timedelta(days=1)
