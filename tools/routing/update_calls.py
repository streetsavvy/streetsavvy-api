"""
   :platform: Unix
   :synopsis:  TBD 

.. moduleauthor:: Lauren Slason <lauren.slason@gmail.com>


"""

import re
import sys
import logging
import argparse
import pprint
import itertools
import datetime
import configparser
from collections import defaultdict
from copy import deepcopy
from pymongo import MongoClient
from geopy.distance import vincenty

def hash_nodes(c_nodes):
    nodes_cursor = c_nodes.find({}, {"_id":0})
    nodes = dict()
    for node in nodes_cursor:
        coordinate = (node['loc']['coordinates'][1], node['loc']['coordinates'][0])
        nodes[coordinate] = node['id']
    return nodes

def find_closest_node(point, nodes):
    min_distance = sys.maxsize
    min_node = None
    for n in nodes:
        distance = vincenty(n, point).meters
        if distance < min_distance:
            logging.info("MSG=New Minimum, MIN=%d, NODE=%s", distance, str(n))
            min_distance = distance
            min_node = n
    return min_distance, min_node

def find_closest_mongo(node_coll, lon, lat, min_dist, max_dist):
    query = {
        "loc": {
            '$near': {
                '$geometry': {'type': 'Point', 'coordinates': [lon, lat]},
                '$minDistance': min_dist,
                '$maxDistance': max_dist
            }
        }
    }
    return node_coll.find(query).limit(1)

def find_all_edges(segment_coll, node_id):
    segments_one = list(segment_coll.find({"node_1":node_id}))
    segments_two = list(segment_coll.find({"node_2":node_id}))
    return segments_one + segments_two

def hash_centroids(edges):
    segments = dict()
    for e in edges:
        coord = e['loc']['coordinates']
        centroid = (coord[1], coord[0])
        segments[centroid] = e
    return segments

def main():
    # Set configuration
    parser = argparse.ArgumentParser(
        description='Provide configuration details for the API')
    parser.add_argument('-c', type=str, dest='config', default='update.cfg')
    parser.add_argument('-l', type=str, dest='log', default='update_calls')
    args = parser.parse_args()
    
    # Parse the configuration
    config = configparser.ConfigParser()
    config.read(args.config)

    # Initialize logging
    log_date = datetime.datetime.now()
    log_date = log_date.strftime("%m.%d.%Y_%H.%M.%S")
    log_file = '%s_%s.log' % (args.log, log_date)
    logging.basicConfig(filename=log_file,level=logging.DEBUG)
    
    # Open connection with the MongoDB database
    client = MongoClient(config['mongo']['host'], int(config['mongo']['port']))
    db = client[config['mongo']['source']]
    #db.authenticate(name=config['mongo']['username'], 
    #    password=config['mongo']['password'], source=config['mongo']['source'])
    db_police = client[config['mongo']['source_service']]
    db_police.authenticate(name=config['mongo']['username_service'],
        password=config['mongo']['password_service'], source=config['mongo']['source_service'])

    c_nodes = db[config['mongo']['node_coll']]
    c_segments = db[config['mongo']['segment_coll']]
    c_timed_edges = db[config['mongo']['timed_segment_coll']]

    now = datetime.datetime.now()
    difference = now - datetime.timedelta(days=1)
    events = list()
    closest_point_cache = dict()
    # Find all the service call events
    # TODO: Allow users to specify a datetime to limit the scope of this query
    c_police = db_police[config['mongo']['police_coll']]
    police_cursor = c_police.find(
        {"call_dttm":{"$gte":difference}, "latitude":{"$exists":True},
        "longitude":{"$exists":True}, "weight":{"$exists":True}},
        {"call_dttm":1, "_id":1, "longitude":1, "latitude":1,
        "weight":1, "crimetype":1})
    # Loop through all the call events
    count = 0
    t1 = datetime.datetime.now()
    for call in police_cursor:
        # Add a check to verify that this has already been quantified in the DB
        call_node = (call["latitude"], call["longitude"])
        if call_node in closest_point_cache.keys():
            impacted= closest_point_cache[call_node]
        else:
            # Find the closest point
            result = list(find_closest_mongo(c_nodes, call_node[1], call_node[0], 0, 800))
            if not result:
                logging.error("MSG=Identified Node Outside of SF, LOC=%s, ID=%s", 
                    str(call_node), str(call['_id']))
                closest_point_cache[call_node] = list()
                print(call)
                continue
            closest_node = list(result)[0]
            closest_coord = (closest_node['loc']['coordinates'][1], closest_node['loc']['coordinates'][0])
            node_distance = vincenty(call_node, closest_coord).miles
            closest_id = closest_node['id']
            # closest_point_cache[call_node] = (node_distance, closest_id)
            logging.info('MSG=Calculated min node distance, CALL=%s, POINT=%d, DISTANCE=%d',
                str(call["_id"]), closest_id, node_distance)
            # if closest_coord == (37.7483939, -122.4096701):
            #     print(call_node, node_distance, closest_coord, closest_id)

            # Identify all of the edges
            r_centroid = find_closest_mongo(c_segments, call_node[1], call_node[0], 0, 800)
            centroid = list(r_centroid)[0]
            closest_centroid = (centroid['loc']['coordinates'][1], centroid['loc']['coordinates'][0])
            centroid_distance = vincenty(call_node, closest_coord).miles
            logging.info('MSG=Found min centroid distance, CALL=%s, CENTROID=%s, DISTANCE=%d',
                str(call["_id"]), str(closest_centroid), centroid_distance)
            # If the centroid is the closest point, add only the centroid
            if centroid_distance < node_distance:
                logging.info("MSG=Intersection is closest, CALL=%s", str(call["_id"]))
                impacted = [ centroid]
            # If the intersection is the closest point, add all the impacted edges
            else: 
                logging.info("MSG=Intersection is closest, CALL=%s", str(call["_id"]))
                impacted = find_all_edges(c_segments, closest_id)
            logging.info("MSG=Identified Edges, LEN=%d", len(impacted))
            closest_point_cache[call_node] = impacted
        for edge in impacted:
            doc = {"timestamp":call["call_dttm"], "weight":call["weight"],
                "call_id":call["_id"], "node_1":edge["node_1"],
                "node_2":edge["node_2"], "distance":edge["distance"],
                "loc":edge["loc"]}
            events.append(doc)
            logging.info('MSG=Added documents identifying impacted points, LEN=%d',
                len(impacted))
        count = count + 1
        # print(count)
        if divmod(count, 1000)[1] == 0:
             #print(datetime.datetime.now()-t1)
             #print(count)
             # sys.exit()
             c_timed_edges.insert_many(events)
             events = list()
             #t1 = datetime.datetime.now()

    if len(events) != 0:
        c_timed_edges.insert_many(events)

if __name__ == '__main__':
    main()
