"""
   :platform: Unix
   :synopsis:  Verify that there are no segments missing from the timed edges 

.. moduleauthor:: Lauren Slason <lauren.slason@gmail.com>


"""

import re
import sys
import logging
import argparse
import pprint
import itertools
import datetime
import configparser
from collections import defaultdict
from copy import deepcopy
from pymongo import MongoClient
from geopy.distance import vincenty

def find_closest_mongo(node_coll, query, lon, lat, min_dist, max_dist):
    query_loc = {
        "loc": {
            '$near': {
                '$geometry': {'type': 'Point', 'coordinates': [lon, lat]},
                '$minDistance': min_dist,
                '$maxDistance': max_dist
            }
        }
    }
    query.update(query_loc)
    return list(node_coll.find(query, {"_id":0}).limit(1))


if __name__ == '__main__':
    # Set configuration
    parser = argparse.ArgumentParser(
        description='Provide configuration details for the API')
    parser.add_argument('-c', type=str, dest='config', default='update.cfg')
    parser.add_argument('-l', type=str, dest='log', default='verify_segments')
    args = parser.parse_args()
    
    # Parse the configuration
    config = configparser.ConfigParser()
    config.read(args.config)

    # Initialize logging
    log_date = datetime.datetime.now()
    log_date = log_date.strftime("%m.%d.%Y_%H.%M.%S")
    log_file = '%s_%s.log' % (args.log, log_date)
    logging.basicConfig(filename=log_file,level=logging.DEBUG)
    
    # Open connection with the MongoDB database
    client = MongoClient(config['mongo']['host'], int(config['mongo']['port']))
    db = client[config['mongo']['source']]
    #db.authenticate(name=config['mongo']['username'], 
    #    password=config['mongo']['password'], source=config['mongo']['source'])

    c_nodes = db[config['mongo']['node_coll']]
    c_segments = db[config['mongo']['segment_coll']]
    c_timed_edges = db[config['mongo']['timed_segment_coll']]
    c_snapshot = db[config['mongo']['snapshot_coll']]

    # Determine if there are any new segments in the segments collection not in the timed_edges collection
    segment_cursor = c_segments.find({})
    for segment in segment_cursor:
        results = find_closest_mongo(c_timed_edges, 
            {"timestamp":{"$exists":False}}, segment['loc']['coordinates'][0],
            segment['loc']['coordinates'][1], 0, 0)
        # Found a match for centroid.
        if results:
            # Log if the details are not the same
            if (results[0]['node_1'] == segment['node_1'] and 
                results[0]['node_2'] == segment['node_2'] and 
                results[0]['distance'] == segment['distance']):
                found = True
                logging.info("MSG=Segment found in timed edges, NODE_1=%s, NODE_2=%s",
                    segment['node_1'], segment['node_2'])
            else:
                found = False
                logging.warning('MSG=Segment has mismatched date for a centroid, '
                    'NODE_1=%s, NODE_2=%s, TIMED_1=%s, TIMED_2=%s',
                    segment['node_1'], segment['node_2'],
                    results[0]['node_1'], results[0]['node_2'])
        else:
            found = False
            logging.info("MSG=No segment found in timed edges, NODE_1=%s, NODE_2=%s",
                segment['node_1'], segment['node_2'])
        if not found:
            segment['weight'] = 0
            segment['call_id'] = 'n/a'
            c_timed_edges.insert(segment)
            logging.info("MSG=Inserted missing segment, NODE_1=%s, NODE_2=%s",
                segment['node_1'], segment['node_2'])
