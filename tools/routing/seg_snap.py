import logging
from argparse import ArgumentParser
from configparser import ConfigParser
from datetime import datetime, timedelta
from pymongo import MongoClient, GEOSPHERE
from tools.model.ssmodel import SSModel

# Bounds where segment weights will be evalulated
BOUNDS = [-122.5319, 37.7007, -122.3506, 37.8163]

INSERT_LIMIT = 5000

def recreate_coll(coll_seg_snap):
    coll_seg_snap.drop()
    coll_seg_snap.create_index([('loc', GEOSPHERE)])

def get_segments(coll_segments, bounds):
    """Return a cursor to the desired segments"""
    cursor = coll_segments.find(
        {
            'loc': {
                '$geoWithin': {
                    '$box': [
                        [bounds[0], bounds[1]],
                        [bounds[2], bounds[3]]
                    ]
                }
            }
        },
        {'_id': False}
    )
    return cursor

def create_doc(seg_doc, intensity):
    """Create the doc that will be inserted into seg_snap"""
    doc = seg_doc.copy()
    doc['weight'] = intensity
    return doc

def generate_seg_snap(model, coll_seg_snap, coll_segments,
        model_time=datetime.now(), bounds=BOUNDS):
    """Create the seg_snap collection"""
    logging.info("Creating the segments snapshot collection")
    recreate_coll(coll_seg_snap)

    model.setup_fast(model_time)

    cursor = get_segments(coll_segments, bounds)
    docs = []
    counter = 0
    seg_count = cursor.count()
    logging.debug("Number of segments: %d", seg_count)
    for counter, seg in enumerate(cursor):
        loc = (seg['loc']['coordinates'][1], seg['loc']['coordinates'][0])
        intensity = model.get_intensity_fast(loc, model_time)
        doc = create_doc(seg, intensity)
        docs.append(doc)

        if seg_count >= 10 and counter % int(seg_count/10) == 0:
            logging.debug("At segment doc: %d", counter)

        if counter != 0 and counter % INSERT_LIMIT == 0:
            logging.debug("Bulk inserting")
            coll_seg_snap.insert_many(docs)
            docs = []

    if len(docs) != 0:
        coll_seg_snap.insert_many(docs)

def main():
    parser = ArgumentParser(
        description="Get the segments collection weighted")
    parser.add_argument("-c", dest='cfg_db',
                        help='ini file for configurations.')
    parser.add_argument(
        '--geo', nargs='+', dest='geo_bounds', default=BOUNDS,
        help="Geographical bounds for heatmap generation in the form of: " +
             "left bottom right top bounds")
    args = parser.parse_args()

    fmt = "%(name)s:%(asctime)s:%(levelname)s:%(funcName)s:%(message)s"
    logging.basicConfig(level=logging.DEBUG, format=fmt)
    logging.info("Starting weight_segments.py")

    cfg = ConfigParser()
    cfg.read(args.cfg_db)

    model = SSModel(args.cfg_db)

    db_routing = MongoClient()[cfg['Databases']['routing']]
    coll_seg_snap = db_routing[cfg['Collections']['seg_snap']]
    coll_segments = db_routing[cfg['Collections']['segments']]

    #model_time = datetime.now() - timedelta(days=20)
    model_time = datetime(2017, 4, 15)
    generate_seg_snap(model, coll_seg_snap, coll_segments, model_time,
                      args.geo_bounds)

if __name__ == "__main__":
    main()
