import os
from configparser import ConfigParser
from datetime import datetime
import pytest
from pymongo import MongoClient, ASCENDING, GEOSPHERE
from tools.routing.seg_snap import generate_seg_snap
from tools.model.ssmodel import SSModel

DB_INI = os.path.join(os.path.dirname(os.path.realpath(__file__)),
                      'test_db.ini')
SEGMENTS_FILE = os.path.join(os.path.dirname(os.path.realpath(__file__)),
                             'segments_seg_snap.json')

@pytest.fixture()
def cfg():
    """Return initialized ConfigParser"""
    c = ConfigParser()
    c.read(DB_INI)
    return c

@pytest.fixture()
def model():
    return SSModel(DB_INI)

@pytest.fixture()
def coll_police(cfg):
    """
        Return a Mongo connection to the police collection
        that has been populated
    """
    db_streetsavvy_name = cfg['Databases']['streetsavvy']
    coll_police_name = cfg['Collections']['police_serv_calls']

    # Make sure database/collection names starts with test so we don't
    # accidentally mess with production servers
    assert db_streetsavvy_name.startswith('test')
    assert coll_police_name.startswith('test')

    db_streetsavvy = MongoClient()[db_streetsavvy_name]
    # TODO Authentication
    coll_police = db_streetsavvy[coll_police_name]
    coll_police.drop()
    coll_police.create_index('crime_id', unique=True)
    coll_police.create_index([('loc', GEOSPHERE)])
    assert coll_police.count() == 0

    return coll_police

@pytest.fixture()
def coll_segments(cfg):
    """
        Return a Mongo connection to the segments collection
        that has cleared
    """
    db_routing_name = cfg['Databases']['routing']
    coll_segments_name = cfg['Collections']['segments']

    # Make sure database/collection names starts with test so we don't
    # accidentally mess with production servers
    assert db_routing_name.startswith('test')
    assert coll_segments_name.startswith('test')

    db_routing = MongoClient()[db_routing_name]
    # TODO authentication
    #db_routing.authenticate(cfg['Accounts'][''],
                                #cfg['Accounts'][''])
    coll_segments = db_routing[coll_segments_name]
    coll_segments.drop()
    coll_segments.create_index([('node_1', ASCENDING), ('node_2', ASCENDING)],
                               unique=True)
    coll_segments.create_index([('loc', GEOSPHERE)])
    assert coll_segments.count() == 0

    return coll_segments

@pytest.fixture()
def coll_seg_snap(cfg):
    """
        Return a Mongo connection to the seg_snap collection
    """
    db_routing_name = cfg['Databases']['routing']
    coll_seg_snap_name = cfg['Collections']['seg_snap']

    # Make sure database/collection names starts with test so we don't
    # accidentally mess with production servers
    assert db_routing_name.startswith('test')
    assert coll_seg_snap_name.startswith('test')

    db_routing = MongoClient()[db_routing_name]
    # TODO authentication
    #db_routing.authenticate(cfg['Accounts'][''],
                                #cfg['Accounts'][''])
    coll_seg_snap = db_routing[coll_seg_snap_name]
    coll_seg_snap.drop()
    coll_seg_snap.create_index([('loc', GEOSPHERE)])
    assert coll_seg_snap.count() == 0

    return coll_seg_snap

@pytest.mark.mongo
def test_generate_seg_snap_recreates_seg_snap_collection(coll_police,
        coll_segments, coll_seg_snap, model):
    coll_seg_snap.drop()
    coll_seg_snap.insert_one(
        {
            'dummy': 'dummy_value'
        }
    )
    cursor = coll_seg_snap.find({'dummy': {'$exists': True}})
    assert cursor.count() == 1
    indexes = coll_seg_snap.index_information()
    assert 'loc_2dsphere' not in indexes

    coll_segments.insert_one(
        {
            "node_2" : 3999495551,
            "distance" : 37.5953652125266,
            "loc" : {
                    "type" : "Point",
                    "coordinates" : [
                            -122.4075838,
                            37.76594985
                    ]
            },
            "node_1" : 65308767
        }
    )
    coll_segments.insert_one(
        {
            "node_2" : 65308767,
            "distance" : 49.390006571965515,
            "loc" : {
                    "type" : "Point",
                    "coordinates" : [
                            -122.4075444,
                            37.765559249999995
                    ]
            },
            "node_1" : 3999497936
        }
    )
    generate_seg_snap(model, coll_seg_snap, coll_segments)

    cursor = coll_seg_snap.find({'dummy': {'$exists': True}})
    assert cursor.count() == 0

    # Make sure the geospatial index was created
    indexes = coll_seg_snap.index_information()
    assert 'loc_2dsphere' in indexes
    assert 'key' in indexes['loc_2dsphere']
    assert indexes['loc_2dsphere']['key'] == [('loc', '2dsphere')]

@pytest.mark.mongo
def test_generate_seg_snap_creates_doc_for_each_seg(coll_police,
        coll_segments, coll_seg_snap, model):
    coll_segments.insert_one(
        {
            "node_2" : 3999495551,
            "distance" : 37.5953652125266,
            "loc" : {
                    "type" : "Point",
                    "coordinates" : [
                            -122.4075838,
                            37.76594985
                    ]
            },
            "node_1" : 65308767
        }
    )
    coll_segments.insert_one(
        {
            "node_2" : 65308767,
            "distance" : 49.390006571965515,
            "loc" : {
                    "type" : "Point",
                    "coordinates" : [
                            -122.4075444,
                            37.765559249999995
                    ]
            },
            "node_1" : 3999497936
        }
    )
    generate_seg_snap(model, coll_seg_snap, coll_segments)

    assert coll_seg_snap.count() == 2
    loc = {
        "type" : "Point",
        "coordinates" : [
                -122.4075838,
                37.76594985
        ]
    }
    assert coll_seg_snap.find({'loc': loc}).count() == 1
    loc = {
        "type" : "Point",
        "coordinates" : [
                -122.4075444,
                37.765559249999995
        ]
    }
    assert coll_seg_snap.find({'loc': loc}).count() == 1

@pytest.mark.mongo
def test_generate_seg_snap_creates_docs_with_weights_higher_around_ares_with_service_call_weights(
        coll_police, coll_segments, coll_seg_snap, model):
    coll_segments.insert_one(
        {
            "node_2" : 3999495551,
            "distance" : 37.5953652125266,
            "loc" : {
                    "type" : "Point",
                    "coordinates" : [
                            -122.4075838,
                            37.76594985
                    ]
            },
            "node_1" : 65308767
        }
    )
    coll_segments.insert_one(
        {
            "node_2" : 65308767,
            "distance" : 49.390006571965515,
            "loc" : {
                    "type" : "Point",
                    "coordinates" : [
                            -122.4075444,
                            37.765559249999995
                    ]
            },
            "node_1" : 3999497936
        }
    )
    time = datetime(2016, 10, 12, 0, 0, 0)

    generate_seg_snap(model, coll_seg_snap, coll_segments, time)
    assert coll_seg_snap.count() == 2
    # Record the weights in seg_snap before a service call with weight
    loc = {
        "type" : "Point",
        "coordinates" : [
                -122.4075838,
                37.76594985
        ]
    }
    weight_1_1 = coll_seg_snap.find_one({'loc': loc})['weight']
    loc = {
        "type" : "Point",
        "coordinates" : [
                -122.4075444,
                37.765559249999995
        ]
    }
    weight_1_2 = coll_seg_snap.find_one({'loc': loc})['weight']

    # Add a call with weight and evaluate seg_snap and weights
    coll_police.insert_one(
        {
            "city" : "San Francisco",
            "call_date" : datetime(2016, 10, 11, 0, 0, 0),
            "state" : "CA",
            "original_crimetype_name" : "Traffic Stop",
            "call_time" : "18:24",
            "agency_id" : "1",
            "call_dttm" : datetime(2016, 10, 11, 18, 24, 0),
            "crime_id" : "162853299",
            "offense_date" : datetime(2016, 10, 11, 0, 0, 0),
            "disposition" : "HAN",
            "address" : "16th St/potrero Av",
            "report_date" : datetime(2016, 10, 11, 0, 0, 0),
            "address_type" : "Intersection",
            "longitude" : -122.407567,
            "latitude" : 37.7657933,
            "crimetype" : "traffic",
            "weight" : 20,
            "loc" : {
                    "type" : "Point",
                    "coordinates" : [
                            -122.407567,
                            37.7657933
                    ]
            }
        }
    )
    generate_seg_snap(model, coll_seg_snap, coll_segments, time)
    assert coll_seg_snap.count() == 2
    # Check that the weights are now higher than before
    loc = {
        "type" : "Point",
        "coordinates" : [
                -122.4075838,
                37.76594985
        ]
    }
    weight_2_1 = coll_seg_snap.find_one({'loc': loc})['weight']
    assert weight_2_1 > weight_1_1
    loc = {
        "type" : "Point",
        "coordinates" : [
                -122.4075444,
                37.765559249999995
        ]
    }
    weight_2_2 = coll_seg_snap.find_one({'loc': loc})['weight']
    assert weight_2_2 > weight_1_2

    # Add a call with weight and evaluate seg_snap and weights again
    coll_police.insert_one(
        {
            "city" : "San Francisco",
            "call_date" : datetime(2016, 10, 11, 0, 0, 0),
            "state" : "CA",
            "original_crimetype_name" : "Traffic Stop",
            "call_time" : "18:24",
            "agency_id" : "1",
            "call_dttm" : datetime(2016, 10, 11, 18, 24, 0),
            "crime_id" : "162853300",
            "offense_date" : datetime(2016, 10, 11, 0, 0, 0),
            "disposition" : "HAN",
            "address" : "16th St/potrero Av",
            "report_date" : datetime(2016, 10, 11, 0, 0, 0),
            "address_type" : "Intersection",
            "longitude" : -122.407567,
            "latitude" : 37.7657933,
            "crimetype" : "traffic",
            "weight" : 20,
            "loc" : {
                    "type" : "Point",
                    "coordinates" : [
                            -122.407567,
                            37.7657933
                    ]
            }
        }
    )
    generate_seg_snap(model, coll_seg_snap, coll_segments, time)
    assert coll_seg_snap.count() == 2
    # Check that the weights are now higher than before
    loc = {
        "type" : "Point",
        "coordinates" : [
                -122.4075838,
                37.76594985
        ]
    }
    weight_3_1 = coll_seg_snap.find_one({'loc': loc})['weight']
    assert weight_3_1 > weight_2_1
    loc = {
        "type" : "Point",
        "coordinates" : [
                -122.4075444,
                37.765559249999995
        ]
    }
    weight_3_2 = coll_seg_snap.find_one({'loc': loc})['weight']
    assert weight_3_2 > weight_2_2

@pytest.mark.mongo
def test_generate_seg_snap_weights_decreases_when_later_model_time_used(
        coll_police, coll_segments, coll_seg_snap, model):
    coll_segments.insert_one(
        {
            "node_2" : 3999495551,
            "distance" : 37.5953652125266,
            "loc" : {
                    "type" : "Point",
                    "coordinates" : [
                            -122.4075838,
                            37.76594985
                    ]
            },
            "node_1" : 65308767
        }
    )
    coll_police.insert_one(
        {
            "city" : "San Francisco",
            "call_date" : datetime(2016, 10, 11, 0, 0, 0),
            "state" : "CA",
            "original_crimetype_name" : "Traffic Stop",
            "call_time" : "18:24",
            "agency_id" : "1",
            "call_dttm" : datetime(2016, 10, 11, 18, 24, 0),
            "crime_id" : "162853299",
            "offense_date" : datetime(2016, 10, 11, 0, 0, 0),
            "disposition" : "HAN",
            "address" : "16th St/potrero Av",
            "report_date" : datetime(2016, 10, 11, 0, 0, 0),
            "address_type" : "Intersection",
            "longitude" : -122.407567,
            "latitude" : 37.7657933,
            "crimetype" : "traffic",
            "weight" : 20,
            "loc" : {
                    "type" : "Point",
                    "coordinates" : [
                            -122.407567,
                            37.7657933
                    ]
            }
        }
    )
    coll_police.insert_one(
        {
            "city" : "San Francisco",
            "call_date" : datetime(2016, 10, 11, 0, 0, 0),
            "state" : "CA",
            "original_crimetype_name" : "Traffic Stop",
            "call_time" : "18:24",
            "agency_id" : "1",
            "call_dttm" : datetime(2016, 10, 11, 18, 24, 0),
            "crime_id" : "162853300",
            "offense_date" : datetime(2016, 10, 11, 0, 0, 0),
            "disposition" : "HAN",
            "address" : "16th St/potrero Av",
            "report_date" : datetime(2016, 10, 11, 0, 0, 0),
            "address_type" : "Intersection",
            "longitude" : -122.407567,
            "latitude" : 37.7657933,
            "crimetype" : "traffic",
            "weight" : 20,
            "loc" : {
                    "type" : "Point",
                    "coordinates" : [
                            -122.407567,
                            37.7657933
                    ]
            }
        }
    )

    time = datetime(2016, 10, 12, 0, 0, 0)
    generate_seg_snap(model, coll_seg_snap, coll_segments, time)
    assert coll_seg_snap.count() == 1
    loc = {
        "type" : "Point",
        "coordinates" : [
                -122.4075838,
                37.76594985
        ]
    }
    weight_1 = coll_seg_snap.find_one({'loc': loc})['weight']

    time = datetime(3016, 10, 12, 0, 0, 0)
    generate_seg_snap(model, coll_seg_snap, coll_segments, time)
    # Check that the weights are now lower than before
    assert coll_seg_snap.count() == 1
    loc = {
        "type" : "Point",
        "coordinates" : [
                -122.4075838,
                37.76594985
        ]
    }
    weight_2 = coll_seg_snap.find_one({'loc': loc})['weight']
    assert weight_2 < weight_1
