import os
import json
import logging
from configparser import ConfigParser
import pytest
import networkx as nx
from pymongo import MongoClient, ASCENDING, GEOSPHERE
from tools.routing.routing import (
    add_nodes, add_edges, write_graph, get_path, read_graph, get_graph,
    node_in_graph, get_closest_node, make_heuristic)

DB_INI = os.path.join(os.path.dirname(os.path.realpath(__file__)),
                      'test_db.ini')

NODES_JSON = os.path.join(os.path.dirname(os.path.realpath(__file__)),
                          'nodes.json')
SEGMENTS_JSON = os.path.join(os.path.dirname(os.path.realpath(__file__)),
                             'segments.json')

@pytest.fixture()
def cfg():
    """Return initialized ConfigParser"""
    c = ConfigParser()
    c.read(DB_INI)
    return c

def replace_numberLong(json_parsed, field):
    if (not isinstance(json_parsed[field], int) and
            '$numberLong' in json_parsed[field]):
        json_parsed[field] = int(json_parsed[field]['$numberLong'])

@pytest.fixture()
def coll_nodes(cfg):
    """
        Return a Mongo connection to the nodes collection
        that has been populated
    """
    db_routing_name = cfg['Databases']['routing']
    coll_nodes_name = cfg['Collections']['nodes']

    # Make sure database/collection names starts with test so we don't
    # accidentally mess with production servers
    assert db_routing_name.startswith('test')
    assert coll_nodes_name.startswith('test')

    db_routing = MongoClient()[db_routing_name]
    # TODO authentication
    #db_routing.authenticate(cfg['Accounts'][''],
                                #cfg['Accounts'][''])
    coll_nodes = db_routing[coll_nodes_name]
    coll_nodes.drop()
    coll_nodes.create_index('id', unique=True)
    coll_nodes.create_index([('loc', GEOSPHERE)])
    assert coll_nodes.count() == 0

    # Import data from file
    data = []
    with open(NODES_JSON, 'r') as open_file:
        for line in open_file:
            json_parsed = json.loads(line)
            json_parsed.pop('_id', None)
            replace_numberLong(json_parsed, 'id')
            data.append(json_parsed)
    coll_nodes.insert_many(data)

    assert coll_nodes.count() == 553

    return coll_nodes

@pytest.fixture()
def coll_segments(cfg):
    """
        Return a Mongo connection to the segments collection
        that has been populated
    """
    db_routing_name = cfg['Databases']['routing']
    coll_nodes_name = cfg['Collections']['segments']

    # Make sure database/collection names starts with test so we don't
    # accidentally mess with production servers
    assert db_routing_name.startswith('test')
    assert coll_nodes_name.startswith('test')

    db_routing = MongoClient()[db_routing_name]
    # TODO authentication
    #db_routing.authenticate(cfg['Accounts'][''],
                                #cfg['Accounts'][''])
    coll_segments = db_routing[coll_nodes_name]
    coll_segments.drop()
    coll_segments.create_index([('node_1', ASCENDING), ('node_2', ASCENDING)],
                               unique=True)
    coll_segments.create_index([('loc', GEOSPHERE)])
    assert coll_segments.count() == 0

    # Import data from file
    data = []
    with open(SEGMENTS_JSON, 'r') as open_file:
        for line in open_file:
            json_parsed = json.loads(line)
            json_parsed.pop('_id', None)
            replace_numberLong(json_parsed, 'node_1')
            replace_numberLong(json_parsed, 'node_2')
            data.append(json_parsed)
    coll_segments.insert_many(data)

    assert coll_segments.count() == 662

    return coll_segments

@pytest.fixture()
def graph_file(coll_nodes, coll_segments, tmpdir):
    graph = nx.Graph()
    add_nodes(graph, coll_nodes)
    add_edges(graph, coll_segments)

    out_file = tmpdir.join('graph_file')
    assert out_file.check() is False

    write_graph(graph, str(out_file))
    assert out_file.check() is True

    return str(out_file)

def test_node_in_graph_returns_true_when_node_in_graph():
    graph = nx.Graph()
    graph.add_node(1)
    assert node_in_graph(1, graph) is True

def test_node_in_graph_returns_false_when_node_not_in_graph():
    graph = nx.Graph()
    graph.add_node(1)
    assert node_in_graph(2, graph) is False

def test_add_nodes_adds_nodes_to_graph(coll_nodes):
    graph = nx.Graph()
    add_nodes(graph, coll_nodes)
    assert graph.number_of_nodes() == 553
    assert graph.node[3999504911] is not None

@pytest.mark.mongo
def test_add_edges_adds_edges_to_graph(coll_nodes, coll_segments):
    graph = nx.Graph()
    add_nodes(graph, coll_nodes)
    add_edges(graph, coll_segments)
    assert graph.number_of_edges() == 662
    assert (65365910, 65334190) in graph.edges()
    assert (1264296812, 1264296815) in graph.edges()

@pytest.mark.mongo
def test_get_closest_node_returns_closest_node(coll_nodes, coll_segments):
    node = get_closest_node(coll_nodes, 37.76377, -122.39756, 100)
    assert node == 65290920

def test_get_closest_node_returns_none_when_no_node_within_search_radius(
        coll_nodes, coll_segments):
    node = get_closest_node(coll_nodes, 37.76377, -122.39756, 1)
    assert node is None

def test_dist_returns_correct_dist():
    graph = nx.Graph()
    graph.add_node(1, lat=37.76377, lon=-122.39756)
    graph.add_node(2, lat=37.7675, lon=-122.4089)
    graph.add_edge(1, 2)
    assert make_heuristic(graph)(1, 2) == 1081.5544545338967

@pytest.mark.mongo
def test_get_path_returns_correct_path(coll_nodes, coll_segments):
    graph = nx.Graph()
    add_nodes(graph, coll_nodes)
    add_edges(graph, coll_segments)
    path = get_path(graph, coll_nodes, 37.76377, -122.39756,
                    37.7675, -122.4089)
    assert path == [
        (-122.3976126, 37.7637902),
        (-122.397737, 37.765072),
        (-122.398703, 37.765014),
        (-122.39877, 37.76573),
        (-122.3988252, 37.766312),
        (-122.3991414, 37.7662932),
        (-122.399678, 37.766695),
        (-122.4001312, 37.7670592),
        (-122.4004285, 37.767298),
        (-122.400681, 37.767503),
        (-122.400877, 37.767488),
        (-122.4010903, 37.7674749),
        (-122.401843, 37.767431),
        (-122.402807, 37.767373),
        (-122.403588, 37.767326),
        (-122.40376, 37.767316),
        (-122.4045115, 37.7672712),
        (-122.4047579, 37.7672565),
        (-122.4052274, 37.7672286),
        (-122.405708, 37.7672),
        (-122.406677, 37.767142),
        (-122.4076907, 37.7670817),
        (-122.408582, 37.767028),
        (-122.4087903, 37.7672652),
        (-122.4088305, 37.7676183)
    ]

@pytest.mark.mongo
def test_get_path_returns_correct_path_2(coll_nodes, coll_segments):
    graph = nx.Graph()
    add_nodes(graph, coll_nodes)
    add_edges(graph, coll_segments)
    path = get_path(graph, coll_nodes, 37.76190, -122.40820,
                    37.76315, -122.40832)
    assert path == [(-122.4082095, 37.761882),
                    (-122.4083314, 37.7631555)]

@pytest.mark.mongo
def test_write_graph_writes_graph_to_file(coll_nodes, coll_segments, tmpdir):
    graph = nx.Graph()
    add_nodes(graph, coll_nodes)
    add_edges(graph, coll_segments)

    out_file = tmpdir.join('graph')
    assert out_file.check() is False

    write_graph(graph, str(out_file))
    assert out_file.check() is True

@pytest.mark.mongo
def test_get_path_with_graph_file_returns_correct_path(
        graph_file, coll_nodes):
    graph = read_graph(graph_file)
    path = get_path(graph, coll_nodes, 37.76377, -122.39756,
                    37.7675, -122.4089)
    assert path == [
        (-122.3976126, 37.7637902),
        (-122.397737, 37.765072),
        (-122.398703, 37.765014),
        (-122.39877, 37.76573),
        (-122.3988252, 37.766312),
        (-122.3991414, 37.7662932),
        (-122.399678, 37.766695),
        (-122.4001312, 37.7670592),
        (-122.4004285, 37.767298),
        (-122.400681, 37.767503),
        (-122.400877, 37.767488),
        (-122.4010903, 37.7674749),
        (-122.401843, 37.767431),
        (-122.402807, 37.767373),
        (-122.403588, 37.767326),
        (-122.40376, 37.767316),
        (-122.4045115, 37.7672712),
        (-122.4047579, 37.7672565),
        (-122.4052274, 37.7672286),
        (-122.405708, 37.7672),
        (-122.406677, 37.767142),
        (-122.4076907, 37.7670817),
        (-122.408582, 37.767028),
        (-122.4087903, 37.7672652),
        (-122.4088305, 37.7676183)
    ]

@pytest.mark.mongo
def test_get_path_with_graph_file_returns_correct_path_2(
        graph_file, coll_nodes):
    graph = read_graph(graph_file)
    path = get_path(graph, coll_nodes, 37.76190, -122.40820,
                    37.76315, -122.40832)
    assert path == [(-122.4082095, 37.761882),
                    (-122.4083314, 37.7631555)]

@pytest.mark.mongo
def test_add_nodes_with_bounds_only_adds_nodes_in_bounds(coll_nodes):
    graph = nx.Graph()
    bounds = (-122.41059, 37.76444, -122.40806, 37.76561)
    add_nodes(graph, coll_nodes, bounds)
    assert graph.number_of_nodes() == 40
    assert graph.node[266903685] is not None
    assert graph.node[3965113857] is not None
    with pytest.raises(KeyError):
        graph.node[3999504911]

@pytest.mark.mongo
def test_add_edges_with_bounds_only_adds_edges_in_bounds(
        coll_nodes, coll_segments):
    graph = nx.Graph()
    bounds = (-122.41059, 37.76444, -122.40806, 37.76561)
    add_nodes(graph, coll_nodes, bounds)
    add_edges(graph, coll_segments, bounds)
    assert graph.number_of_edges() == 40
    assert (266903690, 266903691) in graph.edges()
    assert (65365910, 65334190) not in graph.edges()
    assert (1264296812, 1264296815) not in graph.edges()

@pytest.mark.mongo
def test_get_graph_gets_graph_with_nodes_and_segments(
        coll_nodes, coll_segments):
    graph = get_graph(coll_nodes, coll_segments)

    assert graph.number_of_nodes() == 553
    assert graph.node[3999504911] is not None

    assert graph.number_of_edges() == 662
    assert (65365910, 65334190) in graph.edges()
    assert (1264296812, 1264296815) in graph.edges()

@pytest.mark.mongo
def test_get_graph_with_bounds_only_adds_nodes_and_edges_in_bounds(
        coll_nodes, coll_segments):
    bounds = (-122.41059, 37.76444, -122.40806, 37.76561)
    graph = get_graph(coll_nodes, coll_segments, bounds)

    assert graph.number_of_nodes() == 40
    assert graph.node[266903685] is not None
    assert graph.node[3965113857] is not None
    with pytest.raises(KeyError):
        graph.node[3999504911]

    assert graph.number_of_edges() == 40
    assert (266903690, 266903691) in graph.edges()
    assert (65365910, 65334190) not in graph.edges()
    assert (1264296812, 1264296815) not in graph.edges()
