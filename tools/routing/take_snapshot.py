"""
   :platform: Unix
   :synopsis:  TBD 

.. moduleauthor:: Lauren Slason <lauren.slason@gmail.com>


"""

import re
import sys
import logging
import argparse
import itertools
import datetime
import configparser
from pymongo import MongoClient


def main():
    # Set configuration
    parser = argparse.ArgumentParser(
        description='Provide configuration details for the API')
    parser.add_argument('-c', type=str, dest='config', default='update.cfg')
    parser.add_argument('-l', type=str, dest='log', default='take_snapshots')
    args = parser.parse_args()
    
    # Parse the configuration
    config = configparser.ConfigParser()
    config.read(args.config)

    # Initialize logging
    log_date = datetime.datetime.now()
    log_date = log_date.strftime("%m.%d.%Y_%H.%M.%S")
    log_file = '%s_%s.log' % (args.log, log_date)
    logging.basicConfig(filename=log_file,level=logging.DEBUG)
    
    # Open connection with the MongoDB database
    client = MongoClient(config['mongo']['host'], int(config['mongo']['port']))
    db = client[config['mongo']['source']]
    #db.authenticate(name=config['mongo']['username'], 
    #    password=config['mongo']['password'], source=config['mongo']['source'])

    c_nodes = db[config['mongo']['node_coll']]
    c_segments = db[config['mongo']['segment_coll']]
    c_timed_edges = db[config['mongo']['timed_segment_coll']]
    c_snapshot = config['mongo']['snapshot_coll']

    now = datetime.datetime.utcnow()
    month_ago = now - datetime.timedelta(days=30)
    c_timed_edges.aggregate(
        [ 
            {"$match": 
                {"$or": [
                    {"timestamp":{"$exists":False}},
                    {"timestamp":{"$gte": month_ago }}
                ]}
            }, 
            { "$group" : {
                "_id": "$loc",
                "weight": {"$sum":"$weight"},
                "call_id":{"$addToSet":"$call_id"},
                "node_1":{"$first":"$node_1"},
                "node_2":{"$first":"$node_2"},
                "distance":{"$first":"$distance"} }
            },
            {"$out": c_snapshot }
            ], allowDiskUse=True)

if __name__ == '__main__':
    main()
