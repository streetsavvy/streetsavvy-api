import logging
from argparse import ArgumentParser
from configparser import ConfigParser
from pymongo import MongoClient
from PIL import Image, ImageDraw
from tools.process_osm.render import (render, ll_to_pixel,
    LINE_PIXEL_WIDTH, draw_circle, NODE_PIXEL_RADIUS, render_segments)
from tools.routing.routing import get_path, get_graph, read_graph, write_graph
from tools.heatmap.render_heatmap import get_pixel_dimensions

def render_route(file_prefix, path, coll_nodes, coll_segments, geo_bounds,
        long_dim, seg_width=1, seg_color='blue', route_width=None,
        route_color='white', start_marker_radius=3,
        start_marker_color='orange', end_marker_radius=3,
        end_marker_color='violet'):
    seg_file = render_segments(file_prefix, coll_nodes, coll_segments,
                               geo_bounds, long_dim, seg_width, seg_color)

    logging.info("Rendering route")
    image = Image.open(seg_file)
    draw = ImageDraw.Draw(image)
    image_dim = get_pixel_dimensions(geo_bounds, long_dim)
    c = ll_to_pixel(path[0][1], path[0][0], geo_bounds, image_dim)
    draw_circle(draw, c, start_marker_radius, outline=start_marker_color,
                fill=start_marker_color)

    c = ll_to_pixel(path[-1][1], path[-1][0], geo_bounds, image_dim)
    draw_circle(draw, c, end_marker_radius, end_marker_color,
                fill=end_marker_color)

    if route_width is None:
        route_width = seg_width*2
    for i in range(1, len(path)):
        c1 = ll_to_pixel(path[i-1][1], path[i-1][0], geo_bounds, image_dim)
        c2 = ll_to_pixel(path[i][1], path[i][0], geo_bounds, image_dim)
        draw.line((c1, c2), fill=route_color, width=route_width)

    del draw
    out_file = '{0}_route.png'.format(file_prefix)
    image.save(out_file)
    logging.info("Route rendered into %s", out_file)

    return out_file

def render_on_top(file, path, coll_nodes, coll_segments, geo_bounds,
        long_dim, seg_width=1, seg_color='blue', route_width=None,
        route_color='white', start_marker_radius=3,
        start_marker_color='orange', end_marker_radius=3,
        end_marker_color='violet'):
    logging.info("Rendering route on top of %s", file)
    image = Image.open(file)
    draw = ImageDraw.Draw(image)

    draw = ImageDraw.Draw(image)
    image_dim = get_pixel_dimensions(geo_bounds, long_dim)
    c = ll_to_pixel(path[0][1], path[0][0], geo_bounds, image_dim)
    draw_circle(draw, c, start_marker_radius, outline=start_marker_color,
                fill=start_marker_color)

    c = ll_to_pixel(path[-1][1], path[-1][0], geo_bounds, image_dim)
    draw_circle(draw, c, end_marker_radius, end_marker_color,
                fill=end_marker_color)

    if route_width is None:
        route_width = seg_width*2
    for i in range(1, len(path)):
        c1 = ll_to_pixel(path[i-1][1], path[i-1][0], geo_bounds, image_dim)
        c2 = ll_to_pixel(path[i][1], path[i][0], geo_bounds, image_dim)
        draw.line((c1, c2), fill=route_color, width=route_width)

    del draw
    out_file = '{0}_overlay.png'.format(file)
    image.save(out_file)
    logging.info("Route rendered into %s", out_file)

    return out_file

def main():
    """Main entry point function."""
    DEFAULT_GEO_BOUNDS = (-122.5175, 37.7007, -122.3506, 37.8204) # All of San Francisco
    GRAPH_FILE = 'graph.gpickle'
    DIST_GRAPH_FILE = 'dist_graph.gpickle'
    IMAGE_PREFIX = 'images/render_route'

    parser = ArgumentParser(
        description="Add nodes and segments from the given osm file to Mongo")
    parser.add_argument("-c", dest='cfg_db', default='db.ini',
                        help='ini file for configurations. Default is db.ini')
    parser.add_argument("-f", default=IMAGE_PREFIX,
                        help='file prefix to store images')
    parser.add_argument(
        '--geo', nargs='+', dest='geo_bounds', default=DEFAULT_GEO_BOUNDS,
        help="Geographical bounds for heatmap generation in the form of: " +
             "left bottom right top bounds")
    args = parser.parse_args()

    fmt = "%(name)s:%(asctime)s:%(levelname)s:%(funcName)s:%(message)s"
    logging.basicConfig(level=logging.DEBUG, format=fmt)
    logging.info("Starting mock_data.py")

    cfg = ConfigParser()
    cfg.read(args.cfg_db)

    db_routing = MongoClient()[cfg['Databases']['routing']]
    # TODO authentication
    #db_routing.authenticate(cfg['Accounts'][''],
                                #cfg['Accounts'][''])
    coll_nodes = db_routing[cfg['Collections']['nodes']]
    coll_seg_snap = db_routing[cfg['Collections']['seg_snap']]
    coll_segments = db_routing[cfg['Collections']['segments']]

    #render(args.file, coll_nodes, coll_segments)
    #render_call(args.file, coll_nodes, coll_segments)

    # Regenerate seg_snap
    #from tools.routing.seg_snap import generate_seg_snap
    #from tools.model.ssmodel import SSModel
    #from datetime import datetime
    #model = SSModel(args.cfg_db)
    #model_time = datetime(2017, 4, 15, 12, 0, 0)
    #logging.info("Regenerating seg_snap")
    #generate_seg_snap(model, coll_seg_snap, coll_segments, model_time,
    #                  args.geo_bounds)

    import os
    if os.path.isfile(GRAPH_FILE):
        logging.info("Graph file exists. Reading it.")
        g = read_graph(GRAPH_FILE)
    else:
        logging.info("Graph file not found. Recreating it")
        g = get_graph(coll_nodes, coll_seg_snap, bounds=args.geo_bounds,
                      distance=False)
        write_graph(g, GRAPH_FILE)

    # Graph file without distance modification
    if os.path.isfile(GRAPH_FILE):
        logging.info("Graph file exists. Reading it and including distance modification.")
        g_with_dist = read_graph(GRAPH_FILE, include_dist=True)
    else:
        logging.info("Graph file not found. Recreating it and including distance modification.")
        g_with_dist = get_graph(coll_nodes, coll_seg_snap, bounds=args.geo_bounds,
                                distance=False)
        write_graph(g_with_dist, GRAPH_FILE)
        g_with_dist = read_graph(GRAPH_FILE, include_dist=True)

    if os.path.isfile(DIST_GRAPH_FILE):
        logging.info("Distance graph file exists. Reading it.")
        dist_g = read_graph(DIST_GRAPH_FILE)
    else:
        logging.info("Distance graph file not found. Recreating it")
        dist_g = get_graph(coll_nodes, coll_segments, bounds=args.geo_bounds)
        write_graph(dist_g, DIST_GRAPH_FILE)

    # Force recreate
    #logging.info("Creating graph file")
    #g = get_graph(coll_nodes, coll_seg_snap, bounds=args.geo_bounds,
    #              distance=False)
    #write_graph(g, GRAPH_FILE)

    lat1, lon1 = 37.7800, -122.4201
    lat2, lon2 = 37.7916, -122.4411
    logging.info("Getting/rendering crime based route.")
    p = get_path(g, coll_nodes, lat1, lon1, lat2, lon2, distance=False)
    file = render_route('route', p, coll_nodes, coll_segments,
                 args.geo_bounds, 5000, route_color='green',
                 route_width=3)

    logging.info("Getting/rendering distance based route.")
    p = get_path(dist_g, coll_nodes, lat1, lon1, lat2, lon2)
    file = render_on_top(file, p, coll_nodes, coll_segments,
                  args.geo_bounds, 5000, route_color='pink',
                  route_width=2)

    logging.info("Getting/rendering crime and distance based route.")
    p = get_path(g_with_dist, coll_nodes, lat1, lon1, lat2, lon2)
    file = render_on_top(file, p, coll_nodes, coll_segments,
                  args.geo_bounds, 5000, route_color='white',
                  route_width=1)

if __name__ == "__main__":
    main()
