"""
Routing for StreetSavvy using NetworkX
"""
import logging
from argparse import ArgumentParser
from configparser import ConfigParser
from pymongo import MongoClient
import networkx as nx
from geopy.distance import vincenty, great_circle

def add_nodes(graph, coll_nodes, bounds=None):
    """Add nodes to the graph"""
    logging.info("Adding nodes")
    if bounds is None:
        logging.info("Bounds option off. Getting all nodes in %s",
                     coll_nodes.full_name)
        cursor = coll_nodes.find()
    else:
        logging.info("Getting all nodes in the bounds: '%s' in %s",
                     bounds, coll_nodes.full_name)
        cursor = get_docs_in_bounds(coll_nodes, bounds)

    cnt = cursor.count()
    logging.info("Number of nodes: %d", cnt)
    for i, node in enumerate(cursor):
        coords = node['loc']['coordinates']
        graph.add_node(node['id'], lat=coords[1], lon=coords[0])
        if cnt > 10 and i % int(cnt/10) == 0:
            logging.info("At node: %d", i)

def add_edges(graph, coll_edges, bounds=None, distance=True):
    """
        Add segments as edges to the graph

        This function will only add a segment as an edge if the
        corresponding nodes are in the graph
    """
    logging.info("Adding edges")

    if bounds is None:
        logging.info("Bounds option off. Getting all edges in %s",
                     coll_edges.full_name)
        cursor = coll_edges.find()
    else:
        logging.info("Getting all edges in the bounds: '%s' inside %s",
                     bounds, coll_edges.full_name)
        if distance:
            cursor = get_docs_in_bounds(coll_edges, bounds)
        else:
            cursor = get_docs_in_bounds(coll_edges, bounds)

    cnt = cursor.count()
    logging.info("Number of edges: %d", cnt)
    for i, seg in enumerate(cursor):
        # Only add the edge if the node exists in the graph
        if (node_in_graph(seg['node_1'], graph) and
                node_in_graph(seg['node_2'], graph)):
            if distance:
                graph.add_edge(seg['node_1'], seg['node_2'],
                    weight=seg['distance'])
            else:
                graph.add_edge(seg['node_1'], seg['node_2'],
                    weight=seg['weight'], distance=seg['distance'])
        if cnt > 10 and i % int(cnt/10) == 0:
            logging.info("At edge: %d", i)

def get_docs_in_bounds(coll, bounds, loc_field='loc'):
    query = {
        loc_field: {
            '$geoWithin': {
                '$box': [
                    [bounds[0], bounds[1]],
                    [bounds[2], bounds[3]]
                ]
            }
        }
    }
    return coll.find(query)

def node_in_graph(node, graph):
    try:
        graph.node[node]
        return True
    except KeyError:
        return False

def get_closest_node(coll_nodes, lat, lon, radius):
    """
        Get the closest node to given lat/lon. Search only within the
        given radius (in meters). This means if no nodes are found, this
        function can return none (or maybe throw an error... TODO find
        this out).
    """
    query = {
        'loc': {
            '$near': {
                '$geometry': {'type': 'Point', 'coordinates': [lon, lat]},
                '$maxDistance': radius
            }
        }
    }
    # Note: Not using find_one because I'm not sure if it'll
    # retrieve the closest node
    cursor = coll_nodes.find(query).limit(1)
    if cursor.count() == 0:
        return None
    return cursor[0]['id']

def make_heuristic(g):
    """Wrapper def to expose graph to the heuristic function"""
    def dist(a, b):
        """Heuristic callback for astar"""
        return vincenty((g.node[a]['lat'], g.node[a]['lon']),
                        (g.node[b]['lat'], g.node[b]['lon'])).meters
    return dist

def get_path(g, coll_nodes, lat1=37.76377, lon1=-122.39756,
             lat2=37.7675, lon2=-122.4089, search_radius=100, distance=True):
    """
        Get the path between the given lat/lon pairs. The search_radius
        is in meters and specifies how far from the given lat/lon pairs
        will be searched for the node closest to them.

        TODO Get rid of these default arguments. They're there right now
        for testing purposes.

        Returned path is a list of tuples of (lon, lat)
    """
    logging.info("Getting path between %f, %f and %f, %f (lat, lon)",
                 lat1, lon1, lat2, lon2)
    logging.info("Searching within %f meters for the start/end nodes",
                 search_radius)

    n1 = get_closest_node(coll_nodes, lat1, lon1, search_radius)
    n2 = get_closest_node(coll_nodes, lat2, lon2, search_radius)

    if n1 is None or n2 is None:
        logging.warning(
            "Start or end node is None. Can't do path " +
            "finding. Try specifying a larger search_radius")
        return None

    if distance:
        node_path = nx.astar_path(g, n1, n2, make_heuristic(g))
    else:
        node_path = nx.astar_path(g, n1, n2)
    logging.debug("Calculated node path: %s", node_path)

    path = []
    for node in node_path:
        lat = g.node[node]['lat']
        lon = g.node[node]['lon']
        path.append((lon, lat))

    logging.debug("Calculated path: %s", path)
    return path

def get_max_weight(g):
    max_weight = 0
    for e in g.edges_iter():
        weight = g[e[0]][e[1]]['weight']
        if weight > max_weight:
            max_weight = weight
    return max_weight

def get_max_distance(g):
    max_distance = 0
    for e in g.edges_iter():
        distance = g[e[0]][e[1]]['distance']
        if distance > max_distance:
            max_distance = distance
    return max_distance

def get_graph(coll_nodes, coll_edges, bounds=None, distance=True):
    """Return a graph with nodes and edges from the given collections"""
    logging.info("Getting graph")
    g = nx.Graph()
    add_nodes(g, coll_nodes, bounds)
    add_edges(g, coll_edges, bounds, distance),
    return g

def write_graph(g, out_file):
    logging.info("Writing graph to %s", out_file)
    nx.write_gpickle(g, out_file)

def read_graph(graph_file, include_dist=False, alpha=0.1, beta=0.9):
    logging.info("Reading graph %s", graph_file)
    g = nx.read_gpickle(graph_file)
    if include_dist is True:
        modify_graph(g, alpha, beta)
    return g

def modify_graph(g, alpha, beta):
    # Modify graph edges
    # max_dist = 837.011274717263
    max_dist = get_max_distance(g)
    max_weight = get_max_weight(g)
    logging.debug("Max dist: %f", max_dist)
    logging.debug("Max weight: %f", max_weight)
    for e in g.edges_iter():
        edge = g[e[0]][e[1]]
        weight = edge['weight']
        dist = edge['distance']
        new_weight = (1-alpha)*(weight/max_weight) + (1-beta)*(dist/max_dist)
        edge['weight'] = new_weight
    return g

# TEMP
# TODO Remove
def doit(coll_nodes, coll_edges, file_name, bounds, distance):
    """Do some routing"""
    g = get_graph(coll_nodes, coll_edges, bounds, distance)
    write_graph(g, file_name)
    get_path(g, coll_nodes, 37.763770, -122.397560, 37.767500, -122.408900)

# TEMP
# TODO Remove
def doit_with_file(coll_nodes, coll_edges):
    """Do some routing"""
    graph = read_graph('graph.gpickle')
    get_path(graph, coll_nodes)

# TEMP
# TODO Remove
def do_mock_routing():
    """Do some routing with made up nodes and edges"""
    g = nx.Graph()
    n1 = Node(1, 0, 0)
    n2 = Node(2, 1, 1)
    n3 = Node(3, 2, 2)

    g.add_node(n1)
    g.add_node(n2)
    g.add_node(n2)

    g.add_edge(n1, n2)
    g.add_edge(n2, n3)

    path = nx.astar_path(g, n1, n3, lambda a, b: 10)
    print(path)

def main():
    """Main entry point function."""
    parser = ArgumentParser(
        description="Add nodes and segments from the given osm file to Mongo")
    parser.add_argument("-c", dest='cfg_db', default='db.ini',
                        help='ini file for configurations. Default is db.ini')
    parser.add_argument("-o", dest='output', default='graph.gpickle',
                        help='name out the pickled output file')
    parser.add_argument('--crime', dest='distance', default=True,
                        action='store_false', help='Indicate whether to make a distance or crime graph')
    parser.add_argument('-b', '--bounds', dest='bounds', nargs='+', type=float,
                        help="Bounds: left bottom right top. " +
                             "e.g.: -b -122.4320 37.7519 -122.3807 37.7760")
    args = parser.parse_args()

    fmt = "%(name)s:%(asctime)s:%(levelname)s:%(funcName)s:%(message)s"
    logging.basicConfig(level=logging.INFO, format=fmt)
    logging.info("Starting routing.py")

    cfg = ConfigParser()
    cfg.read(args.cfg_db)

    if args.bounds is not None:
        geo_bounds = args.bounds
        logging.info("Bounds specified in command line: %s", geo_bounds)
    else:
        geo_bounds = None
        logging.info("Bounds not specified in command line")

    if args.distance:
        logging.info("Distance graph was specified.")
    else:
        logging.info("Crime based graph was specified")

    db_routing = MongoClient()[cfg['Databases']['routing']]
    # TODO authentication
    #db_routing.authenticate(cfg['Accounts'][''],
                                #cfg['Accounts'][''])
    coll_nodes = db_routing[cfg['Collections']['nodes']]
    if args.distance:
        coll_edges = db_routing[cfg['Collections']['segments']]
    else:
        coll_edges = db_routing[cfg['Collections']['snapshot']]
    doit(coll_nodes, coll_edges, args.output, geo_bounds, args.distance)

    #doit_with_file(coll_nodes, coll_edges)

if __name__ == "__main__":
    main()
