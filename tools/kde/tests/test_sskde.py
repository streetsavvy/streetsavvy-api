"""Tests for sskde"""
# pylint: disable=redefined-outer-name,invalid-name

# 3rd Party Libs
import pytest
from numpy import float64

# Internal Libs
from tools.kde.sskde import SSKDE

@pytest.fixture()
def sskde():
    """Return SSKDE"""
    return SSKDE()

def test_getscore_returns_score(sskde):
    """
        Test getscore() returns a score when have been added
    """
    loc = [37.7528983, -122.5075735]
    sskde.addlocation(loc)
    score = sskde.getscore(loc)
    assert isinstance(score, float64)

def test_addlocation_adds_location(sskde):
    """
        Test addlocation() adds a location to sskde data
    """
    loc = [37.7528983, -122.5075735]
    sskde.addlocation(loc)
    assert sskde.locations[-1][0] == loc[0]
    assert sskde.locations[-1][1] == loc[1]

def test_addlocation_refits_kde(sskde):
    """
        Test addlocation() refits the KDE
    """
    # Initialize KDE. KDE must be fitted with some initial data before
    # trying to get a score
    sskde.kde.fit([[0, 0]])

    loc = [37.7528983, -122.5075735]
    score = sskde.getscore(loc) # Record score before addlocations

    sskde.addlocation(loc)

    # Confirm that the score has increased as a result of refitting the
    # KDE
    new_score = sskde.getscore(loc)
    assert new_score > score

def test_addlocations_adds_locations(sskde):
    """
        Test addlocations() adds a location to sskde data
    """
    locs = [[37.7528983, -122.5075735],
            [37.752933, -122.506495],
            [37.756852, -122.502440]]
    sskde.addlocations(locs)

    assert sskde.locations[-3][0] == locs[0][0]
    assert sskde.locations[-3][1] == locs[0][1]
    assert sskde.locations[-2][0] == locs[1][0]
    assert sskde.locations[-2][1] == locs[1][1]
    assert sskde.locations[-1][0] == locs[2][0]
    assert sskde.locations[-1][1] == locs[2][1]

def test_addlocations_refits_kde(sskde):
    """
        Test addlocations() refits the KDE
    """
    # Initialize KDE. KDE must be fitted with some initial data before
    # trying to get a score
    sskde.kde.fit([[0, 0]])

    loc = [37.7528983, -122.5075735]
    score = sskde.getscore(loc) # Record score before addlocations

    locs = [[37.7528983, -122.5075735],
            [37.752933, -122.506495],
            [37.756852, -122.502440]]
    sskde.addlocations(locs)

    # Confirm that the score has increased as a result of refitting the
    # KDE
    new_score = sskde.getscore(loc)
    assert new_score > score

def test_resetlocations_resets_locations(sskde):
    """
        Test resetlocations() resets locations
    """
    loc = [37.7528983, -122.5075735]
    sskde.addlocation(loc)
    sskde.resetlocations()
    assert len(sskde.locations) == 0
