"""
    This module contains the SSKDE class. The KDE used for StreetSavvy.
"""
from numpy import array, exp
from sklearn.neighbors import KernelDensity

BW = 0.04 # Bandwidth for KDE

class SSKDE:
    """
        StreetSavvy KDE class. Locations must be added before getscore()
        can be called, else unspecified behavior will result.
    """
    def __init__(self, bandwidth=BW):
        self.kde = KernelDensity(bandwidth=bandwidth, metric='haversine',
                                 kernel='gaussian', algorithm='ball_tree')
        self.locations = []

    def addlocation(self, location):
        """
            Add a location to the list of locations and refit the KDE

            :param location
            :type location list of 2 floats
        """
        self.locations.append(location)
        self.kde.fit(self.locations)

    def addlocations(self, locations):
        """
            Add locations to the list of locations and refit the KDE

            :param locations
            :type locations list of list of 2 floats
        """
        for location in locations:
            self.locations.append(location)
        self.kde.fit(self.locations)

    def getscore(self, location):
        """
            Get score at the specified location according to the current
            KDE

            :param location
            :type location list of 2 floats
        """
        # Reformat location because passing in 1d arrays into kde.score
        # is deprecated
        loc = array(location).reshape(1, -1)
        return exp(self.kde.score_samples(loc))[0]

    def resetlocations(self):
        """
            Reset the locations. Locations must be added again before
            getscore() is called, else unspecified behavior will result
        """
        self.locations = []
