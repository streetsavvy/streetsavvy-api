import os
import logging
from argparse import ArgumentParser
from configparser import ConfigParser
from datetime import datetime, timedelta
from PIL import Image, ImageDraw
from pymongo import MongoClient
#from tools.process_osm.process_osm import get_near
from tools.heatmap.render_heatmap import get_pixel_dimensions

NODE_PIXEL_RADIUS = 3
CENTROID_PIXEL_RADIUS = 3
LINE_PIXEL_WIDTH = 2

CALL_PIXEL_RADIUS = NODE_PIXEL_RADIUS + 2
NEAR_CENTROIDS_PIXEL_RADIUS = NODE_PIXEL_RADIUS + 1

def draw_circle(draw, center, radius, outline=None, fill=None):
    xy = [(center[0] - radius, center[1] - radius),
          (center[0] + radius, center[1] + radius)]
    draw.ellipse(xy, outline=outline, fill=fill)

# Lat long to pixel
def ll_to_pixel(lat, lon, bounds, image_dim):
    adj_lat = lat - bounds[1]
    adj_lon = lon - bounds[0]

    delta_lat = bounds[3] - bounds[1]
    delta_lon = bounds[2] - bounds[0]

    lon_frac = adj_lon/delta_lon
    lat_frac = adj_lat/delta_lat

    x = int(lon_frac*image_dim[0])
    y = int((1-lat_frac)*image_dim[1])

    return x, y

# Pixel to lat long
def pixel_to_ll(x, y, bounds, image_dim):
    delta_lat = bounds[3] - bounds[1]
    delta_lon = bounds[2] - bounds[0]

    x_frac = float(x)/image_dim[0]
    y_frac = float(y)/image_dim[1]

    lon = bounds[0] + x_frac*delta_lon
    lat = bounds[3] - y_frac*delta_lat

    # TODO Figure out why this is here
    calc_x, calc_y = ll_to_pixel(lat, lon, bounds, image_dim)
    if abs(calc_x-x) > 1 or abs(calc_y-y) > 1:
        print("Mismatch: %s, %s => %s %s" % (
            x,y, calc_x, calc_y))

    return lat, lon

def get_docs_in_box(coll, box, loc_field='loc'):
    query = {
        loc_field: {
            '$geoWithin': {
                '$box': [
                    [box[0], box[1]],
                    [box[2], box[3]]
                ]
            }
        }
    }
    return coll.find(query)

def render(out_file, coll_nodes, coll_segments, geo_bounds, image_dim):
    logging.info("Rendering")

    image = Image.new('RGBA', image_dim, color='black')
    draw = ImageDraw.Draw(image)

    # Draw nodes
    logging.info("Drawing nodes")
    nodes = get_docs_in_box(coll_nodes, geo_bounds)
    logging.info("Nodes to draw: %d", nodes.count())
    for n in nodes:
        coord = n['loc']['coordinates']
        center = ll_to_pixel(coord[1], coord[0], geo_bounds, image_dim)
        draw_circle(draw, center, NODE_PIXEL_RADIUS,
                    outline='red', fill='red')

    image.save('{0}_nodes.png'.format(out_file))

    # Draw segments
    logging.info("Drawing segments")
    segments = get_docs_in_box(coll_segments, geo_bounds)
    logging.info("Segment to draw: %d", segments.count())
    for s in segments:
        node_1 = coll_nodes.find_one({'id': s['node_1']})
        coord_1 = node_1['loc']['coordinates']
        center_1 = ll_to_pixel(coord_1[1], coord_1[0], geo_bounds, image_dim)

        node_2 = coll_nodes.find_one({'id': s['node_2']})
        coord_2 = node_2['loc']['coordinates']
        center_2 = ll_to_pixel(coord_2[1], coord_2[0], geo_bounds, image_dim)
        draw.line((center_1, center_2), fill='blue', width=LINE_PIXEL_WIDTH)

    image.save('{0}_nodes_segs.png'.format(out_file))

    # Draw centroids
    logging.info("Drawing centroids")
    centroids = get_docs_in_box(coll_segments, geo_bounds)
    logging.info("Centroids to draw: %d", centroids.count())
    for c in centroids:
        coord = c['loc']['coordinates']
        center = ll_to_pixel(coord[1], coord[0], geo_bounds, image_dim)
        draw_circle(draw, center, CENTROID_PIXEL_RADIUS,
                    outline='green', fill='green')

    # TEMP Draw some points
    #draw_circle(draw, ll_to_pixel(37.7620169, -122.4062406), NODE_RADIUS+1,
    #            outline='yellow', fill='yellow')
    #draw_circle(draw, ll_to_pixel(37.7673296, -122.4127672), NODE_RADIUS+1,
    #            outline='yellow', fill='yellow')

    # 266903710
    #draw_circle(draw, ll_to_pixel(37.7655075, -122.410356), NODE_PIXEL_RADIUS+3,
    #            outline='violet', fill='violet')
    # 266903707
    #draw_circle(draw, ll_to_pixel(37.7654439, -122.4102541), NODE_PIXEL_RADIUS+3,
    #            outline='orange', fill='orange')
    # 65290286 - not interested
    #draw_circle(draw, ll_to_pixel(37.7656088, -122.4104935), NODE_PIXEL_RADIUS+3,
    #            outline='white', fill='white')

    del draw
    image.save('{0}_nodes_segs_centroids.png'.format(out_file))

def render_call(file, coll_nodes, coll_segments):
    logging.info("rendering call")

    image = Image.open('{0}_nodes_segs_centroids.png'.format(file))
    draw = ImageDraw.Draw(image)

    # Choose a point to simulate the call
    #call_locaton = (-122.41055, 37.76561)
    call_locaton = (-122.41028, 37.76298)
    # Let's say this service call affects anything in this radius (meters)
    radius = 100

    # Mark the call location
    draw_circle(draw, ll_to_pixel(call_locaton[1], call_locaton[0]),
                CALL_PIXEL_RADIUS, outline='violet', fill='violet')

    # Get centroids near the location
    cursor = get_near(coll_segments, call_locaton[1],
                      call_locaton[0], radius)

    # Iterate over affected centroids
    for c in cursor:
        # Mark them on the map
        coord = c['loc']['coordinates']
        center = ll_to_pixel(coord[1], coord[0])
        draw_circle(draw, center, NEAR_CENTROIDS_PIXEL_RADIUS,
                    outline='yellow', fill='yellow')

    del draw
    image.save('{0}_nodes_segs_centroids_call.png'.format(file))

def render_calls(file, coll_police, coll_nodes, coll_segments, image_dim, geo_bounds, time):
    logging.info("render_calls")

    image = Image.new('RGBA', image_dim, color='black')
    draw = ImageDraw.Draw(image)

    logging.info("Drawing segments")
    segments = get_docs_in_box(coll_segments, geo_bounds)
    logging.info("Segment to draw: %d", segments.count())
    for s in segments:
        node_1 = coll_nodes.find_one({'id': s['node_1']})
        coord_1 = node_1['loc']['coordinates']
        center_1 = ll_to_pixel(coord_1[1], coord_1[0], geo_bounds, image_dim)

        node_2 = coll_nodes.find_one({'id': s['node_2']})
        coord_2 = node_2['loc']['coordinates']
        center_2 = ll_to_pixel(coord_2[1], coord_2[0], geo_bounds, image_dim)
        draw.line((center_1, center_2), fill='blue', width=LINE_PIXEL_WIDTH)

    CALLS_TIME = 86400
    min_time = time - timedelta(seconds=CALLS_TIME)
    query = {
            'call_dttm': {'$lte': time, '$gte': min_time},
            'weight': {'$exists': True},
            'loc': {
                '$geoWithin': {
                    '$box': [
                        [geo_bounds[0], geo_bounds[1]],
                        [geo_bounds[2], geo_bounds[3]]
                    ]
                }
            }
    }
    proj = {
        '_id': False,
        'loc': True,
    }
    logging.info("Drawing calls")
    cursor = coll_police.find(query, proj)
    logging.info("Number of calls to draw: %d", cursor.count())
    for call in cursor:
        call_locaton = call['loc']['coordinates']
        draw_circle(draw, ll_to_pixel(call_locaton[1], call_locaton[0], geo_bounds, image_dim),
                CALL_PIXEL_RADIUS, outline='violet', fill='violet')
    del draw
    image.save('{0}_calls.png'.format(file))

def render_snapshot(out_file, coll_snapshot, geo_bounds, image_dim):
    logging.info("Rendering snapshot")

    image = Image.new('RGBA', image_dim)
    draw = ImageDraw.Draw(image)

    cursor = get_docs_in_box(coll_snapshot, geo_bounds, loc_field='_id')
    logging.info("Number of points to draw: %d", cursor.count())

    for i in cursor:
        coord = i['_id']['coordinates']
        center = ll_to_pixel(coord[1], coord[0], geo_bounds, image_dim)
        draw_circle(draw, center, int(i['weight']/50), outline='red', fill='red')

    image.save(out_file)

def render_on_top(in_file, out_file, coll_nodes, coll_segments, geo_bounds,
        long_dim, node_pixel_radius=1, line_pixel_radius=1):
    logging.info("Rendering")

    image = Image.open(in_file)
    draw = ImageDraw.Draw(image)

    image_dim = get_pixel_dimensions(geo_bounds, long_dim)

    # Draw nodes
    #logging.info("Drawing nodes")
    #nodes = get_docs_in_box(coll_nodes, geo_bounds)
    #logging.info("Nodes to draw: %d", nodes.count())
    #for n in nodes:
    #    coord = n['loc']['coordinates']
    #    center = ll_to_pixel(coord[1], coord[0], geo_bounds, image_dim)
    #    draw_circle(draw, center, node_pixel_radius,
    #                outline='red', fill='red')

    # Draw segments
    logging.info("Drawing segments")
    segments = get_docs_in_box(coll_segments, geo_bounds)
    logging.info("Segments to draw: %d", segments.count())
    for s in segments:
        node_1 = coll_nodes.find_one({'id': s['node_1']})
        coord_1 = node_1['loc']['coordinates']
        center_1 = ll_to_pixel(coord_1[1], coord_1[0], geo_bounds, image_dim)

        node_2 = coll_nodes.find_one({'id': s['node_2']})
        coord_2 = node_2['loc']['coordinates']
        center_2 = ll_to_pixel(coord_2[1], coord_2[0], geo_bounds, image_dim)
        draw.line((center_1, center_2), fill='white', width=line_pixel_radius)

    del draw
    image.save('{0}.png'.format(out_file))

def render_segments(file_prefix, coll_nodes, coll_segments, geo_bounds, long_dim,
        seg_width=1, seg_color='blue'):
    out_file = "{0}_{1}_{2}_{3}_{4}_{5}.png".format(
        file_prefix, long_dim, geo_bounds[0], geo_bounds[1],
        geo_bounds[2], geo_bounds[3])
    logging.info("Segments will be rendered into %s", out_file)

    if os.path.isfile(out_file):
        logging.info("Found rendered segments cached %s", out_file)
        return out_file

    logging.info("File not cached. Generating it")
    segments = get_docs_in_box(coll_segments, geo_bounds)
    logging.info("Segments to draw: %d", segments.count())
    image_dim = get_pixel_dimensions(geo_bounds, long_dim)
    image = Image.new('RGBA', image_dim)
    draw = ImageDraw.Draw(image)
    for s in segments:
        node_1 = coll_nodes.find_one({'id': s['node_1']})
        coord_1 = node_1['loc']['coordinates']
        center_1 = ll_to_pixel(coord_1[1], coord_1[0], geo_bounds, image_dim)

        node_2 = coll_nodes.find_one({'id': s['node_2']})
        coord_2 = node_2['loc']['coordinates']
        center_2 = ll_to_pixel(coord_2[1], coord_2[0], geo_bounds, image_dim)
        draw.line((center_1, center_2), fill=seg_color, width=seg_width)

    del draw
    image.save(out_file)
    logging.info("Segments rendered into %s", out_file)
    return out_file

def main():
    """Main entry point function."""
    parser = ArgumentParser(
        description="Add nodes and segments from the given osm file to Mongo")
    parser.add_argument("-c", dest='cfg_db', default='db.ini',
                        help='ini file for configurations. Default is db.ini')
    parser.add_argument("file_prefix", help='file prefix to store images')
    parser.add_argument("long_dim", type=int,
        help=("Pixel length of the longer dimension. The other dimension " +
              "(whether its the width or the height will be calculated " +
              "based on this and the given bounds"))
    parser.add_argument("left", type=float, help="Left bound (min lon)")
    parser.add_argument("bottom", type=float, help="Bottom bound (min lat)")
    parser.add_argument("right", type=float, help="Right bound (max lon)")
    parser.add_argument("top", type=float, help="Top bound (max_lat)")
    args = parser.parse_args()

    fmt = "%(name)s:%(asctime)s:%(levelname)s:%(funcName)s:%(message)s"
    logging.basicConfig(level=logging.INFO, format=fmt)
    logging.info("Starting render.py")

    geo_bounds = (args.left, args.bottom, args.right, args.top)

    cfg = ConfigParser()
    cfg.read(args.cfg_db)

    db_routing = MongoClient()[cfg['Databases']['routing']]
    db_streetsavvy = MongoClient()[cfg['Databases']['streetsavvy']]
    # TODO authentication
    #db_routing.authenticate(cfg['Accounts'][''],
                                #cfg['Accounts'][''])
    coll_nodes = db_routing[cfg['Collections']['nodes']]
    coll_segments = db_routing[cfg['Collections']['segments']]
    coll_police = db_streetsavvy[cfg['Collections']['police_serv_calls']]

    image_dim = get_pixel_dimensions(geo_bounds, args.long_dim)
    #render(args.file_prefix, coll_nodes, coll_segments, geo_bounds, image_dim)
    #render_call(args.file_prefix, coll_nodes, coll_segments, geo_bounds, image_dim)
    render_calls(args.file_prefix, coll_police, coll_nodes, coll_segments, image_dim, geo_bounds, datetime(2017, 4, 15))

    #coll_snapshot = db_routing[cfg['Collections']['snapshot']]
    #render_snapshot('images/render_snapshot.png', coll_snapshot, geo_bounds, image_dim)

if __name__ == "__main__":
    main()
