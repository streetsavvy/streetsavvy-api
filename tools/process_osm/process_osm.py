"""
Store node and path information by processing an osm file

The way this works is that we take in a osm file and create and use
a osmium.SimpleHandler to run callbacks. In the Handler class,
the node and way functions are callbacks for when a node or way element
is encountered.

The osm files that are processed should already be filtered using tools
such as osmosis. This means removing nodse and ways in the osm file that
we don't want to store

The osmium package is required for this to work. Installing osmium
requires a lot of memory to build it so install osmium with caution.
"""
import logging
from argparse import ArgumentParser
from configparser import ConfigParser
from pymongo import MongoClient, GEOSPHERE, ASCENDING
from pymongo.errors import InvalidOperation, BulkWriteError
from geopy.distance import vincenty
import osmium

# Threshold when Mongo bulk write operators will execute
EXEC_THRESH = 10000

class Handler(osmium.SimpleHandler):
    """Handler for processing ways and nodes in an osm file"""
    def __init__(self, coll_nodes, coll_segments):
        osmium.SimpleHandler.__init__(self)
        self.coll_nodes = coll_nodes
        self.coll_segments = coll_segments
        self.bulk_nodes_cnt = 0
        self.bulk_segments_cnt = 0
        self.bulk_nodes = coll_nodes.initialize_unordered_bulk_op()
        self.bulk_segments = coll_segments.initialize_unordered_bulk_op()

    def apply_file(self, filename, locations=False):
        """
            Apply handlers to the specified osm file. Returned bulk
            writers should be executed (catching 'No operations to
            execute' just in case) to insert remaining docs into Mongo
        """
        super().apply_file(filename, locations)
        return self.bulk_nodes, self.bulk_segments

    def node(self, n):
        """Callback when node is encountered"""
        # Add node to Mongo
        doc = {'id': n.id,
               'loc': Handler.get_geo_json(n.location.lat, n.location.lon)}
        self.bulk_write(self.coll_nodes, doc)

    def way(self, w):
        """Callback when way is encountered"""
        # Iterate over the node list
        for i in range(len(w.nodes) - 1):
            node_1 = w.nodes[i]
            node_2 = w.nodes[i+1]

            # Calculate centroid of this segment
            centroid_lat = (node_1.lat + node_2.lat)/2
            centroid_lon = (node_1.lon + node_2.lon)/2
            centroid_geo_json = (
                Handler.get_geo_json(centroid_lat, centroid_lon))

            # Add the connection between node i and node i+1 to Mongo
            dist = vincenty(
                (node_1.lat, node_1.lon), (node_2.lat, node_2.lon)).meters
            doc = {'node_1': node_1.ref, 'node_2': node_2.ref,
                   'distance': dist, 'loc': centroid_geo_json}
            self.bulk_write(self.coll_segments, doc)

    @staticmethod
    def get_geo_json(lat, lon):
        """Get the given lat/lon in form of a geoJSON"""
        # NOTE: coordinates should be lon then lat when storing as geoJSON
        return {'type': 'Point', 'coordinates': [lon, lat]}

    def bulk_write(self, coll, doc):
        """Helper function to manage bulk writers"""
        if coll == self.coll_nodes:
            self.bulk_nodes.insert(doc)
            self.bulk_nodes_cnt += 1
            if self.bulk_nodes_cnt >= EXEC_THRESH:
                logging.debug(
                    "%s is above the threshold. Executing its bulk writer.",
                    self.coll_nodes.full_name)
                safe_bulk_exec(self.bulk_nodes)
                self.bulk_nodes = (
                    self.coll_nodes.initialize_unordered_bulk_op())
                self.bulk_nodes_cnt = 0
        elif coll == self.coll_segments:
            # TODO Decide to do upsert or just insert
            #self.bulk_segments.find({
            #    'node_1': doc['node_1'], 'node_2': doc['node_2']}).upsert(
            #    ).update({'$set': doc}) # Upsert
            self.bulk_segments.insert(doc)
            self.bulk_segments_cnt += 1
            if self.bulk_segments_cnt >= EXEC_THRESH:
                logging.debug(
                    "%s is above the threshold. Executing its bulk writer.",
                    self.coll_segments.full_name)
                safe_bulk_exec(self.bulk_segments)
                self.bulk_segments = (
                    self.coll_segments.initialize_unordered_bulk_op())
                self.bulk_segments_cnt = 0

def safe_bulk_exec(bulk):
    """
        Safely execute bulk.execute so that when there are no operations
        to execute or there is a duplicate key error, no exceptions are
        thrown
    """
    try:
        bulk.execute()
    except InvalidOperation as exc:
        if exc.args[0] != 'No operations to execute':
            raise exc
    except BulkWriteError as exc:
        logging.warning("Bulk write error encountered: %s", exc.details)
        for err in exc.details['writeErrors']:
            if err['code'] != 11000:
                raise exc

def process_osm(osm_file, coll_nodes, coll_segments):
    """
        Add nodes and segments from the osm file to Mongo

        Note: The only reason remove_unused parameter is there is for
        much smaller osm files. Most of the time processing osm files is
        because of removing unused nodes. So set remove_unused to True
        for the most optimal performance
    """
    logging.info("Processing osm file: %s", osm_file)

    # Make sure the collections have the proper indexes setup
    coll_nodes.create_index('id', unique=True)
    coll_nodes.create_index([('loc', GEOSPHERE)])
    coll_segments.create_index([('node_1', ASCENDING), ('node_2', ASCENDING)],
                               unique=True)
    coll_segments.create_index('node_2')
    coll_segments.create_index([('loc', GEOSPHERE)])

    h = Handler(coll_nodes, coll_segments)
    bulk_nodes, bulk_segments = h.apply_file(osm_file, locations=True)
    logging.info("Finished processing osm file")

    logging.info("Executing leftover bulks")
    safe_bulk_exec(bulk_nodes)
    safe_bulk_exec(bulk_segments)

def get_near(coll, lat, lon, max_dist, min_dist=0, loc_field='loc'):
    """
        Return a cursor of points near the given lat/lon. min_dist and
        max_dist are in meters.

        By default, the name of the location field assumed for the given
        collection is 'loc'
    """
    query = {
        loc_field: {
            '$near': {
                '$geometry': {'type': 'Point', 'coordinates': [lon, lat]},
                '$minDistance': min_dist,
                '$maxDistance': max_dist
            }
        }
    }
    return coll.find(query)

# TODO This is still incomplete
# TEMP The form of this function is going to be iffy but it'll give
# the general idea
def map_call(coll_segments, lat, lon, radius, crime_type):
    """
        Map the given call for service's latitude and longitude
        to a set of impacted centroids

        Radius is in meters
    """
    # Get centroids that are near the service call location
    cursor = get_near(coll_segments, lat, lon, radius)

    # Do something with this
    # I'm confused on what to do here

# TEMP
def recreate_coll(coll_nodes, coll_segments):
    """Recreate the collections"""
    logging.info("Recreating collections")
    coll_nodes.drop()
    #coll_nodes.create_index('id', unique=True)
    #coll_nodes.create_index([('loc', GEOSPHERE)])
    coll_segments.drop()
    #coll_segments.create_index([('node_1', ASCENDING), ('node_2', ASCENDING)],
    #                           unique=True)
    #coll_segments.create_index([('loc', GEOSPHERE)])

def main():
    """Main entry point function."""
    parser = ArgumentParser(
        description="Add nodes and segments from the given osm file to Mongo")
    parser.add_argument("-c", dest='cfg_db', default='db.ini',
                        help='ini file for configurations. Default is db.ini')
    parser.add_argument("osm_file", help='osm file to process')
    args = parser.parse_args()

    #logging.getLogger().setLevel(logging.INFO)
    fmt = "%(name)s:%(asctime)s:%(levelname)s:%(funcName)s:%(message)s"
    logging.basicConfig(filename='process_osm.log', level=logging.INFO,
                        format=fmt, filemode='w')
    logging.info("Starting process_osm.py")

    cfg = ConfigParser()
    cfg.read(args.cfg_db)

    db_routing = MongoClient()[cfg['Databases']['routing']]
    # TODO authentication
    #db_routing.authenticate(cfg['Accounts'][''],
                                #cfg['Accounts'][''])
    coll_nodes = db_routing[cfg['Collections']['nodes']]
    coll_segments = db_routing[cfg['Collections']['segments']]

    #recreate_coll(coll_nodes, coll_segments) # TEMP, drop collections
    process_osm(args.osm_file, coll_nodes, coll_segments)

    #logging.getLogger().setLevel(logging.DEBUG)

    logging.info("Finished. Clean exit.")

if __name__ == "__main__":
    main()
