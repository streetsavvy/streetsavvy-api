import os
import logging
from math import isclose
from configparser import ConfigParser
from pymongo import MongoClient, ASCENDING, GEOSPHERE
from pymongo.errors import BulkWriteError
from geopy.distance import vincenty
import pytest
from tools.process_osm.process_osm import (
    process_osm, get_near, Handler, safe_bulk_exec)

DB_INI = os.path.join(os.path.dirname(os.path.realpath(__file__)),
                      'test_db.ini')

# OSM file with a small number of nodes and ways
TEST_OSM_1 = os.path.join(os.path.dirname(os.path.realpath(__file__)),
                          '1.osm')
# OSM file with many nodes and ways for stress testing
TEST_OSM_2 = os.path.join(os.path.dirname(os.path.realpath(__file__)),
                          '2.osm')
# OSM file that has been filtered to have only the desired pedistrian
# nodes and ways
TEST_OSM_2_FILT = os.path.join(os.path.dirname(os.path.realpath(__file__)),
                               '2_filt.osm.pbf')
# TODO figure out why this is here. I forget what 3.osm is
TEST_OSM_3 = os.path.join(os.path.dirname(os.path.realpath(__file__)),
                          '3.osm')

@pytest.fixture()
def cfg():
    """Return initialized ConfigParser"""
    c = ConfigParser()
    c.read(DB_INI)
    return c

@pytest.fixture()
def coll_nodes(cfg):
    """
        Return a Mongo connection to the geo_cache collection
        that has been cleared
    """
    db_routing_name = cfg['Databases']['routing']
    coll_nodes_name = cfg['Collections']['nodes']

    # Make sure database/collection names starts with test so we don't
    # accidentally mess with production servers
    assert db_routing_name.startswith('test')
    assert coll_nodes_name.startswith('test')

    db_routing = MongoClient()[db_routing_name]
    # TODO authentication
    #db_routing.authenticate(cfg['Accounts'][''],
                                #cfg['Accounts'][''])
    coll_nodes = db_routing[coll_nodes_name]
    coll_nodes.drop()
    coll_nodes.create_index('id', unique=True)
    coll_nodes.create_index([('loc', GEOSPHERE)])
    assert coll_nodes.count() == 0

    return coll_nodes

@pytest.fixture()
def coll_segments(cfg):
    """
        Return a Mongo connection to the geo_cache collection
        that has been cleared
    """
    db_routing_name = cfg['Databases']['routing']
    coll_nodes_name = cfg['Collections']['segments']

    # Make sure database/collection names starts with test so we don't
    # accidentally mess with production servers
    assert db_routing_name.startswith('test')
    assert coll_nodes_name.startswith('test')

    db_routing = MongoClient()[db_routing_name]
    # TODO authentication
    #db_routing.authenticate(cfg['Accounts'][''],
                                #cfg['Accounts'][''])
    coll_segments = db_routing[coll_nodes_name]
    coll_segments.drop()
    coll_segments.create_index([('node_1', ASCENDING), ('node_2', ASCENDING)],
                               unique=True)
    coll_segments.create_index('node_2')
    coll_segments.create_index([('loc', GEOSPHERE)])
    assert coll_segments.count() == 0

    return coll_segments

def turn_on_debug_log():
    logging.getLogger().setLevel(logging.DEBUG)

@pytest.mark.mongo
def test_safe_bulk_exec_raises_no_errors_when_bulk_with_no_operations_is_given(
        coll_nodes):
    bulk = coll_nodes.initialize_unordered_bulk_op()
    safe_bulk_exec(bulk)

@pytest.mark.mongo
def test_safe_bulk_exec_raises_no_errors_when_bulk_write_receives_duplicate_key_error_in_coll_nodes(
        coll_nodes):
    bulk = coll_nodes.initialize_unordered_bulk_op()
    doc = {'id': 1}
    bulk.insert(doc)
    bulk.insert(doc)
    bulk.insert(doc)
    bulk.insert(doc)
    safe_bulk_exec(bulk)

@pytest.mark.mongo
def test_safe_bulk_exec_raises_no_errors_when_bulk_write_receives_duplicate_key_error_in_coll_segments(
        coll_segments):
    bulk = coll_segments.initialize_unordered_bulk_op()
    doc = {'node_1': 1, 'node_2': 2}
    bulk.insert(doc)
    bulk.insert({'node_1': 123123, 'node_2': 943209})
    bulk.insert(doc)
    bulk.insert(doc)
    safe_bulk_exec(bulk)

@pytest.mark.mongo
def test_process_osm_adds_nodes_segments_and_centroids_to_mongo(
        coll_nodes, coll_segments):
    process_osm(TEST_OSM_1, coll_nodes, coll_segments)

    cursor = coll_nodes.find().sort('id', ASCENDING)
    nodes = list(cursor)

    # Calculate centroid lat/lon
    coord_1 = nodes[0]['loc']['coordinates']
    coord_2 = nodes[1]['loc']['coordinates']
    centroid_lon = (coord_1[0] + coord_2[0])/2
    centroid_lat = (coord_1[1] + coord_2[1])/2

    assert nodes[0]['id'] == 1
    assert nodes[1]['id'] == 2

    cursor = coll_segments.find().sort('node_1', ASCENDING)
    segs = list(cursor)
    assert segs[0]['node_1'] == 1
    assert segs[0]['node_2'] == 2
    expected_dist = 443.02976715299644
    assert (isclose(segs[0]['distance'], expected_dist) or
            isclose(segs[0]['distance'], expected_dist, abs_tol=3))
    assert segs[0]['loc']['coordinates'][0] == centroid_lon
    assert segs[0]['loc']['coordinates'][1] == centroid_lat

@pytest.mark.mongo
def test_process_osm_doesnt_raises_err_on_add_duplicates(coll_nodes, coll_segments):
    process_osm(TEST_OSM_1, coll_nodes, coll_segments)
    node_count = coll_nodes.count()
    seg_count = coll_segments.count()
    process_osm(TEST_OSM_1, coll_nodes, coll_segments)
    assert node_count == coll_nodes.count()
    assert seg_count == coll_segments.count()

@pytest.mark.mongo
@pytest.mark.stress
def test_process_osm_stress(coll_nodes, coll_segments):
    process_osm(TEST_OSM_2, coll_nodes, coll_segments)
    assert coll_nodes.count() == 8477
    assert coll_segments.count() == 9648

@pytest.mark.mongo
@pytest.mark.stress
def test_process_osm_works_when_bulk_writers_write_during_apply_file_and_after(
        monkeypatch, coll_nodes, coll_segments):
    monkeypatch.setattr('tools.process_osm.process_osm.EXEC_THRESH', 100)
    process_osm(TEST_OSM_2, coll_nodes, coll_segments)

@pytest.mark.mongo
@pytest.mark.stress
def test_process_osm_works_when_bulk_writers_have_no_operations_after_apply_file(
        monkeypatch, coll_nodes, coll_segments):
    monkeypatch.setattr('tools.process_osm.process_osm.EXEC_THRESH', 8477)
    process_osm(TEST_OSM_2, coll_nodes, coll_segments)

@pytest.mark.parametrize('lat1, lon1, lat2, lon2, expected', [
    (38.898556, -77.037852, 38.897147, -77.043934, 549),
    (37.7612700, -122.4141600, 37.7690600, -122.3973200, 1716)
])
def test_vincenty_returns_tolerable_distance(lat1, lon1, lat2, lon2, expected):
    """Sanity check for vincenty from geopy"""
    dist = vincenty((lat1, lon1), (lat2, lon2)).meters
    assert (isclose(dist, expected, rel_tol=1e-3) or
            isclose(dist, expected, abs_tol=3))

@pytest.mark.mongo
def test_get_near_returns_cursor_with_near_locations(
        coll_nodes, coll_segments):
    process_osm(TEST_OSM_1, coll_nodes, coll_segments) # Setup test data

    cursor = get_near(coll_nodes, 37.7736354, -122.3976410, 100)
    near = list(cursor)
    assert near[0]['loc']['coordinates'][0] == -122.3976404
    assert near[0]['loc']['coordinates'][1] == 37.7736352

@pytest.mark.mongo
def test_process_osm_on_filtered_file_with_unused_nodes_removed_has_nodes_removed(
        coll_nodes, coll_segments):
    process_osm(TEST_OSM_2_FILT, coll_nodes, coll_segments)
    assert coll_nodes.count() == 553
    assert coll_segments.count() == 662
