# TODO Refactor this to fit the refactored render_heatmap.py
# pylint: disable=redefined-outer-name,invalid-name
import os
import json
import logging
import pytest
from PIL import Image
from tools.heatmap.render_heatmap import (
    get_pixel_dimensions, get_scores, get_buckets, get_color, draw_heatmap,
    render_heatmap_part, render_heatmap, write_bounds, split_pixel_bounds,
    stich, render_heatmap_parts, COLORS, MIN_SCORE, MAX_SCORE,
    PART_WIDTH, PART_HEIGHT)

# Dimensions for mock scores whose values are all 222
MOCK_222_WIDTH = 123
MOCK_222_HEIGHT = 253
MOCK_222_IMAGE_DIM = (MOCK_222_WIDTH, MOCK_222_HEIGHT)
MOCK_222_PIXEL_BOUNDS = (0, MOCK_222_HEIGHT, MOCK_222_WIDTH, 0)

TEST_GEO_BOUNDS = (-1, -1, 1, 1)

MOCK_WIDTH = 100
MOCK_HEIGHT = 100
MOCK_IMAGE_DIM = (MOCK_WIDTH, MOCK_HEIGHT)
MOCK_PIXEL_BOUNDS = (0, MOCK_HEIGHT, MOCK_WIDTH, 0)

@pytest.fixture()
def mock_model_222():
    """Create dummy model class that will only return scores of 222"""
    class Model222():
        def get_intensity(self, loc, time):
            return 222
    return Model222()

@pytest.fixture()
def mock_scores_222(mock_model_222):
    return get_scores(mock_model_222, TEST_GEO_BOUNDS, MOCK_222_IMAGE_DIM,
                      MOCK_222_PIXEL_BOUNDS)

@pytest.fixture()
def mock_model():
    """
        Return a dummy model which which will score it's upper half to
        be MIN_SCORE and its lower half to be MAX_SCORE when bounds are
        [-1, -1, 1, 1] (Or when the bounds have a center of (0, 0))
    """
    class Model():
        def get_intensity(self, loc, time):
            if loc[0] > 0:
                return MIN_SCORE
            else:
                return MAX_SCORE
    return Model()

@pytest.fixture()
def mock_scores(mock_model):
    """Return scores that would be generated from mock_model"""
    pixel_bounds = (0, 100, 20, 20)
    return get_scores(mock_model, TEST_GEO_BOUNDS, MOCK_IMAGE_DIM,
                      MOCK_PIXEL_BOUNDS)

def turn_on_debug_log():
    logging.getLogger().setLevel(logging.DEBUG)

@pytest.mark.parametrize('image_dim, part_width, part_height, expected', [
    (
        (100, 100), 50, 50, [
            [[0, 50, 50, 0], [50, 50, 100, 0]],
            [[0, 100, 50, 50], [50, 100, 100, 50]]
        ]
    ),
    (
        (100, 100), 67, 50, [
            [[0, 50, 67, 0], [67, 50, 100, 0]],
            [[0, 100, 67, 50], [67, 100, 100, 50]]
        ]
    ),
    (
        (100, 100), 50, 67, [
            [[0, 67, 50, 0], [50, 67, 100, 0]],
            [[0, 100, 50, 67], [50, 100, 100, 67]]
        ]
    ),
    (
        (100, 100), 67, 67, [
            [[0, 67, 67, 0], [67, 67, 100, 0]],
            [[0, 100, 67, 67], [67, 100, 100, 67]]
        ]
    )
])
def test_get_split_pixel_bounds_returns_proper_bounds(
        image_dim, part_width, part_height, expected):
    split_bounds = split_pixel_bounds(image_dim, part_width, part_height)
    assert split_bounds == expected

@pytest.mark.parametrize('geo_bounds, long_dim, expected', [
    ((38.897147, -77.043934, 38.898556, -77.037852), 1000, (231, 1000)),
    ((37.7612700, -122.4141600, 37.7690600, -122.3973200), 500, (231, 500))
])
def test_get_dimensons_returns_proper_dimensions(geo_bounds, long_dim, expected):
    dimensions = get_pixel_dimensions(geo_bounds, long_dim)
    assert dimensions == expected

def test_get_scores_returns_2d_array_with_proper_dimensions(mock_model_222):
    scores = get_scores(mock_model_222, TEST_GEO_BOUNDS, MOCK_222_IMAGE_DIM,
                        MOCK_222_PIXEL_BOUNDS)
    assert len(scores) == MOCK_222_HEIGHT
    for row in scores:
        assert len(row) == MOCK_222_WIDTH

@pytest.mark.parametrize('min, max, num_buckets, expected', [
    (0, 100, 2, [(0, 50), (50, 100)]),
    (50, 225, 3, [(50, 108.33333333333334),
                  (108.33333333333334, 166.66666666666669),
                  (166.66666666666669, 225.00000000000003)])
])
def test_get_buckets_returns_proper_buckets(min, max, num_buckets, expected):
    buckets = get_buckets(min, max, num_buckets)
    assert buckets == expected

@pytest.mark.parametrize('score, expected', [
    (0, COLORS[-1]),
    (MAX_SCORE, COLORS[0]),
])
def test_get_color_returns_proper_colors(score, expected):
    assert get_color(score) == expected

def test_draw_heatmap_creates_image_file(tmpdir, mock_scores_222):
    out_file = tmpdir.join('img.png')
    assert out_file.check() is False
    draw_heatmap(str(out_file), mock_scores_222)
    assert out_file.check() is True

def test_draw_heatmap_creates_image_file_with_right_colors_and_right_size(
        tmpdir, mock_scores):
    out_file = tmpdir.join('img.png')
    assert out_file.check() is False
    draw_heatmap(str(out_file), mock_scores)
    assert out_file.check() is True

    image = Image.open(str(out_file))
    assert image.size == (len(mock_scores[0]), len(mock_scores))

    pixels = image.load()
    # Check that the upper half is the color of COLORS[-1]
    assert pixels[0, 0] == COLORS[-1]
    # Check that the lower half is the color of COLORS[0]
    assert pixels[MOCK_WIDTH-1, MOCK_HEIGHT-1] == COLORS[0]

def test_render_heatmap_part_creates_image_file_with_right_colors_and_right_size(
        tmpdir, mock_model):
    file = tmpdir.join('img.png')
    assert file.check() is False
    render_heatmap_part(str(file), mock_model, TEST_GEO_BOUNDS,
                        MOCK_IMAGE_DIM, MOCK_PIXEL_BOUNDS)
    assert file.check() is True

    image = Image.open(str(file))
    width = MOCK_PIXEL_BOUNDS[2] - MOCK_PIXEL_BOUNDS[0]
    height = MOCK_PIXEL_BOUNDS[1] - MOCK_PIXEL_BOUNDS[3]
    assert image.size == (width, height)

    pixels = image.load()
    # Check that the upper half is the color of COLORS[-1]
    assert pixels[0, 0] == COLORS[-1]
    # Check that the lower half is the color of COLORS[0]
    assert pixels[MOCK_WIDTH-1, MOCK_HEIGHT-1] == COLORS[0]

def test_render_heatmap_parts_creates_image_files_with_right_colors_and_right_sizes(
        tmpdir, mock_model):
    file_prefix = str(tmpdir.join('heatmap'))
    files, split_bounds, image_dim = render_heatmap_parts(
        file_prefix, mock_model, TEST_GEO_BOUNDS, int(PART_WIDTH*1.1))
    assert image_dim == get_pixel_dimensions(TEST_GEO_BOUNDS, int(PART_WIDTH*1.1))
    for y, row_files in enumerate(files):
        for x, img_file in enumerate(row_files):
            image = Image.open(str(img_file))
            width = split_bounds[y][x][2] - split_bounds[y][x][0]
            height = split_bounds[y][x][1] - split_bounds[y][x][3]
            assert image.size == (width, height)

            pixels = image.load()
            if y == 0: # First two images is the upper half
                # Which should be COLORS[-1]
                assert pixels[0, 0] == COLORS[-1]
            else: # Second two images is the lower half
                # Which should be COLORS[0]
                assert pixels[width-1, height-1] == COLORS[0]

def test_write_bounds_writes_geo_bounds_to_json_file(tmpdir):
    geo_bounds = (-0.1, -0.1, 0.1, 0.1)
    out_file = str(tmpdir.join('heatmap_bounds.json'))
    write_bounds(geo_bounds, out_file)

    with open(out_file, 'r') as file:
        data = json.load(file)
    assert data['bounds'] == list(geo_bounds)

def test_stich_stiches_images_properly_and_is_right_size(tmpdir, mock_model):
    file_prefix = str(tmpdir.join('heatmap'))
    files, split_bounds, image_dim = render_heatmap_parts(
        file_prefix, mock_model, TEST_GEO_BOUNDS, int(PART_WIDTH*1.1))
    out_file = tmpdir.join('heatmap.png')
    stich(files, str(out_file), split_bounds, image_dim)

    assert out_file.check() is True

    image = Image.open(str(out_file))
    assert image.size == image_dim

    pixels = image.load()

    # Check that the upper half is the color of COLORS[-1]
    assert pixels[0, 0] == COLORS[-1]

    # Check that the lower half is the color of COLORS[0]
    assert pixels[image_dim[0]-1, image_dim[1]-1] == COLORS[0]

def test_render_heatmap_renders_heatmap(tmpdir, mock_model):
    file_prefix = str(tmpdir.join('heatmap'))
    geo_bounds = (-122.4442, 37.7486, -122.3768, 37.7781)
    heatmap = render_heatmap(file_prefix, mock_model, geo_bounds, 1000)
    assert os.path.isfile(heatmap)
