"""
Module to render the heatmap image
"""
import logging
import json
import functools
from argparse import ArgumentParser
from configparser import ConfigParser
from datetime import datetime
from geopy.distance import vincenty, great_circle
from pymongo import MongoClient
from PIL import Image, ImageDraw
import tools.model.ssmodel as ssmodel
from multiprocessing import Pool

# TEMP for TEMP Model
SCORE_AT_POINT = 500
DIST = 100

# Scores to help choose appropriate colors for the heatmap
# Points closer to the max score will have a hotter color
# Points closer to the min score will have a cooler color
# MIN_SCORE = 0
# MAX_SCORE = 500
# MAX_SCORE = 10

"""
Pixel dimensions that the whole image will be split up into. E.g.:
If the image ends up being 1000x1000 pixels and the PART_WIDTH and
PART_HEIGHT are both 500, the image will be split up into 4 500x500
images and be rendered separately and then stiched back together
"""
PART_WIDTH = 50
PART_HEIGHT = PART_WIDTH

IMAGE_FMT = 'png'

COLORS = [(255, 0, 0), # Hottest, red
          (255, 53, 0),
          (255, 107, 0),
          (255, 161, 0),
          (255, 214, 0),
          (241, 214, 0),
          (241, 255, 0),
          (187, 255, 0),
          (134, 255, 0),
          (80, 255, 0),
          (26, 255, 0),
          (0, 255, 26),
          (0, 255, 80),
          (0, 255, 187),
          (0, 255, 241),
          (0, 214, 255),
          (0, 161, 255),
          (0, 107, 255),
          (0, 53, 255),
          (0, 0, 255), # Coolest, blue
         ]


# TEMP
class Model():
    def __init__(self):
        self.points = []
        self.b = SCORE_AT_POINT
        self.m = (0 - self.b)/DIST

    def add_point(self, pt):
        self.points.append(pt)

    def get_intensity(self, loc):
        score = 0
        # TODO Optimize in actual model, only consider points within
        # certain threshold of the location
        for p in self.points:
            dist = great_circle((p[0], p[1]), loc).meters
            # y = mx + b
            curr_score = self.m*dist + self.b
            if curr_score > 0:
                score += curr_score
        return score

class Model2():
    def __init__(self, coll_snapshot):
        self.points = []
        self.b = SCORE_AT_POINT
        self.m = (0 - self.b)/DIST
        self.coll_snapshot = coll_snapshot

    def get_intensity(self, loc):
        score = 0
        #logging.debug("Getting score for %f, %f", lat, lon)
        for p in get_points(self.coll_snapshot, loc[0], loc[1], 200, loc_field='_id'):
            weight = p['weight']
            coord = p['_id']['coordinates']

            b = weight
            m = (0 - MAX_SCORE)/50

            dist = great_circle((coord[1], coord[0]), loc).meters
            # y = mx + b
            curr_score = m*dist + b
            #logging.debug("Score: %d", curr_score)
            if curr_score > 0:
                score += curr_score
        return score

def ll_to_pixel(lat, lon, bounds, dim):
    """Lat/lon to pixel"""
    adj_lat = lat - bounds[1]
    adj_lon = lon - bounds[0]

    delta_lat = bounds[3] - bounds[1]
    delta_lon = bounds[2] - bounds[0]

    lon_frac = adj_lon/delta_lon
    lat_frac = adj_lat/delta_lat

    x = int(lon_frac*dim[0])
    y = int((1-lat_frac)*dim[1])

    return x, y

def pixel_to_ll(x, y, bounds, dim):
    """Pixel to lat long"""
    delta_lat = bounds[3] - bounds[1]
    delta_lon = bounds[2] - bounds[0]

    x_frac = float(x)/dim[0]
    y_frac = float(y)/dim[1]

    lon = bounds[0] + x_frac*delta_lon
    lat = bounds[3] - y_frac*delta_lat

    # TODO Figure out why this is here
    calc_x, calc_y = ll_to_pixel(lat, lon, bounds, dim)
    if abs(calc_x-x) > 1 or abs(calc_y-y) > 1:
        print("Mismatch: %s, %s => %s %s" % (
            x, y, calc_x, calc_y))

    return lat, lon

# TEMP
def draw_circle(draw, center, radius, outline=None, fill=None):
    xy = [(center[0] - radius, center[1] - radius),
          (center[0] + radius, center[1] + radius)]
    draw.ellipse(xy, outline=outline, fill=fill)

def get_color(score, buckets):
    """Get a color based off the given score"""
    if score <= buckets[0][0]:
        return COLORS[-1]
    elif score >= buckets[-1][1]:
        return COLORS[0]
    else:
        for index, bucket in enumerate(buckets):
            if bucket[0] <= score < bucket[1]:
                # Dirty fix to choose color from other side
                return COLORS[index*-1-1]

def get_buckets(max_value, num_buckets):
    """Split a range between min_value and max_value into buckets"""
    buckets = [None] * num_buckets
    buckets[num_buckets - 1] = (max_value*0.60, max_value)
    for i in range(num_buckets-2, 0, -1):
        buckets[i] = (buckets[i+1][0]*0.60, buckets[i+1][0])
    buckets[0] = (0, buckets[i+1][0])
    return buckets

# TEMP
def generate_mock_model():
    model = Model()

    #model.add_point((37.76505, -122.40942))

    #model.add_point((37.76292, -122.41109))
    #model.add_point((37.75597, -122.40942))

    # SE of the one above
    #model.add_point((37.75384, -122.40544))

    model.add_point((37.7651, -122.4095))
    model.add_point((37.7640, -122.4003))
    model.add_point((37.76212, -122.40346))
    model.add_point((37.76200, -122.40430))
    model.add_point((37.76051, -122.40951))
    model.add_point((37.76041, -122.41134))

    return model

def get_pixel_dimensions(geo_bounds, long_dim):
    """
        Get a pixel width and height based on the given bounds and a
        given dimension which will be the pixel length of the returned
        longer dimension. The reason why pixel dimensions are given this
        way is to prevent image skewing. For example, if the given
        bounds have a large width to height ratio, then the pixel
        dimensions need to scale accordingly to maintain this ratio. In
        this scenario, the returned width will be equal to the long_dim.

        :param bounds Bounding box of lat/longs to render. Should be in
        the form of (left, bottom, right, top), aka
        (min_lon, min_lat, max_lon, max_lat)
        :type bounds tuple
        :param long_dim Desired pixel length of the longer dimension
        :type int

        :return Tuple containing (width, height)
        :return type: tuple
    """
    delta_x = geo_bounds[2] - geo_bounds[0]
    delta_y = geo_bounds[3] - geo_bounds[1]
    if delta_y > delta_x:
        height = long_dim
        width = int((delta_x/delta_y)*height)
    else:
        width = long_dim
        height = int((delta_y/delta_x)*width)
    return (width, height)

def get_scores(bounds_args, model, geo_bounds, image_dim,
        model_time=datetime.now(), call_list=None, fast=False,
        ):
    """
        Get the scores the model outputs for each pixel inside
        pixel_bounds.
    """
    pixel_bounds = bounds_args[2]
    width = pixel_bounds[2] - pixel_bounds[0]
    height = pixel_bounds[1] - pixel_bounds[3]

    scores = [[0]*width for _ in range(height)]
    for y in range(pixel_bounds[3], pixel_bounds[1]):
        for x in range(pixel_bounds[0], pixel_bounds[2]):
            lat, lon = pixel_to_ll(x, y, geo_bounds, image_dim)
            if fast:
                score = model.get_intensity_fast((lat, lon), model_time)
            else:
                if call_list is None:
                    score = model.get_intensity((lat, lon), model_time)
                else:
                    score = model.get_intensity((lat, lon), model_time,
                                                calls=call_list)
            scores[y-pixel_bounds[3]][x-pixel_bounds[0]] = score
        # Log progress every 10% if possible
        if height >= 10 and y % int(height/10) == 0:
            logging.debug((
                "Scoring Progress. Width: %d, Height: %d. " +
                "At: (%d, %d)"), width, height, x, y)
    return ((bounds_args[0], bounds_args[1]), scores)

def split_pixel_bounds(image_dim, part_width, part_height):
    """Split up the given image dimensions into smaller parts"""
    # There's probably a much better way to do this
    # This is essentially splitting a rectangle into smaller rectangles
    split_bounds = []

    width, height = image_dim

    num_width_parts = int(width // part_width)
    last_width = None
    if not width % part_width == 0:
        last_width = width - (num_width_parts * part_width)
        num_width_parts += 1

    num_height_parts = int(height // part_height)
    last_height = None
    if not height % part_height == 0:
        last_height = height - (num_height_parts * part_height)
        num_height_parts += 1

    for y in range(num_height_parts):
        row_bounds = []
        for x in range(num_width_parts):
            # If at bottom right most edge
            if (x == (num_width_parts - 1) and y == (num_height_parts - 1) and
                    last_width is not None and last_height is not None):
                curr_bound = [
                    x*part_width,
                    y*part_height + last_height,
                    x*part_width + last_width,
                    y*part_height
                ]
             # At right most edge
            elif x == (num_width_parts - 1) and last_width is not None:
                curr_bound = [
                    x*part_width,
                    (y+1)*part_height,
                    x*part_width + last_width,
                    y*part_height
                ]
             # At bottom most edge
            elif y == (num_height_parts - 1) and last_height is not None:
                curr_bound = [
                    x*part_width,
                    y*part_height + last_height,
                    (x+1)*part_width,
                    y*part_height
                ]
            else:
                curr_bound = [
                    x*part_width,
                    (y+1)*part_height,
                    (x+1)*part_width,
                    y*part_height
                ]
            row_bounds.append(curr_bound)
        split_bounds.append(row_bounds)
    return split_bounds

def draw_heatmap(args, files, max_score):
    """Draw the heatmap with each pixel corresponding to a score"""
    r, scores = args
    logging.info("Y=%d, X=%d", r[0], r[1])
    out_file = files[r[0]][r[1]]
    logging.debug("Rendering: %s", out_file)
    width = len(scores[0])
    height = len(scores)
    image = Image.new('RGB', (width, height))
    pixels = image.load()
    buckets = get_buckets(max_score, len(COLORS))
    for y in range(height):
        for x in range(width):
            pixels[x, y] = get_color(scores[y][x], buckets)
        # Log progress every 10% if possible
        if height >= 10 and y % int(height/10) == 0:
            logging.debug(
                "draw_heatmap Progress. Width: %d, Height: %d. " +
                "At: (%d, %d)", width, height, x, y)
    image.save(out_file, format=IMAGE_FMT)

def stich(files, out_file, split_bounds, image_dim):
    """Combine all the given image files"""
    logging.info(
        "Stiching %d files. Resulting image will be %d x %d " +
        "(width x height)", len(files)*len(files[0]),
        image_dim[0], image_dim[1])
    new_image = Image.new('RGB', image_dim)

    for y, row_files in enumerate(files):
        for x, file in enumerate(row_files):
            image = Image.open(str(file))
            topleft_corner = (split_bounds[y][x][0], split_bounds[y][x][3])
            logging.debug("Pasting %s into top left corner: %s", file, topleft_corner)
            new_image.paste(image, topleft_corner)

    new_image.save(out_file)
    logging.info("Done stiching image")

def render_heatmap_parts(file_prefix, model, geo_bounds, long_dim,
        model_time=datetime.now(), call_list=None, fast=False):
    """
        Render the heatmap (in multiple parts if necessary)

        :param file_prefix
        :type file_prefix
        :param model
        :type model
        :param geo_bounds Bounding box of lat/longs to render. Should
        be in the form of (left, bottom, right, top), aka
        (min_lon, min_lat, max_lon, max_lat)
        :type geo_bounds tuple
        :param long_dim Desired pixel length of the longer dimension for
        each image that will be generated
        :type long_dim int

        :return List of files that are rendered parts of the heatmap,
        list of pixel bounds that the rendering was split into and
        the pixel dimensions of the whole image
    """
    image_dim = get_pixel_dimensions(geo_bounds, long_dim)
    split_bounds = split_pixel_bounds(image_dim, PART_WIDTH, PART_HEIGHT)
    files = [['']*len(row_pixel_bounds) for row_pixel_bounds in split_bounds]

    logging.info("Number of images to render: %d", len(files)*len(files[0]))
    logging.info("The whole geographical bounds to render are %s", geo_bounds)
    logging.info("The split pixel bounds to render are %s", split_bounds)

    # Generate a list of arguments to allow for parallel processing
    render_args = list()
    for y, row_bounds in enumerate(split_bounds):
        for x, bounds in enumerate(row_bounds):
            files[y][x] = file_prefix + "_{0}_{1}.{2}".format(y, x, IMAGE_FMT)
            render_args.append((y, x, bounds))
    # Multiprocess scores
    pool = Pool(8)
    get_scores_multi = functools.partial(get_scores, model=model, geo_bounds=geo_bounds,
          image_dim=image_dim, model_time=model_time, call_list=call_list, fast=fast)
    results = pool.map(get_scores_multi, render_args)
    # Prevent new processes from being added to the pool
    pool.close()
    # Wait for the pool to finish
    pool.join()
    logging.info('Calculating Max Score')
    max_score = 0
    for r in results:
        section_max = max(list(map(max, r[1])))
        if section_max >= max_score:
             max_score = section_max
    logging.info("Max Score: %f", max_score)
    draw_heatmap_multi = functools.partial(draw_heatmap, files=files, max_score=max_score)
    pool = Pool(2)
    pool.map(draw_heatmap_multi, results)
    pool.close()
    pool.join()

    logging.info("Heatmap rendered into the files: %s", files)
    logging.info("Done rendering heatmap parts")
    return files, split_bounds, image_dim

def render_heatmap(file_prefix, model, geo_bounds, long_dim,
        model_time=datetime.now(), call_list=None, fast=False):
    """Render the heatmap"""
    files, split_bounds, image_dim = (
        render_heatmap_parts(file_prefix, model, geo_bounds, long_dim,
                             model_time, call_list, fast))
    out_file = file_prefix + "." + IMAGE_FMT
    stich(files, out_file, split_bounds, image_dim)
    logging.info("Heatmap rendered into file: '%s'", out_file)
    return out_file

def write_bounds(geo_bounds, out_file):
    """Write geographical bounds into a json file"""
    with open(out_file, 'w') as open_file:
        json.dump({'bounds': geo_bounds}, open_file)

def get_points(coll, lat, lon, max_dist, min_dist=0, loc_field='loc'):
    """
        Return a cursor to geographical points in the given bounds of
        the given location
    """
    query = {
        loc_field: {
            '$near': {
                '$geometry': {'type': 'Point', 'coordinates': [lon, lat]},
                '$minDistance': min_dist,
                '$maxDistance': max_dist
            }
        },
        'weight': {'$gt': 0}
    }
    return coll.find(query)

# TEMP
def do_render_heatmap():
    model = generate_mock_model()
    bounds = (-122.4442, 37.7486, -122.3768, 37.7781)
    render_heatmap('images/heatmap', model, bounds, 500)

# TEMP
def generate_mock_model_2(coll_snapshot):
    return Model2(coll_snapshot)

def main():
    """Main entry point function."""
    parser = ArgumentParser(
        description=(
            "Render heatmap images based on a model over a given lat/lon " +
            "bounds. Multiple images are rendered in the form of a 2D array " +
            "which can be stitched together to get the whole heatmap over " +
            "the bounds."))
    parser.add_argument("-c", dest='cfg_db', default='db.ini',
                        help='ini file for configurations. Default is db.ini')
    parser.add_argument(
        "file_prefix",
        help=("Outputted file prefixes. Heatmaps will be stored in the " +
              "form of the file_prefix followed by _x_y.png where x and y " +
              "represent a x, y coordinates. Add the directory to this if " +
              "these images should be saved in a specified directory. " +
              "e.g.: 'images/heatmap'"))
    parser.add_argument(
        "long_dim", type=int,
        help=("Pixel length of the longer dimension. The other dimension " +
              "(whether its the width or the height will be calculated " +
              "based on this and the given bounds"))
    parser.add_argument("left", type=float, help="Left bound (min lon)")
    parser.add_argument("bottom", type=float, help="Bottom bound (min lat)")
    parser.add_argument("right", type=float, help="Right bound (max lon)")
    parser.add_argument("top", type=float, help="Top bound (max_lat)")
    parser.add_argument("--date", default='2017-04-15 12:00:00')
    args = parser.parse_args()

    fmt = "%(name)s:%(asctime)s:%(levelname)s:%(funcName)s:%(message)s"
    logging.basicConfig(level=logging.DEBUG, format=fmt)
    logging.info("Starting render_heatmap.py")

    geo_bounds = (args.left, args.bottom, args.right, args.top)

    cfg = ConfigParser()
    cfg.read(args.cfg_db)

    model = ssmodel.SSModel(args.cfg_db)
    #model_time = datetime(2017, 4, 15)
    model_time = datetime.strptime(args.date, '%Y-%m-%d %H:%M:%S')
    logging.debug("Model Time: %s", model_time)

    call_list = model.get_calls_list(model_time)
    logging.debug("Number of calls: %d", len(call_list))

    write_bounds(geo_bounds, args.file_prefix + '_bounds.json')

    #logging.info("Slow render_heatmap")
    #render_heatmap(args.file_prefix, model, geo_bounds, args.long_dim,
    #    model_time, call_list=call_list)

    logging.info("Fast render_heatmap")
    logging.info("Setup fast on model side")
    model.setup_fast(model_time)
    logging.info("Start rendering")
    render_heatmap("{0}_fast".format(args.file_prefix), model, geo_bounds, args.long_dim,
        model_time, fast=True)

    # render map on top
    from tools.process_osm.render import render_on_top
    db_routing = MongoClient()[cfg['Databases']['routing']]
    # TODO authentication
    #db_routing.authenticate(cfg['Accounts'][''],
                                #cfg['Accounts'][''])
    coll_nodes = db_routing[cfg['Collections']['nodes']]
    coll_segments = db_routing[cfg['Collections']['segments']]
    render_on_top(
            '{0}_fast.png'.format(args.file_prefix),
            '{0}_fast_with_map'.format(args.file_prefix),
            coll_nodes, coll_segments, geo_bounds, args.long_dim)

if __name__ == "__main__":
    main()
