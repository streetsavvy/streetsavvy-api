"""
"""
# TODO authentication for all mongo database/collection accesses
# TODO refactor so that we're not calling the main function of another
# module and modifying sys.argv
import sys
import time
import json
import logging
from argparse import ArgumentParser
from configparser import ConfigParser
from datetime import datetime, timedelta
from pymongo import MongoClient, GEOSPHERE, ASCENDING, DESCENDING
from tools.routing.routing import get_graph, write_graph, read_graph, get_path
from tools.mapper.map_desc import main as map_desc_main
from tools.routing.update_calls import main as update_calls_main
from tools.routing.take_snapshot import main as snapshot_main
from tools.routing.seg_snap import generate_seg_snap
from tools.routing.render_route import render_route
from tools.model.ssmodel import SSModel
from tools.heatmap.render_heatmap import render_heatmap
from tools.process_osm.render import render_on_top, render_segments

CFG_DB = 'cfg/test_bed_db.ini'
NODES_JSON = 'data/nodes.json'
SEGMENTS_JSON = 'data/segments.json'

MAPPING_CFG = 'cfg/test_bed_mapping.cfg'
MAPPING_LOG = 'logs/map_desc'
MAPPING_JSON = 'data/mapping.json'
KEYS_JSON = 'data/keys.json'
SEVERITY_JSON = 'data/severity.json'
WEIGHTS_JSON = 'data/weights.json'

UPDATE_CFG = 'cfg/test_bed_update.cfg'
UPDATE_LOG = 'logs/update_calls'

SNAPSHOT_LOG = 'logs/take_snapshot'

GRAPH_FILE = 'data/graph.gpickle'

HEATMAP_PREFIX = 'data/heatmap'
#DEFAULT_GEO_BOUNDS = (-122.41400, 37.76240, -122.40390, 37.76708)
#DEFAULT_GEO_BOUNDS = (-122.4188, 37.7601, -122.3986, 37.7695)
#DEFAULT_GEO_BOUNDS = (-122.4255, 37.7555, -122.3906, 37.7748)
DEFAULT_GEO_BOUNDS = (-122.5175, 37.7007, -122.3506, 37.8204) # All of San Francisco
DEFAULT_LONG_DIM = 1000

# cfg file key names
DATABASES = ['streetsavvy', 'routing']
COLLECTIONS = ['police_serv_calls', 'fire_serv_calls', 'geocode_cache',
    'geo_api_rates', 'geo_exc', 'nodes', 'segments', 'snapshot',
    'timed_edges']

# cfg file key names for mapping.cfg
MAPPING_DB = ['source', 'source_service']
MAPPING_COLL = ['mapping_coll', 'keys_coll', 'severity_coll',
                'weight_coll', 'police_coll']

# cfg file key names for update.cfg
UPDATE_DB = ['source', 'source_service']
UPDATE_COLL = ['node_coll', 'segment_coll', 'snapshot_coll',
               'timed_segment_coll', 'police_coll']

INSERT_LIMIT = 5000

def drop_and_check(coll):
    """Helper function to drop a collection and check that it was dropped"""
    logging.info("Drop and check collection '%s'", coll.full_name)
    coll.drop()
    assert coll.count() == 0

def recreate_coll(cfg, map_cfg, recreate=False, recreate_ns=False,
        recreate_map=False):
    logging.info("Recreating desired collections")

    db_routing = MongoClient()[cfg['Databases']['routing']]
    db_streetsavvy = MongoClient()[cfg['Databases']['streetsavvy']]
    db_mapping = MongoClient()[map_cfg['mongo']['source']]

    logging.debug("Recreating service_calls.police collection")
    coll_police = db_streetsavvy[cfg['Collections']['police_serv_calls']]
    drop_and_check(coll_police)
    coll_police.create_index('crime_id', unique=True)
    coll_police.create_index([('loc', GEOSPHERE)])
    add_mock_police_data(coll_police)

    updated_police_calls = False
    if recreate or recreate_ns:
        logging.debug("Recreating routing.nodes collection")
        coll_nodes = db_routing[cfg['Collections']['nodes']]
        drop_and_check(coll_nodes)
        coll_nodes.create_index('id', unique=True)
        coll_nodes.create_index([('loc', GEOSPHERE)])
        mongo_import_nodes(coll_nodes, NODES_JSON)

        logging.debug("Recreating routing.segments collection")
        coll_segments = db_routing[cfg['Collections']['segments']]
        drop_and_check(coll_segments)
        coll_segments.create_index([('node_1', ASCENDING), ('node_2', ASCENDING)],
                                   unique=True)
        coll_segments.create_index('node_2')
        coll_segments.create_index([('loc', GEOSPHERE)])
        mongo_import_segments(coll_segments, SEGMENTS_JSON)

    if recreate or recreate_map:
        logging.debug("Recreating crime_types.crime_cache collection")
        coll_mapping = db_mapping[map_cfg['mongo']['mapping_coll']]
        drop_and_check(coll_mapping)
        mongo_import_mapping(coll_mapping, MAPPING_JSON)

        logging.debug("Recreating crime_types.crime_keys collection")
        coll_keys = db_mapping[map_cfg['mongo']['keys_coll']]
        drop_and_check(coll_keys)
        mongo_import_keys(coll_keys, KEYS_JSON)

        logging.debug("Recreating crime_types.severity collection")
        coll_severity = db_mapping[map_cfg['mongo']['severity_coll']]
        drop_and_check(coll_severity)
        mongo_import_severity(coll_severity, SEVERITY_JSON)

        logging.debug("Recreating crime_types.weights collection")
        coll_weights = db_mapping[map_cfg['mongo']['weight_coll']]
        drop_and_check(coll_weights)
        mongo_import_weights(coll_weights, WEIGHTS_JSON)

def replace_numberLong(json_parsed, field):
    """Helper function to get rid of $numberLong inside json file"""
    if (not isinstance(json_parsed[field], int) and
            '$numberLong' in json_parsed[field]):
        json_parsed[field] = int(json_parsed[field]['$numberLong'])

def mongo_import_nodes(coll_nodes, data_file):
    logging.info("Importing nodes data file into mongo")
    logging.debug("Collection: %s", coll_nodes.full_name)
    start_time = time.process_time()
    data = []
    count = 0
    with open(data_file, 'r') as open_file:
        for line in open_file:
            json_parsed = json.loads(line)
            json_parsed.pop('_id', None)
            replace_numberLong(json_parsed, 'id')
            data.append(json_parsed)

            count += 1
            if count % INSERT_LIMIT == 0:
                coll_nodes.insert_many(data)
                data = []
    if len(data) != 0:
        coll_nodes.insert_many(data)
    elapsed = time.process_time() - start_time
    logging.info("Elapsed time: %f seconds", elapsed)

def mongo_import_segments(coll_segments, data_file):
    logging.info("Importing segments data file into mongo")
    logging.debug("Collection: %s", coll_segments.full_name)
    start_time = time.process_time()
    data = []
    count = 0
    with open(data_file, 'r') as open_file:
        for line in open_file:
            json_parsed = json.loads(line)
            json_parsed.pop('_id', None)
            replace_numberLong(json_parsed, 'node_1')
            replace_numberLong(json_parsed, 'node_2')
            data.append(json_parsed)

            count += 1
            if count % INSERT_LIMIT == 0:
                coll_segments.insert_many(data)
                data = []
    if len(data) != 0:
        coll_segments.insert_many(data)
    elapsed = time.process_time() - start_time
    logging.info("Elapsed time: %f seconds", elapsed)

def add_mock_police_data(coll_police):
    logging.info("Adding mock police data")
    logging.debug("Collection: %s", coll_police.full_name)
    #call_dttm = datetime.now() - timedelta(minutes=30)
    call_dttm = datetime(2017, 4, 15, 12, 0, 0)
    call_date = datetime(call_dttm.year, call_dttm.month,
                         call_dttm.day)
    coll_police.insert_one(
        {
            "city" : "San Francisco",
            "call_date" : call_date,
            "state" : "CA",
            "original_crimetype_name" : "Poss",
            "call_time" : "02:37",
            "agency_id" : "1",
            "call_dttm" : call_dttm,
            "crime_id" : "160930368",
            "offense_date" : call_date,
            "disposition" : "UTL",
            "address" : "16th St/potrero Av",
            "report_date" : call_date,
            "address_type" : "Intersection",
            "latitude" : 37.7657933,
            "longitude" : -122.407567,
            "crimetype" : "n/a",
            "weight" : 5,
            "loc" : {
                    "type" : "Point",
                    "coordinates" : [
                            -122.407567,
                            37.7657933
                    ]
            }
        }
    )
    #call_dttm = datetime.now() - timedelta(hours=1)
    call_dttm = datetime(2017, 4, 15, 12, 0, 0)
    call_date = datetime(call_dttm.year, call_dttm.month,
                         call_dttm.day)
    #coll_police.insert_one(
    #    {
    #        "city" : "San Francisco",
    #        "call_date" : call_date,
    #        "state" : "CA",
    #        #"original_crimetype_name" : "Encampment",
    #        "call_time" : "21:47",
    #        "agency_id" : "1",
    #        "call_dttm" : call_dttm,
    #        "crime_id" : "160923618",
    #        "offense_date" : call_date,
    #        "disposition" : "CIT",
    #        "address" : "300 Block Of Alabama St",
    #        "report_date" : call_date,
    #        "address_type" : "Premise Address",
    #        "latitude" : 37.7654913,
    #        "longitude" : -122.4124403,
    #        "crimetype" : "explosion/bomb threat",
    #        "weight" : 5,
    #        "loc" : {
    #                "type" : "Point",
    #                "coordinates" : [
    #                        -122.4124403,
    #                        37.7654913
    #                ]
    #        }
    #    }
    #)

def mongo_import_mapping(coll_mapping, data_file):
    logging.info("Importing mapping data file into mongo")
    logging.debug("Collection: %s", coll_mapping.full_name)
    start_time = time.process_time()
    data = []
    with open(data_file, 'r') as open_file:
        for line in open_file:
            json_parsed = json.loads(line)
            json_parsed.pop('_id', None)
            data.append(json_parsed)
    coll_mapping.insert_many(data)
    elapsed = time.process_time() - start_time
    logging.info("Elapsed time: %f seconds", elapsed)

def mongo_import_keys(coll_keys, data_file):
    logging.info("Importing keys data file into mongo")
    logging.debug("Collection: %s", coll_keys.full_name)
    start_time = time.process_time()
    data = []
    with open(data_file, 'r') as open_file:
        for line in open_file:
            json_parsed = json.loads(line)
            json_parsed.pop('_id', None)
            data.append(json_parsed)
    coll_keys.insert_many(data)
    elapsed = time.process_time() - start_time
    logging.info("Elapsed time: %f seconds", elapsed)

def mongo_import_severity(coll_severity, data_file):
    logging.info("Importing severity data file into mongo")
    logging.debug("Collection: %s", coll_severity.full_name)
    start_time = time.process_time()
    data = []
    with open(data_file, 'r') as open_file:
        for line in open_file:
            json_parsed = json.loads(line)
            json_parsed.pop('_id', None)
            data.append(json_parsed)
    coll_severity.insert_many(data)
    elapsed = time.process_time() - start_time
    logging.info("Elapsed time: %f seconds", elapsed)

def mongo_import_weights(coll_weights, data_file):
    logging.info("Importing weights data file into mongo")
    logging.debug("Collection: %s", coll_weights.full_name)
    start_time = time.process_time()
    data = []
    with open(data_file, 'r') as open_file:
        for line in open_file:
            json_parsed = json.loads(line)
            json_parsed.pop('_id', None)
            data.append(json_parsed)
    coll_weights.insert_many(data)
    elapsed = time.process_time() - start_time
    logging.info("Elapsed time: %f seconds", elapsed)

def update_police_calls():
    logging.info("Running map_desc script to update police calls")
    sys.argv = ['map_desc.py', '-c', MAPPING_CFG, '-l', MAPPING_LOG]
    map_desc_main()

def setup(cfg, map_cfg, update_cfg, model=None, model_time=None,
        geo_bounds=None, recreate=False, recreate_ns=False, recreate_map=False,
        recreate_seg_snap=False, recreate_graph=False):
    logging.info("Setting up test bed")

    recreate_coll(cfg, map_cfg, recreate, recreate_ns, recreate_map)

    if recreate or recreate_seg_snap:
        db_routing = MongoClient()[cfg['Databases']['routing']]
        coll_seg_snap = db_routing[cfg['Collections']['seg_snap']]
        coll_segments = db_routing[cfg['Collections']['segments']]
        generate_seg_snap(model, coll_seg_snap, coll_segments, model_time,
                          geo_bounds)

    if recreate or recreate_graph:
        logging.info("Generating graph file")
        db_routing = MongoClient()[cfg['Databases']['routing']]
        coll_nodes = db_routing[cfg['Collections']['nodes']]
        coll_edges = db_routing[cfg['Collections']['seg_snap']]
        graph = get_graph(coll_nodes, coll_edges, bounds=geo_bounds,
                          distance=False)
        write_graph(graph, GRAPH_FILE)

def check_cfgs(cfg, map_cfg, update_cfg):
    """Ensure databases/collections are test ones"""
    logging.info("check_cfg")

    logging.debug("Checking %s", CFG_DB)
    for db in DATABASES:
        logging.debug("Checking Database key '%s' starts with 'test_bed'", db)
        assert cfg['Databases'][db].startswith('test_bed')
    for coll in COLLECTIONS:
        logging.debug("Checking Collections key '%s' starts with 'test_bed'", coll)
        assert cfg['Collections'][coll].startswith('test_bed')

    logging.debug("Checking %s", MAPPING_CFG)
    for db in MAPPING_DB:
        logging.debug("Checking Database key '%s' starts with 'test_bed'", db)
        assert map_cfg['mongo'][db].startswith('test_bed')
    for coll in MAPPING_COLL:
        logging.debug("Checking Collections key '%s' starts with 'test_bed'", coll)
        assert map_cfg['mongo'][coll].startswith('test_bed')

    logging.debug("Checking %s", UPDATE_CFG)
    for db in UPDATE_DB:
        logging.debug("Checking Database key '%s' starts with 'test_bed'", db)
        assert update_cfg['mongo'][db].startswith('test_bed')
    for coll in UPDATE_COLL:
        logging.debug("Checking Collections key '%s' starts with 'test_bed'", coll)
        assert update_cfg['mongo'][coll].startswith('test_bed')

def main():
    """Main entry point function."""
    parser = ArgumentParser(
        description="TOOO Description")
    parser.add_argument('-r', action='store_true', dest='recreate',
        help="Recreate all collections and the graph file")
    parser.add_argument(
        '--ns', action='store_true', dest='recreate_ns',
        help="Recreate nodes and segments collections")
    parser.add_argument(
        '-m', action='store_true', dest='recreate_map',
        help="Recreate mapping collections")
    parser.add_argument(
        '-s', action='store_true', dest='recreate_seg_snap',
        help="Recreate seg_snap collection")
    parser.add_argument(
        '-g', action='store_true', dest='recreate_graph',
        help="Recreate graph file")
    parser.add_argument(
        '--heat', action='store_true', dest='recreate_heatmap',
        help="Recreate heatmap")
    parser.add_argument(
        '--geo', nargs='+', dest='geo_bounds', default=DEFAULT_GEO_BOUNDS,
        help="Geographical bounds for heatmap generation in the form of: " +
             "left bottom right top bounds")
    parser.add_argument(
        '--long_dim', type=int, dest='long_dim', default=DEFAULT_LONG_DIM,
        help="Pixel length of the longer dimension of the heatmap")
    parser.add_argument(
        '--route', action='store_true', dest='gen_route',
        help="Generate routes and display them")
    args = parser.parse_args()

    fmt = "%(name)s:%(asctime)s:%(levelname)s:%(funcName)s:%(message)s"
    logging.basicConfig(level=logging.DEBUG, format=fmt)
    logging.info("Starting mock_data.py")

    cfg = ConfigParser()
    cfg.read(CFG_DB)
    map_cfg = ConfigParser()
    map_cfg.read(MAPPING_CFG)
    update_cfg = ConfigParser()
    update_cfg.read(UPDATE_CFG)

    model = SSModel(CFG_DB)
    #model_time = datetime.now()
    model_time = datetime(2017, 4, 15, 12, 0, 0)
    logging.info("Model Time: %s", model_time)

    check_cfgs(cfg, map_cfg, update_cfg)
    setup(cfg, map_cfg, update_cfg, model, model_time, args.geo_bounds,
          args.recreate, args.recreate_ns, args.recreate_map,
          args.recreate_seg_snap, args.recreate_graph)

    # Generate heatmap
    if args.recreate_heatmap:
        #logging.info("Rendering slow heatmap with bounds %s and long dim %f",
        #             args.geo_bounds, args.long_dim)
        #call_list = model.get_calls_list(model_time)
        #render_heatmap(HEATMAP_PREFIX, model, args.geo_bounds, args.long_dim,
        #               model_time, call_list=call_list)

        logging.info("Rendering fast heatmap with bounds %s and long dim %f",
                     args.geo_bounds, args.long_dim)
        logging.info("Model Time: %s", model_time)
        model.setup_fast(model_time)
        render_heatmap("{0}_fast".format(HEATMAP_PREFIX), model,
                       args.geo_bounds, args.long_dim,
                       model_time, fast=True)

        #logging.info("Rendering map on top of slow heatmap")
        #db_routing = MongoClient()[cfg['Databases']['routing']]
        #coll_nodes = db_routing[cfg['Collections']['nodes']]
        #coll_segments = db_routing[cfg['Collections']['segments']]
        #render_on_top(
        #    '{0}.png'.format(HEATMAP_PREFIX),
        #    '{0}_with_map'.format(HEATMAP_PREFIX),
        #    coll_nodes, coll_segments, args.geo_bounds, args.long_dim)

        logging.info("Rendering map on top of fast heatmap")
        db_routing = MongoClient()[cfg['Databases']['routing']]
        coll_nodes = db_routing[cfg['Collections']['nodes']]
        coll_segments = db_routing[cfg['Collections']['segments']]
        render_on_top(
            '{0}_fast.png'.format(HEATMAP_PREFIX),
            '{0}_fast_with_map'.format(HEATMAP_PREFIX),
            coll_nodes, coll_segments, args.geo_bounds, args.long_dim)


        #model_time = datetime(2017, 4, 15, 13, 0, 0)
        #logging.info("Rendering fast heatmap with bounds %s and long dim %f",
        #             args.geo_bounds, args.long_dim)
        #logging.info("Model Time: %s", model_time)
        #model.setup_fast(model_time)
        #render_heatmap("{0}_fast_1_hr".format(HEATMAP_PREFIX), model,
        #               args.geo_bounds, args.long_dim,
        #               model_time, fast=True)
        #logging.info("Rendering map on top of fast heatmap")
        #db_routing = MongoClient()[cfg['Databases']['routing']]
        #coll_nodes = db_routing[cfg['Collections']['nodes']]
        #coll_segments = db_routing[cfg['Collections']['segments']]
        #render_on_top(
        #    '{0}_fast_1_hr.png'.format(HEATMAP_PREFIX),
        #    '{0}_fast_1_hr_with_map'.format(HEATMAP_PREFIX),
        #    coll_nodes, coll_segments, args.geo_bounds, args.long_dim)
        #
        #model_time = datetime(2017, 4, 15, 17, 0, 0)
        #logging.info("Rendering fast heatmap with bounds %s and long dim %f",
        #             args.geo_bounds, args.long_dim)
        #logging.info("Model Time: %s", model_time)
        #model.setup_fast(model_time)
        #render_heatmap("{0}_fast_5_hr".format(HEATMAP_PREFIX), model,
        #               args.geo_bounds, args.long_dim,
        #               model_time, fast=True)
        #logging.info("Rendering map on top of fast heatmap")
        #db_routing = MongoClient()[cfg['Databases']['routing']]
        #coll_nodes = db_routing[cfg['Collections']['nodes']]
        #coll_segments = db_routing[cfg['Collections']['segments']]
        #render_on_top(
        #    '{0}_fast_5_hr.png'.format(HEATMAP_PREFIX),
        #    '{0}_fast_5_hr_with_map'.format(HEATMAP_PREFIX),
        #    coll_nodes, coll_segments, args.geo_bounds, args.long_dim)

    # Generate a route and display it in an image
    if args.gen_route:
        db_routing = MongoClient()[cfg['Databases']['routing']]
        coll_nodes = db_routing[cfg['Collections']['nodes']]
        coll_segments = db_routing[cfg['Collections']['segments']]
        g = read_graph(GRAPH_FILE)
        lat1, lon1 = 37.7655, -122.4106
        lat2, lon2 = 37.7499, -122.4116
        p = get_path(g, coll_nodes, lat1, lon1, lat2, lon2, distance=False)
        logging.info("Path: %s", p)
        render_route('data/route', p, coll_nodes, coll_segments,
                     args.geo_bounds, 5000)

if __name__ == "__main__":
    main()
