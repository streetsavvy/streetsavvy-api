"""Tests for ssgeo"""
# pylint: disable=redefined-outer-name,invalid-name

# Standard Libs
import os
from configparser import ConfigParser
from datetime import datetime
from math import isclose

# 3rd Party Libs
import pytest
from pymongo import MongoClient

# Internal Libs
from tools.ssgeo.ssgeo import SSGeo

DB_INI = os.path.join(os.path.dirname(os.path.realpath(__file__)),
                      'test_db.ini')

@pytest.fixture()
def geo():
    """Return instance of SSGeo"""
    return SSGeo(DB_INI)

@pytest.fixture()
def cfg():
    """Return initialized ConfigParser"""
    c = ConfigParser()
    c.read(DB_INI)
    return c

@pytest.fixture()
def coll_geo_cache(cfg):
    """
        Return a Mongo connection to the geo_cache collection
        that has been cleared
    """
    db_streetsavvy_name = cfg['Databases']['streetsavvy']
    coll_geo_cache_name = cfg['Collections']['geocode_cache']

    # Make sure database/collection names starts with test so we don't
    # accidentally mess with production servers
    assert db_streetsavvy_name.startswith('test')
    assert coll_geo_cache_name.startswith('test')

    db_streetsavvy = MongoClient()[db_streetsavvy_name]
    db_streetsavvy.authenticate(cfg['Accounts']['user_scraper'],
                                cfg['Accounts']['pw_scraper'])
    coll_geo_cache = db_streetsavvy[coll_geo_cache_name]
    coll_geo_cache.drop()
    assert coll_geo_cache.count() == 0

    return coll_geo_cache

@pytest.fixture()
def coll_geo_cache_with_data(coll_geo_cache):
    """
        Return a Mongo connection to the geo_cache collection
        that has data
    """
    coll_geo_cache.insert_one(
        {
            'address': '3401 Market Street',
            'city': 'Philadelphia',
            'state': 'PA',
            'longitude': -75.1917294,
            'latitude': 39.9561749
        }
    )
    coll_geo_cache.insert_one(
        {
            'address': 'Embarcadero & Washington',
            'state': 'CA',
            'longitude': -122.395477,
            'latitude': 37.796728
        }
    )
    assert coll_geo_cache.count() == 2
    return coll_geo_cache

@pytest.fixture()
def coll_geo_api_rates_with_data(cfg):
    """
        Return a Mongo connection to the geo_api_rates collection
        that has been initialized
    """
    db_streetsavvy_name = cfg['Databases']['streetsavvy']
    coll_geo_api_rates_name = cfg['Collections']['geo_api_rates']

    # Make sure database/collection names starts with test so we don't
    # accidentally mess with production servers
    assert db_streetsavvy_name.startswith('test')
    assert coll_geo_api_rates_name.startswith('test')

    db_streetsavvy = MongoClient()[db_streetsavvy_name]
    db_streetsavvy.authenticate(cfg['Accounts']['user_scraper'],
                                cfg['Accounts']['pw_scraper'])
    coll_geo_api_rates = db_streetsavvy[coll_geo_api_rates_name]
    coll_geo_api_rates.drop()
    assert coll_geo_api_rates.count() == 0

    # Initialize geo_api_rates with mock data
    coll_geo_api_rates.insert_one(
        {
            'callsperday': 2500,
            'date': datetime(2016, 11, 8, 0, 44, 7, 258),
            'geocoder': 'GoogleV3',
            'callstoday': 2409,
            'apikey': cfg['APIKeys']['GoogleV3']
        }
    )
    coll_geo_api_rates.insert_one(
        {
            'callsperday': 341,
            'date': datetime(2016, 11, 8, 0, 44, 7, 253),
            'geocoder': 'Bing',
            'callstoday': 300,
            'apikey': cfg['APIKeys']['Bing']
        }
    )
    assert coll_geo_api_rates.count() == 2
    return coll_geo_api_rates

@pytest.fixture()
def coll_geo_exc(cfg):
    """
        Return a Mongo connection to the geo_cache collection
        that has been cleared
    """
    db_streetsavvy_name = cfg['Databases']['streetsavvy']
    coll_geo_exc_name = cfg['Collections']['geo_exc']

    # Make sure database/collection names starts with test so we don't
    # accidentally mess with production servers
    assert db_streetsavvy_name.startswith('test')
    assert coll_geo_exc_name.startswith('test')

    db_streetsavvy = MongoClient()[db_streetsavvy_name]
    db_streetsavvy.authenticate(cfg['Accounts']['user_scraper'],
                                cfg['Accounts']['pw_scraper'])
    coll_geo_exc = db_streetsavvy[coll_geo_exc_name]
    coll_geo_exc.drop()
    assert coll_geo_exc.count() == 0

    return coll_geo_exc

@pytest.fixture()
def coll_geo_exc_with_data(coll_geo_exc):
    """
        Return a Mongo connection to the geo_exc collection
        that has data
    """
    coll_geo_exc.insert_one(
        {
            'address': 'Oak and cole Ca 7nfx217 Red Honda Hr-V',
            'state': 'CA'
        }
    )
    coll_geo_exc.insert_one(
        {
            'address': '100 Block Of Leavenworth St #Lobby',
            'city': 'San Francisco',
            'state': 'CA'
        }
    )
    coll_geo_exc.insert_one(
        {
            'address': 'Embarcadero & Washington',
            'state': 'CA',
        }
    )
    assert coll_geo_exc.count() == 3
    return coll_geo_exc

@pytest.mark.mongo
@pytest.mark.geocode
@pytest.mark.usefixtures('coll_geo_api_rates_with_data')
def test_getlatlon_not_cached(geo, coll_geo_cache):
    """
        Test that SSGeo.getlatlon returns the proper lat/lon when
        not in the geocode cache and that it caches the location
        on a successful geocode
    """
    address = "3401 Market Street"
    state = "PA"
    city = "Philadelphia"
    (lat, lon, cache) = geo.getlatlon(address, state, city)
    assert isclose(lat, 39.956, rel_tol=1e-03)
    assert isclose(lon, -75.191, rel_tol=1e-03)
    assert cache is False

    # Check that the geocoded location was cached
    doc = coll_geo_cache.find_one(
        {
            'address': address,
            'state': state,
            'city': city
        }
    )
    # Make sure only one entry for the cache was created
    # If the returned value is a dict, it means that only
    # one entry was returned.
    assert isinstance(doc, dict)

@pytest.mark.mongo
@pytest.mark.usefixtures('coll_geo_api_rates_with_data',
                         'coll_geo_cache_with_data')
def test_getlatlon_cached(geo):
    """
        Test that SSGeo.getlatlon returns the proper lat/lon when
        the location is in geocode cache
    """
    address = "3401 Market Street"
    state = "PA"
    city = "Philadelphia"
    (lat, lon, cache) = geo.getlatlon(address, state, city)
    assert isclose(lat, 39.956, rel_tol=1e-03)
    assert isclose(lon, -75.191, rel_tol=1e-03)
    assert cache is True

@pytest.mark.mongo
@pytest.mark.geocode
@pytest.mark.usefixtures('coll_geo_api_rates_with_data',
                         'coll_geo_cache')
def test_getlatlon_with_bad_info(geo):
    """
        Test that SSGeo.getlatlon returns (None, None, None) when
        given a location that does not compute properly
    """
    (lat, lon, cache) = geo.getlatlon("wherwhr234293842",
                                      "sdklj3s", "dkj1sad")
    assert lat is None
    assert lon is None
    assert cache is None

def test_getlatlon_with_empty_state_returns_none(geo):
    """
        Test that SSGeo.getlatlon returns (None, None, None) when
        given a state that is an empty string
    """
    (lat, lon, cache) = geo.getlatlon("3301 Market Street", "", "Philadelphia")
    assert lat is None
    assert lon is None
    assert cache is None

@pytest.mark.mongo
@pytest.mark.usefixtures('coll_geo_exc_with_data')
def test_isingeoexc_returns_true_when_loc_in_geo_exc(geo):
    """
        Make sure isingeoexc returns True when given a location that
        exists in the geo_exc collection
    """
    assert geo.isingeoexc('100 Block Of Leavenworth St #Lobby',
                          'CA', 'San Francisco') is True

@pytest.mark.mongo
@pytest.mark.usefixtures('coll_geo_exc_with_data')
def test_isingeoexc_returns_true_when_loc_without_city_in_geo_exc(geo):
    """
        Make sure isingeoexc returns True when given a location that
        exists in the geo_exc collection and the location does not have
        a city specified
    """
    assert geo.isingeoexc('Oak and cole Ca 7nfx217 Red Honda Hr-V',
                          'CA') is True

@pytest.mark.mongo
@pytest.mark.usefixtures('coll_geo_exc_with_data')
def test_isingeoexc_returns_false_when_loc_not_in_geo_exc(geo):
    """
        Make sure isingeoexc returns False when given a location that
        does not exist in the geo_exc collection
    """
    assert geo.isingeoexc('Something not in the collection',
                          'Not a State') is False

@pytest.mark.mongo
@pytest.mark.parametrize('address, state, city', [
    ('300 Block Of Ofarrell St', 'CA', 'San Francisco'),
    ('3301 Market Street', 'PA', 'Philadelphia'),
    ('13th St and gateview Ave', 'CA', '')
    ])
def test_addgeoexc_adds_loc_to_coll(geo, coll_geo_exc, address, state, city):
    """
        Make sure addgeoexc adds the specified location to the geo_exc
        collection.
    """
    geo.addgeoexc(address, state, city)
    if city != '':
        doc = coll_geo_exc.find_one({'address': address, 'state': state,
                                     'city': city})
    else:
        doc = coll_geo_exc.find_one({'address': address, 'state': state})
    assert isinstance(doc, dict)

@pytest.mark.parametrize('test_input, expected', [
    ('34th/Market St', '34th and Market St'),
    ('13th St/gateview Ave', '13th St and gateview Ave')
    ])
def test_modifyaddr_with_addr_containing_fwdslash(test_input, expected):
    """
        Test that modifyaddr replaces an address containing a forward
        slash with the string ' and '
    """
    assert SSGeo.modifyaddr(test_input) == expected

@pytest.mark.parametrize('test_input, expected', [
    ('300 Block Of Ofarrell St', '300 Block Of Ofarrell St'),
    ('3301 Market Street', '3301 Market Street')
    ])
def test_modifyaddr_doesnt_change_addr_without_fwdslash(test_input, expected):
    """
        Test that modifyaddr replaces an address containing a forward
        slash with the string ' and '
    """
    assert SSGeo.modifyaddr(test_input) == expected

@pytest.mark.mongo
@pytest.mark.usefixtures('coll_geo_cache', 'coll_geo_exc_with_data')
def test_getlatlon_returns_none_when_loc_in_geo_exc(geo):
    """
        Test that getlatlon returns (None, None, None) when getlatlon
        is given a location that is in the geo_exc collection and not
        in the geocode cache.
    """
    lat, lon, cache = geo.getlatlon('Oak and cole Ca 7nfx217 Red Honda Hr-V',
                                    'CA')
    assert lat is None and lon is None and cache is None

@pytest.mark.mongo
@pytest.mark.usefixtures('coll_geo_cache')
def test_getlatlon_doesnt_modify_geo_exc_when_loc_in_geo_exc(
        geo, coll_geo_exc_with_data):
    """
        Test that getlatlon does not make any modifications to the
        geo_exc collection when given a location that is in the geo_exc
        collection and not in the geocode cache.
    """
    count = coll_geo_exc_with_data.count()
    geo.getlatlon('Oak and cole Ca 7nfx217 Red Honda Hr-V', 'CA')
    # Make sure no new docs were added
    assert count == coll_geo_exc_with_data.count()

@pytest.mark.mongo
@pytest.mark.geocode
@pytest.mark.usefixtures('coll_geo_api_rates_with_data')
def test_getlatlon_adds_to_geo_exc_when_given_bad_addr(
        geo, coll_geo_exc):
    """
        Test that getlatlon returns (None, None, None) when getlatlon
        is given a location will not be able to be geocoded and that
        the location is added to the geo_exc collection
    """
    address = 'awer3019230as'
    state = 'ai3u2jl'
    lat, lon, cache = geo.getlatlon(address, state)
    assert lat is None and lon is None and cache is None

    doc = coll_geo_exc.find_one({'address': address, 'state': state})
    assert isinstance(doc, dict)

@pytest.mark.mongo
@pytest.mark.usefixtures(
    'coll_geo_api_rates_with_data', 'coll_geo_cache_with_data',
    'coll_geo_exc_with_data'
    )
def test_getlatlon_with_loc_in_geo_exc_and_in_cache_returns_latlon(geo):
    """
        Test that SSGeo.getlatlon returns the proper lat/lon when
        the location is in geocode cache and the location is in the
        geo_exc collection.
    """
    address = 'Embarcadero & Washington'
    state = 'CA'
    (lat, lon, cache) = geo.getlatlon(address, state)
    assert isclose(lat, 37.796, rel_tol=1e-03)
    assert isclose(lon, -122.395, rel_tol=1e-03)
    assert cache is True

@pytest.mark.mongo
@pytest.mark.usefixtures('coll_geo_api_rates_with_data',
                         'coll_geo_cache_with_data')
def test_getlatlon_with_loc_in_geo_exc_and_in_cache_deletes_exc(
        geo, coll_geo_exc_with_data):
    """
        Test that when SSGeo.getlatlon runs with the location in the
        geocode cache and in geo_exc, it deletes the doc in geo_exc.
    """
    address = 'Embarcadero & Washington'
    state = 'CA'
    count = coll_geo_exc_with_data.count()
    doc = coll_geo_exc_with_data.find_one({'address': address, 'state': state})
    assert isinstance(doc, dict)

    geo.getlatlon(address, state)

    # Assert a doc was deleted
    assert coll_geo_exc_with_data.count() == (count - 1)
    doc = coll_geo_exc_with_data.find_one({'address': address, 'state': state})
    assert doc is None
