"""Tests for geopy_ratelimited"""
# pylint: disable=redefined-outer-name,invalid-name

# Standard Libs
import os
from configparser import ConfigParser
from math import isclose
from datetime import datetime

# 3rd Party Libs
import pytest
from pymongo import MongoClient

# Internal Libs
from tools.ssgeo.geopy_ratelimited import GeopyRateLimited

DB_INI = os.path.join(os.path.dirname(os.path.realpath(__file__)),
                      'test_db.ini')

@pytest.fixture(scope='module')
def geopy_rate():
    """Return instance of GeopyRateLimited"""
    return GeopyRateLimited(DB_INI)

@pytest.fixture(scope='module')
def cfg():
    """Return initialized ConfigParser"""
    config = ConfigParser()
    config.read(DB_INI)
    return config

@pytest.fixture(scope='module')
def coll_geo_api_rates(cfg):
    """
        Return a Mongo connection to the geo_api_rates collection
        that has been cleared
    """
    db_streetsavvy_name = cfg['Databases']['streetsavvy']
    coll_geo_api_rates_name = cfg['Collections']['geo_api_rates']

    # Make sure database/collection names starts with test so we don't
    # accidentally mess with production servers
    assert db_streetsavvy_name.startswith('test')
    assert coll_geo_api_rates_name.startswith('test')

    db_streetsavvy = MongoClient()[db_streetsavvy_name]
    db_streetsavvy.authenticate(cfg['Accounts']['user_scraper'],
                                cfg['Accounts']['pw_scraper'])
    coll_geo_api_rates = db_streetsavvy[coll_geo_api_rates_name]
    coll_geo_api_rates.delete_many({})
    assert coll_geo_api_rates.count() == 0

    return coll_geo_api_rates

@pytest.mark.mongo
@pytest.mark.geocode
def test_first_geocode_of_date(cfg, geopy_rate, coll_geo_api_rates):
    """
        Test geopy_ratelimited adds documents for
        geocoders that have not been used for the current day
    """
    # Initialize geo_api_rates with mock data
    coll_geo_api_rates.insert_one(
        {
            'callsperday': 2500,
            'date': datetime(2016, 11, 8, 0, 44, 7, 258),
            'geocoder': 'GoogleV3',
            'callstoday': 2409,
            'apikey': cfg['APIKeys']['GoogleV3']
        }
    )
    coll_geo_api_rates.insert_one(
        {
            'callsperday': 341,
            'date': datetime(2016, 11, 8, 0, 44, 7, 253),
            'geocoder': 'Bing',
            'callstoday': 341,
            'apikey': cfg['APIKeys']['Bing']
        }
    )

    geopy_rate.geocode("3201 Market Street, Philadelphia, PA 19104")
    assert coll_geo_api_rates.count() == 4 # Make sure the docs were added

    # Make sure the docs are in the correct format
    starttime = datetime.today().replace(hour=0, minute=0,
                                         second=0, microsecond=0)
    endtime = datetime.today().replace(hour=23, minute=59,
                                       second=59, microsecond=999999)
    doc = coll_geo_api_rates.find_one(
        {'date': {'$gte': starttime, '$lt': endtime}})
    assert isinstance(doc['callsperday'], int)
    assert isinstance(doc['geocoder'], str)
    assert doc['date'].date() == datetime.today().date()

@pytest.mark.mongo
@pytest.mark.geocode
def test_geocode_increments_calls(geopy_rate, coll_geo_api_rates):
    """
        Test geopy_ratelimited adds increments callstoday counter
        when it geocodes something
    """
    starttime = datetime.today().replace(hour=0, minute=0,
                                         second=0, microsecond=0)
    endtime = datetime.today().replace(hour=23, minute=59,
                                       second=59, microsecond=999999)
    docs = coll_geo_api_rates.find(
        {'date': {'$gte': starttime, '$lt': endtime}}).sort('geocoder', 1)
    prev_counts = [] # callstoday counts before geocoding something
    for doc in docs:
        prev_counts.append(doc['callstoday'])

    geopy_rate.geocode("3201 Market Street, Philadelphia, PA 19104")

    # Check if one of the callstoday counts have increased
    docs = coll_geo_api_rates.find(
        {'date': {'$gte': starttime, '$lt': endtime}}).sort('geocoder', 1)
    i = 0
    for doc in docs:
        # If we found a callstoday count that has increased by one
        if doc['callstoday'] == (prev_counts[i] + 1):
            return # The test has succeeded
        i += 1
    assert 0 # If we reached this point, the test has failed

@pytest.mark.mongo
@pytest.mark.geocode
def test_geocode_with_number_block_city_and_state(geopy_rate):
    """
        Test geopy_ratelimited can geocode a properly formatted query
        indicating the number block of a street with a city and a state
    """
    loc = geopy_rate.geocode("400 Block Of Mcallister St, San Francisco, CA")
    assert isclose(loc.longitude, -122.419, rel_tol=1e-03)
    assert isclose(loc.latitude, 37.780, rel_tol=1e-03)

@pytest.mark.mongo
@pytest.mark.geocode
def test_geocode_with_intersetcion_city_and_state(geopy_rate):
    """
        Test geopy_ratelimited can geocode a properly formatted query
        indicating an intersection with a city and a state
    """
    loc = geopy_rate.geocode("Albion St and 16th St, San Francisco, CA")
    assert isclose(loc.longitude, -122.422, rel_tol=1e-03)
    assert isclose(loc.latitude, 37.764, rel_tol=1e-03)

@pytest.mark.mongo
def test_geocode_exceeded_limits(cfg, geopy_rate, coll_geo_api_rates):
    """
        Test that a ValueError is raised if all geocoders have exceeded
        their limits
    """
    # Setup database to make it seem like geocoders are at their limits
    coll_geo_api_rates.delete_many({})
    assert coll_geo_api_rates.count() == 0
    coll_geo_api_rates.insert_one(
        {
            'callsperday': 2500,
            'date': datetime.today(),
            'geocoder': 'GoogleV3',
            'callstoday': 2500,
            'apikey': cfg['APIKeys']['GoogleV3']
        }
    )
    coll_geo_api_rates.insert_one(
        {
            'callsperday': 341,
            'date': datetime.today(),
            'geocoder': 'Bing',
            'callstoday': 341,
            'apikey': cfg['APIKeys']['Bing']
        }
    )
    with pytest.raises(ValueError):
        geopy_rate.geocode("3201 Market Street, Philadelphia, PA 19104")
