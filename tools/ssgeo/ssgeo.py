"""Provides SSGeo, the a class for performing geocoding."""

# Standard Libs
import os
from configparser import ConfigParser

# 3rd Party Libs
from pymongo import MongoClient

# Internal Libs
from tools.ssgeo.geopy_ratelimited import GeopyRateLimited

class SSGeo:
    """
        StreetSavvy Geocoding class. With this class, you can retrieve
        the lat/lon of a given location. This class checks the database
        cache for a location's lat/lon. If it's not there, it will
        geocode the location leveraging `GeopyRateLimited`. Afterwards,
        it will cache the lat/lon for future use.
    """

    def __init__(self, cfg_db="db.ini"):
        if not os.path.isfile(cfg_db):
            raise ValueError("cfg_db file '" + cfg_db + "' does not exist")
        self.cfg_db = cfg_db

    def getlatlon(self, address, state, city=""):
        """
            Return latitude/longitude tuple of the given address,
            city, and state. The third value of the tuple indicates
            whether this value was retrieved from the cache.

            `address`, `city` and `state` should be strings.

            Some locations do not give a city, so adding a city is
            optional, though it will improve geocoding accuracy.

            This function checks the database's `geo_exc` first to see
            if the location was attempted to be geocoded before but
            failed. If it finds it in `geo_exc`, then this function will
            stop and return (None, None, None). If it does not, it will
            check the database's `geocode_cache` to get a lat/long.
            If it does not find it there, it will geocode the location
            leveraging `GeopyRateLimited` and store the lat/long in the
            cache. In the event that the location is unable to be
            geocoded, the location is added to `geo_exc` so that it
            will not be attempted to be geocoded again in the future.

            Example Return Values:
             - (None, None, None): failed to retriece the lat/lon
             - (Lat, Lon, True): lat/lon retrieved from cache
             - (Lat, Lon, False): lat/lon not retrieved from cache
        """
        # If there's no state or address, we can't geocode this
        if (state == "") or (address == ""):
            # TODO Raise proper exception
            print("Unable to geocode (It's missing a state or address): " +
                  "{address: '" + address + "', state: '" + state +
                  "', city: '" + city + "'}")
            return (None, None, None)

        # Make the location easier to geocode
        address = SSGeo.modifyaddr(address)

        ingeoexc = False
        # If we failed geocoding the location before, take notice of it
        if self.isingeoexc(address, state, city):
            ingeoexc = True

        (lat, lon) = self.getlatlon_cached(address, state, city)
        # If retrieving from cache was successful
        if (lat is not None) and (lon is not None):
            # If this location was also in the geo_exc collection
            if ingeoexc:
                # Delete the doc in the geo_exc collection because it
                # has been taken care of since it's in the geocode cache
                # collection now
                print("Location inside geo_exc and is also in cache. " +
                      "Deleting it inside geo_exc: {address: '" + address +
                      "', state: '" + state + "', city: '" + city + "'}")
                self.delgeoexc(address, state, city)

            return (lat, lon, True)
        # If the location was not in the cache and it was found in
        # geo_exc, return so that we don't waste the effort in trying to
        # geocode it again
        elif ingeoexc:
            # TODO Raise proper exception
            print("Location inside geo_exc and not cached. Skipping: " +
                  "{address: '" + address + "', state: '" + state +
                  "', city: '" + city + "'}")
            return (None, None, None)

        # Geocode the lat/lon if it's not cached
        geopy_rate = GeopyRateLimited(self.cfg_db)

        # Format the location info into a query string for the geocoder
        query = address
        if city != "":
            query += ", " + city
        query += ", " + state

        loc = geopy_rate.geocode(query)
        if loc is not None:
            # Geocoding location was successful
            self.cache_location(loc.latitude, loc.longitude,
                                address, state, city)
            return (loc.latitude, loc.longitude, False)
        else:
            # TODO raise proper exception
            print("Location not in cache and unable to geocode. " +
                  "Adding it to the geo_exc collection: " +
                  "{address: '" + address + "', state: '" + state +
                  "', city: '" + city + "'}")
            self.addgeoexc(address, state, city)
            return (None, None, None)

    def getlatlon_cached(self, address, state, city=""):
        """
            Get lat/lon of a location from the database cache.

            Returns (None, None) if the lat/lon is not found.
        """
        # Make the location easier to geocode
        address = SSGeo.modifyaddr(address)

        coll_cache = self.get_coll_geo_cache()

        # Check if the lat/lon is in the database
        if city != "":
            loc_cursor = coll_cache.find(
                {
                    "state": state,
                    "city": city,
                    "address": address
                })
        else:
            loc_cursor = coll_cache.find(
                {
                    "state": state,
                    "city": {"$exists": False},
                    "address": address
                })

        # If the location exists in the database
        if loc_cursor.count() != 0:
            if loc_cursor.count() > 1:
                # TODO Print proper warning using python warnings
                print("WARNING: Location has more than one cached lat/lon. " +
                      "Using first one received")
            loc = loc_cursor.next()
            return (loc['latitude'], loc['longitude'])
        else:
            return (None, None)

    def cache_location(self, lat, lon, address, state, city=""):
        """
            Cache location in the the database's `geocode_cache`
            collection.
        """
        coll_cache = self.get_coll_geo_cache()
        if city != "":
            coll_cache.update_one(
                {
                    "state": state,
                    "city": city,
                    "address": address
                },
                {
                    "$set":
                    {
                        "state": state,
                        "city": city,
                        "address": address,
                        "latitude": lat,
                        "longitude": lon
                    }
                },
                True
            )
        else:
            coll_cache.update_one(
                {
                    "state": state,
                    "address": address
                },
                {
                    "$set":
                    {
                        "state": state,
                        "address": address,
                        "latitude": lat,
                        "longitude": lon
                    }
                },
                True
            )

    def get_coll_geo_cache(self):
        """
            Return a Mongo connection to the `geocode_cache`
            collection.
        """
        cfg = ConfigParser()
        cfg.read(self.cfg_db)
        db_streetsavvy_name = cfg['Databases']['streetsavvy']
        coll_geo_cache_name = cfg['Collections']['geocode_cache']
        db_streetsavvy = MongoClient()[db_streetsavvy_name]
        db_streetsavvy.authenticate(cfg['Accounts']['user_scraper'],
                                    cfg['Accounts']['pw_scraper'])
        return db_streetsavvy[coll_geo_cache_name]

    def get_coll_geo_exc(self):
        """
            Return a Mongo connection to the `geo_exc` collection.
        """
        cfg = ConfigParser()
        cfg.read(self.cfg_db)
        db_streetsavvy_name = cfg['Databases']['streetsavvy']
        coll_geo_exc_name = cfg['Collections']['geo_exc']
        db_streetsavvy = MongoClient()[db_streetsavvy_name]
        db_streetsavvy.authenticate(cfg['Accounts']['user_scraper'],
                                    cfg['Accounts']['pw_scraper'])
        return db_streetsavvy[coll_geo_exc_name]

    @staticmethod
    def modifyaddr(address):
        """
            Modify the address so that it is easier to geocode
        """
        return address.replace("/", " and ")

    def addgeoexc(self, address, state, city=""):
        """
            Helper function to add the given location to the geocode
            exception collection
        """
        coll_geo_exc = self.get_coll_geo_exc()
        if city != "":
            coll_geo_exc.update_one(
                {
                    "state": state,
                    "city": city,
                    "address": address
                },
                {
                    "$set":
                    {
                        "state": state,
                        "city": city,
                        "address": address
                    }
                },
                True
            )
        else:
            coll_geo_exc.update_one(
                {
                    "state": state,
                    "address": address
                },
                {
                    "$set":
                    {
                        "state": state,
                        "address": address
                    }
                },
                True
            )

    def isingeoexc(self, address, state, city=""):
        """
            Check if the given location is in the geocode exception
            collection
        """
        coll_geo_exc = self.get_coll_geo_exc()
        if city != "":
            doc = coll_geo_exc.find_one({"state": state, "city": city,
                                         "address": address})
        else:
            doc = coll_geo_exc.find_one({"state": state, "address": address})
        if doc is None: # If we didn't find the it in geo_exc
            return False
        else:
            return True

    def delgeoexc(self, address, state, city=""):
        """
            Helper function to delete a doc in the geo_exc collection
            with the specified location
        """
        coll_geo_exc = self.get_coll_geo_exc()
        if city != "":
            coll_geo_exc.delete_one({"state": state, "city": city,
                                     "address": address})
        else:
            coll_geo_exc.delete_one({"state": state, "address": address})
