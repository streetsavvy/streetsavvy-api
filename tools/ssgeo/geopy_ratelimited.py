"""Provides GeopyRateLimited, a ratelimited wrapper around geopy."""

# Standard Libs
from configparser import ConfigParser
from datetime import datetime
import sys
import os

# 3rd Party Libs
from pymongo import MongoClient
from geopy import GoogleV3, Bing

class GeopyRateLimited:
    """
        Wrapper class around `Geopy` to make sure Geocode API rates are
        not exceeded. This class uses several Geocoding APIs in order
        to geocode more locations while staying under API rates.
    """

    def __init__(self, cfg_db="db.ini"):
        if not os.path.isfile(cfg_db):
            raise ValueError("cfg_db file '" + cfg_db + "' does not exist")
        self.cfg_db = cfg_db

    def geocode(self, query):
        """
            Geocode the given query. The geocoder that is chosen is
            determined by the `get_geocoder` method.

            Returns the result of `geocoder.geocode(query)`.

            Raises ValueError If a geocoder is not found.
        """
        (geocoder_name, geocoder) = self.get_geocoder()
        location = geocoder.geocode(query)

        # Increase calls today count to make sure we stay under the
        # limits
        coll_geo_api_rates = self.get_coll_geo_api_rates()
        start_date = datetime.today().replace(hour=0, minute=0,
                                              second=0, microsecond=0)
        end_date = datetime.today().replace(hour=23, minute=59,
                                            second=59, microsecond=999999)
        result = coll_geo_api_rates.update_one(
            {'geocoder': geocoder_name,
             'date': {'$gte': start_date, '$lte': end_date}},
            {'$inc': {'callstoday': 1}})

        if result.modified_count != 1:
            raise RuntimeError("The calls today count for geocoder '" +
                               geocoder_name + "' could not be " +
                               "incremented in the database")

        return location

    def get_geocoder(self):
        """
            Get a geocoder that has been used the least today and
            is under its rate.

            Returns a tuple of the `geocoder_name` and its
            associated geocoder instance.

            Raises ValueError is a geocoder is unable to be found.
        """
        coll_geo_api_rates = self.get_coll_geo_api_rates()

        # Get names of all available geocoders and store them
        geocoders = set(coll_geo_api_rates.distinct('geocoder'))

        # Get all geocoders used today
        geo_today_docs = self.get_geo_used_today()

        # Get geocoders unused today
        geo_used_today = set()
        for doc in geo_today_docs:
            geo_used_today.add(doc['geocoder'])
        geo_unused_today = geocoders - geo_used_today

        # For the geocoders that haven't been used today, add a
        # document for it
        for geo in geo_unused_today:
            doc = coll_geo_api_rates.find_one({'geocoder': geo})
            doc['callstoday'] = 0
            doc['date'] = datetime.today()
            del doc['_id']
            coll_geo_api_rates.insert(doc)

        # Choose a geocoder
        geo_today_docs = self.get_geo_used_today()
        geocoder_name = None
        geocoder = None
        min_calls = sys.maxsize
        for doc in geo_today_docs:
            # If the number of calls today is less than the daily limit
            # and if the number of calls
            if (doc['callstoday'] < doc['callsperday']
                    and doc['callstoday'] < min_calls):
                geocoder_name = doc['geocoder']
                min_calls = doc['callstoday']

        if geocoder_name is not None:
            geocoder = self.get_geocoder_by_name(geocoder_name)
        else:
            raise ValueError("All geocoders have exceeded their limits or " +
                             "no geocoders are in the geo_api_rates collection")

        return (geocoder_name, geocoder)

    def get_geocoder_by_name(self, geocoder):
        """
            Return the specified geocoder given its string
            representation.
        """
        cfg = ConfigParser()
        cfg.read(self.cfg_db)
        if geocoder == 'GoogleV3':
            apikey = cfg['APIKeys']['GoogleV3']
            return GoogleV3(apikey)
        elif geocoder == 'Bing':
            apikey = cfg['APIKeys']['Bing']
            return Bing(apikey)

    def get_geo_used_today(self):
        """
            Return a cursor to the docs of geocoders that have been
            used today inside the `geo_api_rates` collection.
        """
        coll_geo_api_rates = self.get_coll_geo_api_rates()

        start_date = datetime.today().replace(hour=0, minute=0,
                                              second=0, microsecond=0)
        end_date = datetime.today().replace(hour=23, minute=59,
                                            second=59, microsecond=999999)
        return coll_geo_api_rates.find({"date": {'$gte': start_date,
                                                 '$lte': end_date}})

    def get_coll_geo_api_rates(self):
        """
            Return a Mongo connection to the `geo_api_rates`
            collection.
        """
        cfg = ConfigParser()
        cfg.read(self.cfg_db)
        db_streetsavvy_name = cfg['Databases']['streetsavvy']
        coll_geo_api_rates_name = cfg['Collections']['geo_api_rates']
        db_streetsavvy = MongoClient()[db_streetsavvy_name]
        db_streetsavvy.authenticate(cfg['Accounts']['user_scraper'],
                                    cfg['Accounts']['pw_scraper'])
        return db_streetsavvy[coll_geo_api_rates_name]
