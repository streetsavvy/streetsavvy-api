"""
   :platform: Unix
   :synopsis:  Tests algorithms agaisnt the StreetSavvy cache 

.. moduleauthor:: Lauren Slason <lauren.slason@gmail.com>


"""

import re
import sys
import argparse
import itertools
import configparser
import Levenshtein
from fuzzywuzzy import fuzz
from collections import defaultdict
from pymongo import MongoClient

sys.path.append('../../')

from tools.mapper.mapping.organize import (
    hash_weights,
    hash_severity,
    hash_codes,
    hash_phrases
)
from tools.mapper.mapping.decipher import (
    map_raw_codes,
    map_raw_phrases,
    map_codes,
    map_phrases
)
from tools.mapper.mapping.normalize import (
    remove_special,
    minimize_spaces,
    trim_codes
)

def create_test_entry(desc, desc_keys, result, keys, weights, doc, method):
    if not result:
        result = 'n/a'
        weight = None
    else:
        weight = weights[result]
    if result == doc['type']:
        correct = True
    else:
        correct = False
    return {"description":desc, "desc_keys":desc_keys, "type":result,
            "success":correct, "matches":keys, "weight":weight,
            "method":method, "original_type":doc['type']}


if __name__ == '__main__':
    # Set configuration
    parser = argparse.ArgumentParser(
        description='Provide configuration details for the API')
    parser.add_argument('-c', type=str, dest='config', default='testing.cfg')
    parser.add_argument('-n', type=str, dest='test_name', required=True)
    parser.add_argument('-r', type=int, dest='ratio', default=80)
    parser.add_argument('--diff', action='store_true', dest='diff')
    args = parser.parse_args()
    
    # Parse the configuration
    config = configparser.ConfigParser()
    config.read(args.config)
    
    # Open connection with the MongoDB database
    client = MongoClient(config['mongo']['host'], int(config['mongo']['port']))
    db = client[config['mongo']['source']]
    db.authenticate(name=config['mongo']['username'], 
        password=config['mongo']['password'], source=config['mongo']['source'])

    # Hash all of the weights
    c_weights = db[config['mongo']['weight_coll']]
    weights = hash_weights(c_weights)

    # Hash all of the severities
    c_severity = db[config['mongo']['severity_coll']]
    severities = hash_severity(c_severity)

    # Identify all the hashed codes
    c_keys = db[config['mongo']['keys_coll']]
    codes = hash_codes(c_keys)
    # Find all the phrases organized by number of spaces
    phrases = hash_phrases(c_keys)

    c_cache = db[config['mongo']['mapping_coll']]
    cache_cursor = c_cache.find({}, {"_id":0, "description":1, "type":1})
    results = list()
    method = args.test_name
    for doc in cache_cursor:
        desc = doc['description']
        trimmed = remove_special(desc)
        minimized = minimize_spaces(trimmed)
        desc_keys = minimized.split(' ')
        desc_keys = desc_keys + trim_codes(desc_keys)
        result, keys = map_raw_codes(desc_keys, codes, severities)
        if result:
            new_entry = create_test_entry(desc, desc_keys, result, keys, weights, doc, method)
            results.append(new_entry)
            continue
        result, keys = map_raw_phrases(desc_keys, phrases, severities)
        if result:
            new_entry = create_test_entry(desc, desc_keys, result, keys, weights, doc, method)
            results.append(new_entry)
            continue
        result, keys = map_codes(desc_keys, codes, severities, args.ratio)
        if result:
            new_entry = create_test_entry(desc, desc_keys, result, keys, weights, doc, method)
            results.append(new_entry)
            continue
        result, keys = map_phrases(desc_keys, phrases, severities, args.ratio)
        if result:
            new_entry = create_test_entry(desc, desc_keys, result, keys, weights, doc, method)
            results.append(new_entry)
            continue
        new_entry = create_test_entry(desc, desc_keys, result, keys, weights, doc, method)
        results.append(new_entry)
    count = 0
    if args.diff:
        for r in results:
            if not r['success']:
                count = count + 1
                print('\t'.join([r['description'], r['type'], r['original_type'], '_'.join(r['matches'])]))
        # print(count)
    else:
        c = db['mapping_tests']
        c.insert_many(results)

