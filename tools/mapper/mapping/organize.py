"""
   :platform: Unix
   :synopsis:  Hashes and organizes keys

.. moduleauthor:: Lauren Slason <lauren.slason@gmail.com>


"""

import re
import sys
import argparse
import itertools
import configparser
import Levenshtein
from fuzzywuzzy import fuzz
from collections import defaultdict
from pymongo import MongoClient


def hash_weights(c_weights):
    # Hash all of the weights
    all_weights = c_weights.find({})
    weights = dict()
    for entry in all_weights:
        weights[entry['type']] = entry['weight']
    return weights

def hash_severity(c_severity):
    all_severity = c_severity.find({})
    severities = dict()
    for entry in all_severity:
        severities[entry['type']] = entry['severity']
    return severities

def hash_codes(c_keys):
    codes = defaultdict(list)
    code_keys = c_keys.find({"code":True})
    for entry in code_keys:
        codes[entry['type']].append(entry['key'])
    return codes

def hash_cache(c_cache):
    cache = dict()
    cached_descr = c_cache.find({})
    for entry in cached_descr:
        cache[entry['description']] = entry['type']
    return cache 

def hash_phrases(c_keys):
    phrases = dict()
    max_spaces = c_keys.find({"code":False}, {"_id":0, "spaces":1}).sort([("spaces", -1)]).limit(1)
    max_spaces = int(list(max_spaces)[0]['spaces'])
    for i in range(0, max_spaces+1):
        phrase_dict = defaultdict(list)
        phrase_cursor = c_keys.find({"code":False, 'spaces':str(i) })
        for phrase in phrase_cursor:
            phrase_dict[phrase['type']].append(phrase['key'].lower())
        phrases[i] = phrase_dict
    return phrases

def create_count_grid(hashed_values):
    counts = dict()
    for crime in hashed_values.keys():
        counts[crime] = list()
    return counts

def tally_count_grid(count_grid, severity):
    max_count = 0
    crime_type = None
    for crime in count_grid.keys():
        current = 0
        for phrase in count_grid[crime]:
            current = current + phrase.count(' ') + 1
        # current = len(count_grid[crime])
        if current > 0 and current >= max_count:
            if current == max_count:
                if severity[crime] < severity[crime_type]:
                    crime_type = crime
            else:
                max_count = current
                crime_type = crime
    return crime_type



