"""
   :platform: Unix
   :synopsis:  Deciphers crime descriptions to identify crime types

.. moduleauthor:: Lauren Slason <lauren.slason@gmail.com>


"""

import re
import sys
import argparse
import itertools
import configparser
import Levenshtein
from fuzzywuzzy import fuzz
from collections import defaultdict
from pymongo import MongoClient

from tools.mapper.mapping.organize import (
    create_count_grid,
    tally_count_grid
)


def map_raw_codes(desc_keys, codes, severity):
    # First consider San Francsisco Police Deparment Codes
    counts = create_count_grid(severity)
    for crime in codes.keys():
        for code in codes[crime]:
            if code in desc_keys:
            # if code in desc_keys or any(x.startswith(code) for x in desc_keys):
                counts[crime].append(code)
    crime_type = tally_count_grid(counts, severity)
    if crime_type:
        return crime_type, counts[crime_type]
    return None, []

def map_raw_phrases(desc_keys, phrases, severity):
    # First consider San Francsisco Police Deparment Codes
    counts = create_count_grid(severity)
    for i in reversed(range(0, max(phrases.keys()) + 1)):
        for crime in phrases[i]:
            for phrase in phrases[i][crime]:
                # print(phrase, i, desc_keys, list(itertools.permutations(desc_keys, i+1)))
                if any(phrase == ' '.join(x) for x in \
                    itertools.permutations(desc_keys, i+1)):
                    counts[crime].append(phrase)
    crime_type = tally_count_grid(counts, severity)
    if crime_type:
        return crime_type, counts[crime_type]
    return None, []

def map_phrases(desc_keys, phrases, severity, ratio=80):
    # First consider San Francsisco Police Deparment Codes
    counts = create_count_grid(severity)
    for i in reversed(range(0, max(phrases.keys()) + 1)):
        for crime in phrases[i]:
            for phrase in phrases[i][crime]:
                if any(fuzz.ratio(phrase, ' '.join(x)) > ratio for x in \
                    itertools.permutations(desc_keys, i+1)):
                    counts[crime].append(phrase)

    crime_type = tally_count_grid(counts, severity)
    if crime_type:
        return crime_type, counts[crime_type]
    return None, []

def map_codes(desc_keys, codes, severity, ratio=80):
    # First consider San Francsisco Police Deparment Codes
    counts = create_count_grid(severity)
    for crime in codes.keys():
        for code in codes[crime]:
            if any(fuzz.ratio(x, code) > ratio for x in desc_keys):
                counts[crime].append(code)
    crime_type = tally_count_grid(counts, severity)
    if crime_type:
        return crime_type, counts[crime_type]
    return None, []

