"""
   :platform: Unix
   :synopsis:  Normalizes crime descriptions

.. moduleauthor:: Lauren Slason <lauren.slason@gmail.com>


"""

import re

def remove_special(description):
    no_special = re.sub('[^A-Za-z0-9]+', ' ', description)
    return no_special.lower()


def minimize_spaces(description):
    min_spaces = re.sub('\s+', ' ', description)
    return min_spaces.strip()

def trim_codes(description_keys):
    trimmed = []
    for key in description_keys:
        if bool(re.search(r'\d', key)) and bool(re.search(r'\D', key)):
            trimmed.append(re.sub("\D", "", key))
            trimmed.append(re.sub("\d", "", key))
    return trimmed
