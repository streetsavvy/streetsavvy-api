"""
   :platform: Unix
   :synopsis:  TBD 

.. moduleauthor:: Lauren Slason <lauren.slason@gmail.com>


"""

import re
import sys
import argparse
import itertools
import configparser
import Levenshtein
from fuzzywuzzy import fuzz
from collections import defaultdict
from pymongo import MongoClient

from mapping.organize import (
    hash_weights,
    hash_severity,
    hash_codes,
    hash_phrases
)
from mapping.decipher import (
    map_raw_codes,
    map_raw_phrases,
    map_codes,
    map_phrases
)
from mapping.normalize import (
    remove_special,
    minimize_spaces
)

if __name__ == '__main__':
    # Set configuration
    parser = argparse.ArgumentParser(
        description='Provide configuration details for the API')
    parser.add_argument('-c', type=str, dest='config', default='mapping.cfg')
    args = parser.parse_args()
    
    # Parse the configuration
    config = configparser.ConfigParser()
    config.read(args.config)
    
    # Open connection with the MongoDB database
    client = MongoClient(config['mongo']['host'], int(config['mongo']['port']))
    db = client[config['mongo']['source']]
    db.authenticate(name=config['mongo']['username'], 
        password=config['mongo']['password'], source=config['mongo']['source'])
    db_police = client[config['mongo']['source_service']]
    db_police.authenticate(name=config['mongo']['username_service'],
        password=config['mongo']['password_service'], source=config['mongo']['source_service'])

    # Hash all of the weights
    c_weights = db[config['mongo']['weight_coll']]
    all_weights = c_weights.find({})
    weights = dict()
    for entry in all_weights:
        weights[entry['type'].lower()] = entry['weight']
    weights['n/a'] = 0

    c_cache = db[config['mongo']['mapping_coll']]
    cache_cursor = c_cache.find({}, {"_id":0, "description":1, "type":1})
    c_police = db_police[config['mongo']['police_coll']]
    for doc in cache_cursor:
        if c_police.find({"original_crimetype_name":doc["description"]}).count() == 0:
            continue
        c_police.update({"original_crimetype_name":doc["description"]},
            { "$set" : { "crimetype": doc["type"] } }, upsert=True, multi=True)
        print(doc['description'], c_police.find({"original_crimetype_name":doc["description"]}).count())
        c_police.update({"original_crimetype_name":doc["description"]},
            { "$set" : { "weight": weights[doc["type"].lower()] } }, upsert=True, multi=True)


