"""
   :platform: Unix
   :synopsis:  Tests algorithms agaisnt the StreetSavvy cache 

.. moduleauthor:: Lauren Slason <lauren.slason@gmail.com>


"""

import re
import sys
import argparse
import itertools
import configparser
from collections import defaultdict
from pymongo import MongoClient


if __name__ == '__main__':
    # Set configuration
    parser = argparse.ArgumentParser(
        description='Provide configuration details for the API')
    parser.add_argument('-c', type=str, dest='config', default='testing.cfg')
    parser.add_argument('-t', type=str, dest='test_name', default='fuzzy_with_phrases')
    # parser.add_argument('--diff', action='store_true', dest='diff')
    args = parser.parse_args()
    
    # Parse the configuration
    config = configparser.ConfigParser()
    config.read(args.config)
    
    # Open connection with the MongoDB database
    client = MongoClient(config['mongo']['host'], int(config['mongo']['port']))
    db = client[config['mongo']['source']]
    db.authenticate(name=config['mongo']['username'], 
        password=config['mongo']['password'], source=config['mongo']['source'])

    c_cache = db[config['mongo']['test_coll']]
    cache_cursor = c_cache.find({"success":False, "method":args.test_name},
        {"_id":0, "description":1, "type":1, "original_type":1, "matches":1, "desc_keys":1})
    for r in cache_cursor:
        print('\t'.join([r['description'], r['type'], r['original_type'], '::'.join(r['matches']), '::'.join(r['desc_keys'])]))
