"""
   :platform: Unix
   :synopsis: Inserts or updates crime weights in mongo db

.. moduleauthor:: Lauren Slason <lauren.slason@gmail.com>


"""

import argparse
import configparser
from pymongo import MongoClient

if __name__ == '__main__':
    # Set configuration
    parser = argparse.ArgumentParser(
        description='Provide configuration details for the API')
    parser.add_argument('-c', type=str, dest='config', default='mapping.cfg')
    parser.add_argument('-f', type=str, dest='file', required=True)
    parser.add_argument('--heading', dest='heading', action='store_true')
    args = parser.parse_args()
    # Parse the configuration
    config = configparser.ConfigParser()
    config.read(args.config)
    docs = []
    with open(args.file, 'r') as f:
        if args.heading:
            next(f)
        for line in f:
            fields = line.strip().split('\t')
            document = {'type':fields[0].lower().strip('"'), 'weight':int(fields[1])}
            docs.append(document)
            # mappings[fields[0]] = mappings[fields[1]]
    # Insert documents into the MongoDB database
    client = MongoClient(config['mongo']['host'], int(config['mongo']['port']))
    db = client[config['mongo']['source']]
    db.authenticate(name=config['mongo']['username'], 
        password=config['mongo']['password'], source=config['mongo']['source'])
    collection = db[config['mongo']['weight_coll']]
    collection.insert_many(docs)
    
