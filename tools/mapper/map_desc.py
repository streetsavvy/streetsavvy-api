"""
   :platform: Unix
   :synopsis:  TBD

.. moduleauthor:: Lauren Slason <lauren.slason@gmail.com>


"""

import re
import sys
import logging
import argparse
import smtplib
import itertools
import datetime
import configparser
import Levenshtein
from fuzzywuzzy import fuzz
from collections import defaultdict
from pymongo import MongoClient

from tools.mapper.mapping.organize import (
    hash_weights,
    hash_severity,
    hash_codes,
    hash_phrases,
    hash_cache
)
from tools.mapper.mapping.decipher import (
    map_raw_codes,
    map_raw_phrases,
    map_codes,
    map_phrases
)
from tools.mapper.mapping.normalize import (
    remove_special,
    minimize_spaces,
    trim_codes
)

def main():
    # Set configuration
    parser = argparse.ArgumentParser(
        description='Provide configuration details for the API')
    parser.add_argument('-c', type=str, dest='config', default='mapping.cfg')
    parser.add_argument('-l', type=str, dest='log', default='map_desc')
    args = parser.parse_args()

    # Parse the configuration
    config = configparser.ConfigParser()
    config.read(args.config)

    # Initialize logging
    log_date = datetime.datetime.now()
    log_date = log_date.strftime("%m.%d.%Y_%H.%M.%S")
    log_file = '%s_%s.log' % (args.log, log_date)
    logging.basicConfig(filename=log_file,level=logging.DEBUG)

    # Open connection with the MongoDB database
    client = MongoClient(config['mongo']['host'], int(config['mongo']['port']))
    db = client[config['mongo']['source']]
    db.authenticate(name=config['mongo']['username'],
        password=config['mongo']['password'], source=config['mongo']['source'])
    db_police = client[config['mongo']['source_service']]
    db_police.authenticate(name=config['mongo']['username_service'],
        password=config['mongo']['password_service'], source=config['mongo']['source_service'])

    # Hash the cached descriptions
    c_cache = db[config['mongo']['mapping_coll']]
    cache = hash_cache(c_cache)

    # Hash all of the weights
    c_weights = db[config['mongo']['weight_coll']]
    weights = hash_weights(c_weights)
    weights['n/a'] = 0

    # Hash all of the severities
    c_severity = db[config['mongo']['severity_coll']]
    severities = hash_severity(c_severity)

    # Identify all the hashed codes
    c_keys = db[config['mongo']['keys_coll']]
    codes = hash_codes(c_keys)
    # Find all the phrases organized by number of spaces
    phrases = hash_phrases(c_keys)

    calls = set()
    c_police = db_police[config['mongo']['police_coll']]
    # police_cursor = c_police.find({})
    police_cursor = c_police.find({"crimetype":{"$exists":False}})
    for doc in police_cursor:
        calls.add(doc['original_crimetype_name'])

    results = list()
    for desc in calls:
        if desc in cache.keys():
            logging.info("DESCRIPTION=%s, TYPE=%s, MSG=%s", desc, cache[desc], 'Found in cache')
            c_police.update({"original_crimetype_name":desc},
                { "$set" : { "crimetype": cache[desc]} }, upsert=False, multi=True)
            c_police.update({"original_crimetype_name":desc},
                { "$set" : { "weight": weights[cache[desc]] } }, upsert=False, multi=True)
            continue
        trimmed = remove_special(desc)
        minimized = minimize_spaces(trimmed)
        desc_keys = minimized.split(' ')
        desc_keys = trim_codes(desc_keys) + desc_keys
        result, keys = map_raw_codes(desc_keys, codes, severities)
        if result:
            logging.info("DESCRIPTION=%s, TYPE=%s, KEYS=%s, MSG=%s",
                desc, result, str(keys), 'Found raw SFPD')
            results.append({"description":desc, "type":result})
            continue
        result, keys = map_raw_phrases(desc_keys, phrases, severities)
        if result:
            logging.info("DESCRIPTION=%s, TYPE=%s, KEYS=%s, MSG=%s",
                desc, result, str(keys), 'Found phrase')
            results.append({"description":desc, "type":result})
            continue
        result, keys = map_codes(desc_keys, codes, severities)
        if result:
            logging.info("DESCRIPTION=%s, TYPE=%s, KEYS=%s, MSG=%s",
                desc, result, str(keys), 'Found code using fuzzy matching')
            results.append({"description":desc, "type":result})
            continue
        result, keys = map_phrases(desc_keys, phrases, severities)
        if result:
            logging.info("DESCRIPTION=%s, TYPE=%s, KEYS=%s, MSG=%s",
                desc, result, str(keys), 'Found phrase using fuzzy matching')
            results.append({"description":desc, "type":result})
            continue
        logging.info("DESCRIPTION=%s, TYPE=%s, KEYS=%s, MSG=%s",
                desc, 'n/a', str([]), 'Found Nothing')
        results.append({"description":desc, "type":'n/a'})
    send_to = config['gmail']['to'].split(',')
    send_from = config['gmail']['from']
    contents = ''
    if results:
        for r in results:
             contents = contents + '%s:\t%s\n' % (r['description'], r['type'])
        c_cache.insert_many(results)
        logging.info('MSG=Inserted New Documents into Mongo, COUNT=%s', len(results))
    else:
        contents = 'No new crime types discovered.'
    message = """From: %s\nTo: %s\nSubject: %s\n\n%s""" % (
            send_from, send_to, 'Street Savvy Daily Report', contents)
    server = smtplib.SMTP("smtp.gmail.com", 587)
    server.ehlo()
    server.starttls()
    server.login(config['gmail']['user'], config['gmail']['password'])
    server.sendmail(send_from, send_to, message)
    server.close()
    for r in results:
         c_police.update({"original_crimetype_name": r['description']},
              { "$set" : { "crimetype": r['type']} }, upsert=True, multi=True)
         c_police.update({"original_crimetype_name":r["description"]},
             { "$set" : { "weight": weights[r['type']] } }, upsert=True, multi=True)
    logging.info("MSG=Updated police collection")

if __name__ == '__main__':
    main()
