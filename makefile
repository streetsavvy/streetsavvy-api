.RECIPEPREFIX +=

init:
    pip install -r requirements.txt
    pip install -e .

pylint:
    pylint -f colorized *.py tools/*/*.py tools/*/tests/*.py

tests:
    pytest -m "not stress" --ignore=tools/process_osm/tests/test_process_osm.py --ignore=tools/test_bed/test_bed.py --ignore=tools/heatmap/tests/test_render_heatmap.py
