# Change Log
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/).

## [0.0.0] - 2016-10-22
### Added
 - Added a get check to verify API is accessible
 - Added a get method to return all the service calls within a specified date range
