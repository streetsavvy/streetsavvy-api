"""
   :platform: Unix
   :synopsis: Perform queries against the local database

.. moduleauthor:: Lauren Slason <lauren.slason@gmail.com>


"""

from pymongo import MongoClient

class MongoConnection(object):
    """Class to manage connections and queries to mongo."""

    def __init__(self, host='localhost', port=27017):
        """Initalizes a mongo db client
        
        :param host: The host mongo resides on
        :type host: str
        :param port: The open mongo port
        :type port: int"""
         
        self._host = host
        self._port = port
        self._db = None
        self._collection = None
        self._username = None
        self._password = None
        self._source = None
        self.client = MongoClient(host, port)

    def provide_credentials(self, username, password, source):
        """Authenticate within the mongo client

        :param username: The username to log in as
        :type username: str
        :param password: The password to authenticate with
        :type password: str
        :param source: the database the user resides in
        :type source: str
        """
        self._username = username
        self._password = password
        self._source = source
        self._db.authenticate(name=self._username, password=self._password, source=self._source)

    def set_database(self, value):
        """Set the database
        :param value: The name of the collection
        :param type: str
        """
        self._db = self.client[value]

    def set_collection(self, value):
        """Set the collection
        :param value: The name of the collection
        :param type: str
        """
        self._collection = self._db[value]

    def query_time_range(self, f, start, stop):
        """Return all the documents within a specified time range
        :param field: Name of the timestamp field to query on
        :type field: str
        :param start: Start of the time range (greater than or equal to)
        :type start: datetime.datetime
        :param stop: Stop of the time range (less than or equal to)
        :type stop: datetime.datetime
        """
        query = {f:{"$gte":start, "$lte":stop}}
        return list(self._collection.find(query, {"_id":0}))

    def query_key(self, key, value):
        """Return all the documents with a specified key value pair
        :param key: Name of the field to query on
        :type key:  str
        :param value: Value the field should match
        :type value: (str, int, bool)
        """
        return list(self._collection.find({key:value}, {"_id":0}))

    def query_greater_than(self, key, value, exclude):
        """Return all the documents with a specified key value pair greater than value
        :param key: Name of the field to query on
        :type key:  str
        :param value: Value the field should match
        :type value: (str, int, bool)
        """
        return list(self._collection.find({key:{"$gt": value}}, exclude))

    def query_all(self):
        """Return all of the documents within a collection."""
        return list(self._collection.find({}, {"_id":0}))

    def query_distinct(self, key):
        """Return all the values with a specified key
        :param key: Name of the field to query on
        :type key:  str
        """
        return self._collection.distinct(key)
