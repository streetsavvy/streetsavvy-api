"""
   :platform: Unix
   :synopsis: Entry point for Street Savvy web api
.. moduleauthor:: Lauren Slason <lauren.slason@gmail.com>


"""

import os
import argparse
import configparser
from datetime import datetime, timedelta
from flask import Flask, jsonify, Blueprint, request, send_from_directory
from flask_autodoc import Autodoc
from geopy.exc import GeocoderTimedOut
from api_libs.mongo_connect import MongoConnection
from tools.routing.routing import read_graph, get_path
from tools.ssgeo.ssgeo import SSGeo


# Set configuration
PARSER = argparse.ArgumentParser(
    description='Provide configuration details for the API')
PARSER.add_argument('-c', type=str, dest='config', default='run.cfg')
ARGS = PARSER.parse_args()
CFG_PARSER = configparser.ConfigParser()
CFG_PARSER.read(ARGS.config)

# Create blueprint with appropriate configuration
API = Blueprint('api', __name__, template_folder='templates')
API.config = CFG_PARSER._sections

AUTO = Autodoc()
# Graph 
# GRAPH = None
# LAST_LOAD = datetime.now()
# CRIME_GRAPH = None
# LAST_LOAD_CRIME = datetime.now()

@API.route("/check", methods=['GET'])
@AUTO.doc()
def default():
    """Return true if the API is available.
    Example: /api/check"""
    return jsonify([{"Running":True}])


@API.route("/police", methods=['GET'])
@AUTO.doc()
def get_police_range():
    """Return JSON representing all the police calls within time range.
    Args:  start (date str, %d:%m:%Y, required), stop (date str, %d:%m:%Y, required)
    Example: /api/police?start=20:10:2016&stop=21:10:2016 """
    start = request.args.get('start')
    stop = request.args.get('stop')
    mongo_conn = MongoConnection()
    mongo_conn.set_database(API.config['mongo']['calls_db'])
    mongo_conn.provide_credentials(
        API.config['mongo']['username'],
        API.config['mongo']['password'],
        API.config['mongo']['source'])
    mongo_conn.set_collection(API.config['mongo']['police'])
    stime = datetime.strptime(start, '%d:%m:%Y')
    etime = datetime.strptime(stop, '%d:%m:%Y')
    results = mongo_conn.query_time_range('report_date', stime, etime)
    return jsonify(results)


@API.route('/fire', methods=['GET'])
@AUTO.doc()
def get_fire_range():
    """Return JSON representing all the fire calls within a range.
    Args:  start (date str, %d:%m:%Y, required), stop (date str, %d:%m:%Y, required)
    Example: /api/fire?start=20:10:2016&stop=21:10:2016 """
    start = request.args.get('start')
    stop = request.args.get('stop')
    mongo_conn = MongoConnection()
    mongo_conn.set_database(API.config['mongo']['calls_db'])
    mongo_conn.provide_credentials(
        API.config['mongo']['username'],
        API.config['mongo']['password'],
        API.config['mongo']['source'])
    mongo_conn.set_collection(API.config['mongo']['fire'])
    stime = datetime.strptime(start, '%d:%m:%Y')
    etime = datetime.strptime(stop, '%d:%m:%Y')
    results = mongo_conn.query_time_range('received_dttm', stime, etime)
    return jsonify(results)


@API.route('/crime_tests', methods=['GET'])
@AUTO.doc()
def get_crime_test():
    """Return JSON representing all the results of validation test(s).
    Args:  method (str, optional)
    Examples:  /api/crime_tests?method=roc0"""
    method = request.args.get('method')
    mongo_conn = MongoConnection()
    mongo_conn.set_database(API.config['mongo']['mapping_db'])
    mongo_conn.provide_credentials(
        API.config['mongo']['username'],
        API.config['mongo']['password'],
        API.config['mongo']['source'])
    mongo_conn.set_collection(API.config['mongo']['crime_tests'])
    if method:
        results = mongo_conn.query_key('method', method)
    else:
        results = mongo_conn.query_all()
    return jsonify(results)

@API.route('/crime_methods', methods=['GET'])
@AUTO.doc()
def get_crime_methods():
    """Return JSON representing all the different crime test methods.
    Examples: /api/crime_methods"""
    mongo_conn = MongoConnection()
    mongo_conn.set_database(API.config['mongo']['mapping_db'])
    mongo_conn.provide_credentials(
        API.config['mongo']['username'],
        API.config['mongo']['password'],
        API.config['mongo']['source'])
    mongo_conn.set_collection(API.config['mongo']['crime_tests'])
    results = mongo_conn.query_distinct('method')
    return jsonify(results)

@API.route('/mapping', methods=['GET'])
@AUTO.doc()
def get_crime_mapping():
    """Return JSON representing all the crime mappings
    Examples: /api/mapping"""
    mongo_conn = MongoConnection()
    mongo_conn.set_database(API.config['mongo']['mapping_db'])
    mongo_conn.provide_credentials(
        API.config['mongo']['username'],
        API.config['mongo']['password'],
        API.config['mongo']['source'])
    mongo_conn.set_collection(API.config['mongo']['crime_type'])
    results = mongo_conn.query_all()
    return jsonify(results)

@API.route('/weights', methods=['GET'])
@AUTO.doc()
def get_crime_weights():
    """Return JSON representing all the crime weights
    Examples: /api/weights"""
    mongo_conn = MongoConnection()
    mongo_conn.set_database(API.config['mongo']['mapping_db'])
    mongo_conn.provide_credentials(
        API.config['mongo']['username'],
        API.config['mongo']['password'],
        API.config['mongo']['source'])
    mongo_conn.set_collection(API.config['mongo']['crime_weights'])
    results = mongo_conn.query_all()
    return jsonify(results)


@API.route('/severity', methods=['GET'])
@AUTO.doc()
def get_crime_severity():
    """Returns JSON representing all the crime severities
    Examples: /api/severity"""
    mongo_conn = MongoConnection()
    mongo_conn.set_database(API.config['mongo']['mapping_db'])
    mongo_conn.provide_credentials(
        API.config['mongo']['username'],
        API.config['mongo']['password'],
        API.config['mongo']['source'])
    mongo_conn.set_collection(API.config['mongo']['crime_severity'])
    results = mongo_conn.query_all()
    return jsonify(results)

@API.route('/crime_keys', methods=['GET'])
@AUTO.doc()
def get_crime_keys():
    """Return JSON representing all the crime keys
    Examples: /api/crime_keys"""
    mongo_conn = MongoConnection()
    mongo_conn.set_database(API.config['mongo']['mapping_db'])
    mongo_conn.provide_credentials(
        API.config['mongo']['username'],
        API.config['mongo']['password'],
        API.config['mongo']['source'])
    mongo_conn.set_collection(API.config['mongo']['crime_keys'])
    results = mongo_conn.query_all()
    return jsonify(results)

@API.route('/heatmap', methods=['GET'])
@AUTO.doc()
def get_heatmap():
    """Return a KML file containing the data to construct the heatmap
    Examples: /api/heatmap"""
    # TODO Create the kml file by inputting points into the model and
    # having it process the points. Then we'll retrieve the points here
    # and their associated weights and create the kml here or the model
    # will do it
    return send_from_directory('static', 'presentation_data.kml')

@API.route('/heatmap2/bounds', methods=['GET'])
@AUTO.doc()
def get_heatmap_2_bounds():
    """Return JSON indicating bounds of all the image files that make up the heatmap
    Examples: /api/heatmap2/bounds
    """
    # TODO not hardcode this
    path = 'static/heatmap'
    return send_from_directory(path, 'heatmap_bounds.json')

@API.route('/heatmap2', methods=['GET'])
@AUTO.doc()
def get_heatmap_2():
    """Return PNG of the heatmap of safety in the city
    Examples: /api/heatmap2"""
    # TODO not hardcode this
    path = 'static/heatmap'
    return send_from_directory(path, 'heatmap.png')

def get_coordinates(address):
    city = API.config['geocode']['city']
    state = API.config['geocode']['state']
    geo = SSGeo(API.config['geocode']['config'])
    try:
        lat1, lon1, cache1 = geo.getlatlon(address, state, city)
    except GeocoderTimedOut:
        lat1, lon1, cache1 = None, None, None
    return lat1, lon1 

@API.route('/route', methods=['GET'])
@AUTO.doc()
def get_route():
    """Return a distance based route between geographical points
    Args: lat1 (str, required), lon1 (str, required), lat2 (str, required), lon2 (str, required) OR address1 (str, required), address2 (str, required)
    Examples: /api/route?lat1=37.7670817&lon1=-122.4076907&lat2=37.767500&lon2=-122.408900"""

    lat1, lon1, lat2, lon2 = request.args.get('lat1'), request.args.get('lon1'), \
        request.args.get('lat2'), request.args.get('lon2')
    address1, address2 = request.args.get('address1'), request.args.get('address2')

    # Verify that start and end points have been provided
    if ((None in [lat1, lon1, lat2, lon2]) and 
         (None in [address1, address2])):
        return jsonify({"error": "Did not supply all lat/lon pairs or two addresses"})
    # Geocode the addresses 
    if address1 != None and address2 != None:
          lat1, lon1 = get_coordinates(address1.replace('_', ' '))
          if not lat1 or not lon1:
              return jsonify({"error": "Cannot geocode %s" % address1})
          lat2, lon2 = get_coordinates(address2.replace('_', ' '))
          if not lat2 or not lon2:
              return jsonify({"error": "Cannot geocode %s" % address2})            
    
    lat1, lon1, lat2, lon2 = float(lat1), float(lon1), float(lat2), float(lon2)

    graph = read_graph(API.config['routing']['graph'])
    mongo_conn = MongoConnection()
    mongo_conn.set_database(API.config['mongo']['routing_db'])
    mongo_conn.set_collection(API.config['mongo']['nodes'])

    try:
        path = get_path(graph, mongo_conn._collection,
                    lat1, lon1, lat2, lon2) 
    except Exception as e:
        return jsonify({"error": "Cannot get path %s" % e})
    return jsonify({"path": path })

@API.route('/risk_route', methods=['GET'])
@AUTO.doc()
def get_risk_route():
    """Get a safety based route between two latitude and longitude points
    Args: lat1 (str, required), lon1 (str, required), lat2 (str, required), lon2 (str, required) OR address1 (str, required), address2 (str, required)
    Examples: /api/route?lat1=37.7670817&lon1=-122.4076907&lat2=37.767500&lon2=-122.408900"""

    lat1, lon1, lat2, lon2 = request.args.get('lat1'), request.args.get('lon1'), \
        request.args.get('lat2'), request.args.get('lon2')
    address1, address2 = request.args.get('address1'), request.args.get('address2')
    risk = request.args.get('risk')
    if risk is not None:
        risk = float(risk)
        if risk >= 0 and risk <= 1.0:
            distance = 1.0 - risk
        else:
            return jsonify({"error": "Risk Factor is not between 0 & 1" })
          

    # Verify that start and end points have been provided
    if ((None in [lat1, lon1, lat2, lon2]) and
         (None in [address1, address2])):
        return jsonify({"error": "Did not supply all lat/lon pairs or two addresses"})
    # Geocode the addresses
    if address1 != None and address2 != None:
          lat1, lon1 = get_coordinates(address1.replace('_', ' '))
          if not lat1 or not lon1:
              return jsonify({"error": "Cannot geocode %s" % address1})
          lat2, lon2 = get_coordinates(address2.replace('_', ' '))
          if not lat2 or not lon2:
              return jsonify({"error": "Cannot geocode %s" % address2})

    lat1, lon1, lat2, lon2 = float(lat1), float(lon1), float(lat2), float(lon2)

    if risk is not None and distance is not None:
        graph = read_graph(API.config['routing']['crime_graph'], True, distance, risk)
    else:
        graph = read_graph(API.config['routing']['crime_graph'])
    mongo_conn = MongoConnection()
    mongo_conn.set_database(API.config['mongo']['routing_db'])
    mongo_conn.set_collection(API.config['mongo']['nodes'])

    try:
        path = get_path(graph, mongo_conn._collection,
                    lat1, lon1, lat2, lon2, distance=False) 
    except Exception as e:
        return jsonify({"error": "Cannot get path %s" % e})
    return jsonify({"path": path })
    

@API.route('/snapshot', methods=['GET'])
@AUTO.doc()
def get_snapshot():
    """Grab the weights at all the street segments in the city
    Examples: /api/snapshot"""
    mongo_conn = MongoConnection()
    mongo_conn.set_database(API.config['mongo']['routing_db'])
    mongo_conn.provide_credentials(
        API.config['mongo']['username'],
        API.config['mongo']['password'],
        API.config['mongo']['source'])
    mongo_conn.set_collection(API.config['mongo']['snapshot'])
    results = mongo_conn.query_greater_than('weight', 0, {"call_id":0, "_id":0})
    return jsonify(results)

@API.route('/calls')
def documentation():
    """Return HTML documentation outlining all the possible API calls
    Examples: /api/calls"""
    return AUTO.html(title="StreetSavvy API Calls", template="autodoc_custom.html")


APP = Flask(__name__)
AUTO.init_app(APP)
APP.register_blueprint(API, url_prefix='/api')


if __name__ == "__main__":
    APP.run(host='0.0.0.0', debug=False)
