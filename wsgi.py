"""uWSGI entry point."""

from run import APP

if __name__ == "__main__":
    APP.run()
